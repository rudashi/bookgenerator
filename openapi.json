{
  "openapi": "3.0.3",
  "info": {
    "title": "Book Generator docs",
    "description": "Generator for full cover schema of Book.",
    "version": "latest",
    "contact": {
      "name": "Borys Żmuda",
      "email": "rudashi@gmail.com"
    }
  },
  "servers": [
    {
      "url": "https://{environment}/api",
      "variables": {
        "environment": {
          "default": "sam.totem.com.pl",
          "enum": [
            "sam.totem.com.pl",
            "v.totem.com.pl"
          ]
        }
      }
    }
  ],
  "paths": {
    "/book-generator": {
      "post": {
        "tags": [
          "Book Calculator"
        ],
        "operationId": "book.create",
        "description": "Get book scheme",
        "summary": "book-generator.view",
        "security": [
          {
            "bearer-token": []
          }
        ],
        "requestBody": {
          "$ref": "#/components/requestBodies/BookApiRequest"
        },
        "responses": {
          "200": {
            "$ref": "#/components/responses/BookResource"
          },
          "422": {
            "description": "Unprocessable Entity"
          }
        }
      }
    },
    "/book-generator/pdf": {
      "post": {
        "tags": [
          "Book Calculator"
        ],
        "operationId": "book.pdf",
        "description": "Get book scheme in pdf",
        "summary": "book-generator.view",
        "security": [
          {
            "bearer-token": []
          }
        ],
        "requestBody": {
          "$ref": "#/components/requestBodies/BookApiRequest"
        },
        "responses": {
          "200": {
            "$ref": "#/components/responses/PDFResponse"
          },
          "422": {
            "description": "Unprocessable Entity"
          }
        }
      }
    },
    "/book-generator/schema": {
      "post": {
        "tags": [
          "Book Calculator"
        ],
        "operationId": "book.pdfOutline",
        "description": "Get book outline scheme in pdf",
        "summary": "book-generator.view",
        "security": [
          {
            "bearer-token": []
          }
        ],
        "requestBody": {
          "$ref": "#/components/requestBodies/BookApiRequest"
        },
        "responses": {
          "200": {
            "$ref": "#/components/responses/PDFResponse"
          },
          "422": {
            "description": "Unprocessable Entity"
          }
        }
      }
    },
    "/book-generator/public": {
      "get": {
        "tags": [
          "Book Calculator Public"
        ],
        "operationId": "book.public.information",
        "description": "Get all public information",
        "parameters": [
          {
            "in": "path",
            "name": "default",
            "required": true,
            "schema": {
              "type": "string",
              "default": "yes"
            }
          },
          {
            "in": "header",
            "name": "Accept-Language",
            "schema": {
              "$ref": "#/components/schemas/LanguageEnum"
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/components/responses/FormResource"
          }
        }
      },
      "post": {
        "tags": [
          "Book Calculator Public"
        ],
        "operationId": "book.public.create",
        "description": "Get book scheme",
        "requestBody": {
          "$ref": "#/components/requestBodies/BookApiRequest"
        },
        "responses": {
          "200": {
            "$ref": "#/components/responses/BookResource"
          },
          "422": {
            "description": "Unprocessable Entity"
          }
        }
      }
    },
    "/book-generator/public/pdf": {
      "post": {
        "tags": [
          "Book Calculator Public"
        ],
        "operationId": "book.public.pdf",
        "description": "Get book scheme in pdf",
        "requestBody": {
          "$ref": "#/components/requestBodies/BookApiRequest"
        },
        "responses": {
          "200": {
            "$ref": "#/components/responses/PDFResponse"
          },
          "422": {
            "description": "Unprocessable Entity"
          }
        }
      }
    },
    "/book-generator/public/schema": {
      "post": {
        "tags": [
          "Book Calculator Public"
        ],
        "operationId": "book.public.pdfOutline",
        "description": "Get book outline scheme in pdf",
        "requestBody": {
          "$ref": "#/components/requestBodies/BookApiRequest"
        },
        "responses": {
          "200": {
            "$ref": "#/components/responses/PDFResponse"
          },
          "422": {
            "description": "Unprocessable Entity"
          }
        }
      }
    }
  },
  "components": {
    "securitySchemes": {
      "bearer-token": {
        "type": "http",
        "scheme": "bearer",
        "bearerFormat": "JWT"
      }
    },
    "requestBodies": {
      "BookApiRequest": {
        "required": true,
        "content": {
          "application/json": {
            "schema": {
              "type": "object",
              "required": [
                "cover_type",
                "width",
                "height",
                "bind_type",
                "paper",
                "printer",
                "language"
              ],
              "properties": {
                "cover_type": {
                  "type": "string",
                  "enum": ["0", "1"],
                  "example": "0"
                },
                "width": {
                  "type": "integer",
                  "minimum": 0,
                  "example": 165
                },
                "height": {
                  "type": "integer",
                  "minimum": 0,
                  "example": 235
                },
                "bind_type": {
                  "type": "string",
                  "example": "4"
                },
                "paper": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "required": [
                      "pages",
                      "type",
                      "weight",
                      "volume"
                    ],
                    "properties": {
                      "pages": {
                        "type": "integer",
                        "example": 100
                      },
                      "type": {
                        "type": "integer",
                        "example": 4
                      },
                      "weight": {
                        "type": "integer",
                        "example": 90
                      },
                      "volume": {
                        "type": "number",
                        "format": "float",
                        "example": 1.25
                      },
                      "volume_name": {
                        "type": "number",
                        "format": "float",
                        "example": 1.25
                      }
                    }
                  }
                },
                "spine": {
                  "$ref": "#/components/schemas/OnOffEnum"
                },
                "spine_encore": {
                  "$ref": "#/components/schemas/OnOffEnum"
                },
                "cardboard": {
                  "$ref": "#/components/schemas/CardboardEnum"
                },
                "flaps": {
                  "$ref": "#/components/schemas/OnOffEnum"
                },
                "flaps_symmetric": {
                  "$ref": "#/components/schemas/OnOffEnum"
                },
                "flaps_width": {
                  "type": "integer",
                  "minimum": 40
                },
                "flaps_left": {
                  "type": "integer",
                  "minimum": 40
                },
                "flaps_right": {
                  "type": "integer",
                  "minimum": 40
                },
                "flaps_thickness": {
                  "$ref": "#/components/schemas/FlapThicknessEnum"
                },
                "printer": {
                  "$ref": "#/components/schemas/PrinterTypeEnum"
                },
                "job_name": {
                  "type": "string",
                  "nullable": true,
                  "example": null
                },
                "show_big": {
                  "$ref": "#/components/schemas/OnOffEnum"
                },
                "no_logo": {
                  "$ref": "#/components/schemas/OnOffEnum"
                },
                "language": {
                  "$ref": "#/components/schemas/LanguageEnum"
                }
              }
            }
          }
        }
      }
    },
    "responses": {
      "FormResource": {
        "description": "OK",
        "content": {
          "application/json": {
            "schema": {
              "$ref": "#/components/schemas/Form"
            }
          }
        }
      },
      "BookResource": {
        "description": "OK",
        "content": {
          "application/json": {
            "schema": {
              "$ref": "#/components/schemas/Book"
            }
          }
        }
      },
      "PDFResponse": {
        "description": "OK",
        "content": {
          "application/pdf": {
            "schema": {
              "type": "string",
              "format": "binary"
            }
          }
        }
      }
    },
    "schemas": {
      "Form": {
        "type": "object",
        "properties": {
          "printers": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/PrinterType"
            }
          },
          "binds": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/BindType"
            }
          },
          "spines": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/SpineType"
            }
          },
          "cardboard": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/CardboardThickness"
            }
          },
          "flap_thickness": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/FlapThickness"
            }
          },
          "languages": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Language"
            }
          }
        }
      },
      "Book": {
        "type": "object",
        "properties": {
          "messages": {
            "type": "object",
            "additionalProperties": {
              "type": "array",
              "items": {}
            },
            "example": {
              "spine": [
                "Spine size problem"
              ]
            }
          },
          "name": {
            "type": "string",
            "nullable": true,
            "example": null
          },
          "binding": {
            "type": "string",
            "example": "Hardcover - thread sewn"
          },
          "cover": {
            "type": "string",
            "example": "Cover"
          },
          "pages": {
            "type": "integer",
            "example": 150
          },
          "parameters": {
            "type": "object",
            "properties": {
              "page": {
                "type": "object",
                "properties": {
                  "rear": {
                    "type": "string",
                    "format": "float",
                    "example": "195.00"
                  },
                  "front": {
                    "type": "string",
                    "format": "float",
                    "example": "195.00"
                  },
                  "height": {
                    "type": "string",
                    "example": "235"
                  }
                }
              },
              "hinge": {
                "type": "string",
                "example": "9"
              },
              "big": {
                "type": "string",
                "example": "0"
              },
              "spine": {
                "type": "string",
                "format": "float",
                "example": "195.00"
              },
              "bleeds": {
                "type": "string",
                "example": "18"
              },
              "bleeds_inner": {
                "type": "string",
                "example": "0"
              },
              "flaps": {
                "type": "object",
                "properties": {
                  "rear": {
                    "type": "string",
                    "example": "0"
                  },
                  "front": {
                    "type": "string",
                    "example": "0"
                  }
                }
              },
              "total": {
                "type": "object",
                "properties": {
                  "width": {
                    "type": "string",
                    "format": "float",
                    "example": "509.0"
                  },
                  "height": {
                    "type": "string",
                    "example": "314"
                  }
                }
              },
              "net": {
                "type": "object",
                "properties": {
                  "width": {
                    "type": "string",
                    "format": "float",
                    "example": "473.0"
                  },
                  "height": {
                    "type": "string",
                    "example": "278"
                  }
                }
              },
              "weight": {
                "type": "string",
                "example": "257.59"
              }
            }
          },
          "block": {
            "type": "string",
            "format": "float",
            "example": "62.75"
          },
          "weight": {
            "type": "string",
            "example": "620.75"
          }
        }
      },

      "PrinterType": {
        "type": "array",
        "items": {
          "type": "object",
          "properties": {
            "key": {
              "type": "string",
              "enum": ["TONER", "INK_JET"],
              "example": "TONER"
            },
            "value": {
              "$ref": "#/components/schemas/PrinterTypeEnum"
            },
            "text": {
              "type": "string",
              "enum": ["Toner", "Ink jet"],
              "example": "Toner"
            },
            "description": {
              "type": "array",
              "items": {
                "type": "string",
                "example": "SCREEN"
              }
            }
          }
        }
      },
      "BindType": {
        "type": "array",
        "items": {
          "type": "object",
          "properties": {
            "key": {
              "type": "string",
              "example": "SOFTCOVER_GLUED"
            },
            "value": {
              "type": "string",
              "example": "1"
            },
            "label": {
              "type": "string",
              "example": "Hardcover - thread sewn"
            },
            "flaps_allowed": {
              "type": "boolean",
              "example": false
            },
            "cardboard_allowed": {
              "type": "boolean",
              "example": false
            },
            "spine_allowed": {
              "type": "boolean",
              "example": false
            },
            "big_allowed": {
              "type": "boolean",
              "example": false
            },
            "spiral_color_allowed": {
              "type": "boolean",
              "example": false
            },
            "spiral_position_allowed": {
              "type": "boolean",
              "example": false
            },
            "dust_jacket_allowed": {
              "type": "boolean",
              "example": false
            }
          }
        }
      },
      "SpineType": {
        "type": "array",
        "items": {
          "type": "object",
          "properties": {
            "key": {
              "type": "string",
              "enum": ["FLAT", "ROUND"],
              "example": "FLAT"
            },
            "value": {
              "$ref": "#/components/schemas/OnOffEnum"
            },
            "text": {
              "type": "string",
              "example": "Flat"
            }
          }
        }
      },
      "CardboardThickness": {
        "type": "array",
        "items": {
          "type": "object",
          "properties": {
            "key": {
              "type": "string",
              "enum": [
                "CARDBOARD_0_8",
                "CARDBOARD_1",
                "CARDBOARD_1_5",
                "CARDBOARD_2",
                "CARDBOARD_2_5",
                "CARDBOARD_3"
              ],
              "example": "CARDBOARD_1"
            },
            "value": {
              "$ref": "#/components/schemas/CardboardEnum"
            },
            "text": {
              "$ref": "#/components/schemas/CardboardEnum"
            }
          }
        }
      },
      "FlapThickness": {
        "type": "array",
        "items": {
          "type": "object",
          "properties": {
            "key": {
              "type": "string",
              "enum": ["FLAP_0", "FLAP_1", "FLAP_2", "FLAP_3"],
              "example": "FLAP_0"
            },
            "value": {
              "$ref": "#/components/schemas/FlapThicknessEnum"
            },
            "text": {
              "$ref": "#/components/schemas/FlapThicknessEnum"
            }
          }
        }
      },
      "Language": {
        "type": "array",
        "items": {
          "type": "object",
          "properties": {
            "key": {
              "type": "string",
              "enum": [
                "POLISH",
                "ENGLISH",
                "GERMAN",
                "FRENCH",
                "SPANISH",
                "PORTUGUESE",
                "SWEDISH"
              ],
              "example": "POLISH"
            },
            "value": {
              "$ref": "#/components/schemas/LanguageEnum"
            },
            "text": {
              "type": "string",
              "example": "Polish"
            }
          }
        }
      },

      "CardboardEnum": {
        "type": "number",
        "format": "float",
        "enum": [0.8, 1, 1.5, 2, 2.5, 3],
        "example": 1
      },
      "FlapThicknessEnum": {
        "type": "integer",
        "enum": [0, 1, 2, 3],
        "example": 0
      },
      "PrinterTypeEnum": {
        "type": "string",
        "enum": ["toner", "ink-jet"],
        "example": "toner"
      },
      "LanguageEnum": {
        "type": "string",
        "enum": ["pl", "en", "de", "fr", "sp", "pt", "sv"],
        "example": "pl"
      },
      "OnOffEnum": {
        "type": "integer",
        "enum": [0, 1],
        "example": 1
      }

    }
  }
}
