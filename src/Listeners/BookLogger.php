<?php

namespace Rudashi\BookGenerator\Listeners;

use Illuminate\Support\Facades\DB;
use Rudashi\BookGenerator\Events\CoverSchemeGenerated;

class BookLogger
{

    public function handle(CoverSchemeGenerated $event): void
    {
        DB::table('book_generator_logs')->insert([
            'created_at' => \Carbon\Carbon::now(),
            'user_id' => $event->user !== null ? $event->user->getAuthIdentifier() : null,
            'user' => $event->user !== null ? $event->user->getAttribute('fullname') : 'API',
            'job_title' => $event->book->name,
            'type' => $event->book->cover->label,
            'language' => $event->request['language'],
            'request' => json_encode($event->request),
            'output' => json_encode($event->book->cover),
        ]);
    }

}
