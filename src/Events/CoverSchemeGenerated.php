<?php

namespace Rudashi\BookGenerator\Events;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Rudashi\BookGenerator\App\Classes\Book;

class CoverSchemeGenerated
{
    use SerializesModels;

    /**
     * @var Authenticatable|Model|null
     */
    public ?Authenticatable $user = null;
    public Book $book;
    public array $request;

    public function __construct(Book $book, Request $request)
    {
        $this->book = $book;
        $this->user = auth()->user();
        $this->request = $request->except('_token');
    }

}
