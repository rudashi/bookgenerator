<?php

return [

    \Rudashi\BookGenerator\App\Enums\SpineType::class => [

        \Rudashi\BookGenerator\App\Enums\SpineType::FLAT   => __('Flat'),
        \Rudashi\BookGenerator\App\Enums\SpineType::ROUND  => __('Round'),
    ],

    \Rudashi\BookGenerator\App\Enums\SpiralPosition::class => [

        \Rudashi\BookGenerator\App\Enums\SpiralPosition::LONG_SIDE   => __('Long side'),
        \Rudashi\BookGenerator\App\Enums\SpiralPosition::SHORT_SIDE  => __('Short side'),
    ],

    \Rudashi\BookGenerator\App\Enums\Language::class => [

        \Rudashi\BookGenerator\App\Enums\Language::POLISH   => __('Polish'),
        \Rudashi\BookGenerator\App\Enums\Language::ENGLISH  => __('English'),
        \Rudashi\BookGenerator\App\Enums\Language::GERMAN  => __('German'),
        \Rudashi\BookGenerator\App\Enums\Language::SPANISH  => __('Spanish'),
        \Rudashi\BookGenerator\App\Enums\Language::PORTUGUESE  => __('Portuguese'),
        \Rudashi\BookGenerator\App\Enums\Language::FRENCH  => __('French'),
        \Rudashi\BookGenerator\App\Enums\Language::SWEDISH  => __('Swedish'),
    ],

];
