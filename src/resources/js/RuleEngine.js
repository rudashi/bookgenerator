class Validation {

    /**
     * @param { { _cache, rowSelector } | Array } selectors
     * @param { string|null } rowSelector
     */
    constructor(selectors, rowSelector = null) {
        this._cache         = [];
        this.validated      = [];
        this.result         = false;
        this.rowSelector    = rowSelector;

        if (selectors instanceof Array) {
            this.setCache(selectors);
        }

        if (selectors instanceof RuleEngine) {
            this.setCache(selectors._cache);
            this.setRowSelector(selectors.rowSelector);
        }
    }

    validate(condition, actions, rowSelector = null) {

        if (typeof condition === "undefined") {
            throw new Error(`No condition hss been provided.`);
        }

        if (rowSelector !== null) {
            this.setRowSelector(rowSelector);
        }

        if (this.rowSelector === null) {
            throw new Error(`Row selector not provided.`);
        }

        this.validateRules(condition.rules);

        this.validateOperator(condition.operator);

        this.executeEvent(actions);
    }

    clearValidated() {
        this.validated = [];
    }

    validateRules(rules) {
        this.clearValidated();

        for (let r in rules) {
            if (rules.hasOwnProperty(r)) {
                this.validated[r] = Utilities[rules[r].operator](this._cache[rules[r].selector], rules[r].value);
            }
        }
    }

    validateOperator(operator) {
        this.result = this.validated[operator]( rule => { return rule === true; });
    }

    executeEvent(actions) {
        actions.map( action => {
            let element = this._cache[action.selector].closest(this.rowSelector);

            if (element === null) {
                throw new Error(`Row selector "${this.rowSelector}" not exists for action "${action.selector}".`);
            }

            if (action.event.property === "style") {
                let key = Object.keys(action.event[this.result])[0];

                element.style[key] = action.event[this.result][key];
            } else {
                let type = this._cache[action.selector].tagName;
                switch (type.toLowerCase()) {
                    case "select" :
                    case "input" :
                        if (this.result === true) {
                            action.event.map(event => {
                                this._cache[action.selector][event.property] = event[this.result];
                            });
                        }
                        break;
                    default :
                }
            }
        });
    }

    setRowSelector(selector) {
        this.rowSelector = selector;
        return this;
    }
    setCache(selectors) {
        for (let i in selectors) {
            if (selectors.hasOwnProperty(i)) {
                let element = document.querySelector(selectors[i]);
                if (element !== null) {
                    this._cache[selectors[i]] = element;
                } else {
                    throw new Error(`Selector: "${selectors[i]}" is not correct.`);
                }
            }
        }
    }
}

class Condition {

    /** @param { { if, then} } condition */
    constructor(condition) {
        if (typeof condition.if !== "undefined" && typeof condition.then !== "undefined") {
            this.setWhen(condition.if);
            this.setThen(condition.then);
        }
    }

    /** @param { { condition, rules : [ { operator, selector, value } ] } } when */
    setWhen(when) {
        if (typeof when.rules !== "undefined") {
            this.when = {
                operator: Condition.condition(when.condition),
                rules: when.rules.map(rule => new Rule(rule)),
            };
        }
    }

    /** @param { [ { selector, operator, attribute, event } ] } then */
    setThen(then) {
        this.then = then.map(
            action => ( {
                selector: Utilities.addIfMissingSelector(action.selector),
                event: Condition.event(action.event, action.attribute),
            } )
        );
    }

    static condition(condition = "AND") {
        switch (condition.toUpperCase()) {
            case "AND" :
                return "every";
            case "OR" :
                return "some";
            default :
                throw new Error(`Condition: "${condition}" is not correct.`);
        }

    }

    static event(event = "undefined", attribute = null) {
        switch (event.toLowerCase()) {
            case "show" :
                return new EventStyle( { display : "" }, { display : "none" } );
            case "hide" :
                return new EventStyle( { display : "none" }, { display : "" } );
            case "undefined" :
                let keys = Object.keys(attribute);
                return keys.map(k => new EventAttribute( attribute[k], k ));
            default :
                throw new Error(`Event: "${event}" is not correct.`);
        }
    }
}

class Rule {

    constructor(rule) {
        if (Object.entries(rule).length === 0 && rule.constructor === Object) {
            throw new Error(`Rule is empty. example:
                {
                    id : 'food', //without #
                    selector: 'food', // name | id | class,
                    operator: 'is', equal | notEqual | is | not
                    value: 'banana', field value | option selected value | checked
                }
            `);
        }
        if (rule.operator in Utilities) {
            this.selector   = Utilities.addIfMissingSelector(rule.selector);
            this.operator   = rule.operator;
            this.value      = Utilities.attribute(rule.value);
        } else {
            throw new Error(`Operator: "${rule.operator}" is not correct.`) ;
        }
    }
}
class Fact {

    constructor(fact) {
        this.selector   = fact.selector;
        this.value      = fact.value;
    }

}

class Event {

    constructor(forTrue, forFalse) {
        this.true   = forTrue;
        this.false  = forFalse;
    }
}
class EventStyle extends Event {
    constructor(forTrue, forFalse) {
        super(forTrue, forFalse);
        this.property   = "style";
    }
}
class EventAttribute extends Event {
    constructor(forTrue, forFalse) {
        super(forTrue, "");
        this.false      = this.setFalse();
        this.property   = Utilities.attribute(forFalse);
    }

    setFalse() {
        return this.true === true ? false : "";
    }

}

class RuleEngine {

    /** @param { { rowSelector: string, conditions: [], facts: [] } } options */
    constructor(options = {}) {
        this.conditions     = [];
        this.facts          = [];
        this._cache         = [];
        this.validator      = {};

        this.setRowSelector(options.rowSelector || ".form-row");
        this.addRules(options.conditions || []) ;
        this.addFacts(options.facts || []);
    }

    setRowSelector(rowSelector) {
        this.rowSelector = rowSelector;
        return this;
    }
    addRule(rule) {
        if (typeof rule === "undefined") {
            throw new Error(`No rule hss been provided.`);
        }

        if (rule instanceof Array) {
            return this.addRules(rule);
        }

        this.conditions.push(
            new Condition(rule)
        );

        this.appendToCache(this.conditions);

        return this;
    }
    addRules(rules) {
        if (rules instanceof Array) {
            this.conditions = this.conditions.concat(
                rules.map( (r) => new Condition(r) )
            );

            this.appendToCache(this.conditions);

            return this;
        }

        return this.addRule(rules);
    }

    addFact(fact) {
        if (fact instanceof Array) {
            return this.addFacts(fact);
        }

        this.facts.push(
            new Fact(fact)
        );

        this.appendToCache(this.facts);

        return this;
    }
    addFacts(facts) {
        if (facts instanceof FormData) {
            facts = [...facts].map( e => ({
                selector: '[name="' + e[0] +'"]',
                value: e[1]
            }));
        }

        if (facts instanceof Array) {
            this.facts = this.facts.concat(
                facts.map( f => new Fact(f) )
            );

            this.appendToCache(this.facts);

            return this;
        }

        return this.addFact(facts);
    }
    setFacts(facts) {
        this.facts = [];

        return this.addFacts(facts);
    }

    appendToCache(object) {
        Utilities.searchKeyInObject(object, "selector", (selector) => this.pushToCache(selector) );

        return this;
    }
    pushToCache(selector) {
        this._cache.push(selector);
        return this;
    }
    clearCache() {
        this._cache = [...new Set(this._cache)];
        return this;
    }

    run(withoutNewValidator = false) {
        this.clearCache();

        if (withoutNewValidator === false) {
            this.validator = new Validation(this._cache, this.rowSelector);
        }

        this.conditions.map(
            c => this.validator.validate(c.when, c.then)
        );

        return this;
    }
}

class Utilities {

    static equal(element, value) {
        return element.value === value;
    }

    static notEqual(element, value) {
        return element.value !== value;
    }

    static is(element, attribute) {
        return element[attribute];
    }

    static not(element, attribute) {
        return !element[attribute];
    }

    static addIfMissingSelector(selector) {
        return ['[name="', '#', '.', '[data-'].some(s => selector.includes(s)) ? selector : '[name="' + selector + '"]';
    }

    static searchKeyInObject(obj, keyName, callback) {

        if (!obj || (typeof obj === "string")) {
            return null;
        }

        if (obj.hasOwnProperty(keyName) ){
            return obj[keyName];
        }

        for (let i in obj) {
            if (obj.hasOwnProperty(i)){
                let selector = this.searchKeyInObject(obj[i], keyName, callback);
                if (selector) {
                    callback(selector);
                }
            }
        }
        return null;
    }

    static attribute(attribute) {
        switch (attribute.toLowerCase()) {
            case "readonly" :
                return "readOnly";
            case "checked" :
                return "checked";
            default:
                return attribute;
        }
    }
}

export default RuleEngine;