class PaperGroup {

    index;

    /** @type { HTMLElement } */
    group;

    /** @type { {row, paper, weight, volume, group} } */
    _sel;

    /** @type { HTMLSelectElement } */
    paperSel;

    /** @type { HTMLSelectElement } */
    weightSel;

    /** @type { HTMLInputElement } */
    weightInput;

    /** @type { HTMLSelectElement } */
    volumeSel;

    /** @type { HTMLInputElement } */
    volumeInput;

    /** @typedef [ key : { name, value, default, volumes : [] } ] */
    currPaper;

    /** @typedef { { name, value, default, volumes : [] } }   */
    currWeight;

    _papers;

    constructor({index, group, selectors: {groupClass, row, paper, weight, volume}, papers}) {
        this.index = index;
        this.group = group;
        this._sel = {
            row : row,
            paper : paper,
            weight : weight,
            volume : volume,
            group : groupClass
        };
        this.paperSel = group.querySelector(`select${this._sel.paper}`);
        this.weightSel = group.querySelector(`select${this._sel.weight}`);
        this.volumeSel = group.querySelector(`select${this._sel.volume}`);
        this.weightInput = group.querySelector(`input${this._sel.weight}`);
        this.volumeInput = group.querySelector(`input${this._sel.volume}`);
        this._papers = papers;

        this.addCounterToFormElement();
    }

    getNode() {
        return this.group;
    }

    setCurrentPaper(papers = null) {
        if (papers === null) {
            throw new Error(`Papers object is empty.`);
        }
        this.currPaper = papers[this.paperSel.options[this.paperSel.selectedIndex].dataset.slug].parameters;
    }

    setCurrentWeight(key) {
        this.currWeight = this.currPaper[key];
    }

    addCounterToFormElement() {
        this.group.dataset.count = this.index;
        this.group.classList.add(this._sel.group);
        this.group.setAttribute('id', `${this.group.getAttribute('id')}${this.index}`);

        this.group.querySelectorAll('select, input, label').forEach(el => {
            el.dataset.count = this.index;

            if (el.hasAttribute('for')) {
                el.setAttribute('for', `${el.getAttribute('for')}${this.index}`);
            }
            if (el.hasAttribute('name')) {
                el.setAttribute('name', el.getAttribute('name').replace(/\[/i, `[${this.index}][`));
            }
            if (el.hasAttribute('id')) {
                el.setAttribute('id', `${el.getAttribute('id')}${this.index}`);
            }

            this.toggleVisibilityParameters(el);
        });
    }

    toggleVisibilityParameters(el) {
        let row = el.closest(this._sel.row);

        if (row === null) {
            throw new Error(`Row selector "${this._sel.row}" not exists for parameter "${el.getAttribute('id')}".`);
        }

        if (el.hasAttribute('disabled')) {
            row.style.display = 'none';
        } else {
            row.style.display = '';
        }
    }

    hasPaperParameters() {
        return Object.keys(this.currPaper).length > 0;
    }

    addWeight() {
        this.setCurrentPaper(this._papers);
        PaperGroup.clearOptions(this.weightSel);

        if (this.hasPaperParameters()) {
            this.deactivateInputs();
            for (let [ key, { name, value, 'default': defaulted } ] of Object.entries(this.currPaper)) {
                PaperGroup.appendOption(this.weightSel, name, value, defaulted, key);
            }

            this.addVolume(this.weightSel.options[this.weightSel.selectedIndex].dataset.key);
        } else {
            this.activateInputs();
        }
    }

    addVolume(key) {
        this.setCurrentWeight(key);
        PaperGroup.clearOptions(this.volumeSel);

        this.currWeight.volumes.map(({name, value, 'default': defaulted}) => {
            PaperGroup.appendOption(this.volumeSel, name, value, defaulted);
        });
    }

    activateInputs() {
        this.weightSel.setAttribute('disabled', 'disabled');
        this.volumeSel.setAttribute('disabled', 'disabled');

        this.weightInput.removeAttribute('disabled');
        this.volumeInput.removeAttribute('disabled');

        [this.weightSel, this.volumeSel, this.weightInput, this.volumeInput].map(el => this.toggleVisibilityParameters(el));
    }

    deactivateInputs() {
        this.weightInput.setAttribute('disabled', 'disabled');
        this.volumeInput.setAttribute('disabled', 'disabled');

        this.weightSel.removeAttribute('disabled');
        this.volumeSel.removeAttribute('disabled');

        [this.weightSel, this.volumeSel, this.weightInput, this.volumeInput].map(el => this.toggleVisibilityParameters(el));
    }

    static clearOptions(select) {
        select.innerHTML = null;
    }

    static appendOption(el, name = null, value = null, defaulted = 0, key = null) {
        let selected = defaulted === 1;

        if (value !== null) {
            let option = new Option(name || value, value, selected, selected);
            option.dataset.key = key;
            el.add(option);
        }
    }

    setEvents() {
        this.paperSel.onchange = () => {
            this.addWeight();
        };
        this.weightSel.onchange = ({target}) => {
            this.addVolume(target.options[target.selectedIndex].dataset.key);
        };
    }

    init() {
        this.setEvents();
        this.addWeight();
    }
}

class Paper {

     #count = 0;

    /** @type { {form, group, groupClass, row, paper, weight, volume, addBtn, removeBtn} } */
    _selectors;

    /** @type { {} } */
    papers;

    /** @type { Array.<PaperGroup>  } */
    groups = [];

    /** @type { HTMLElement } */
    cloned;

    /** @type { HTMLFormElement } */
    #form;

    /** @type { { name, elem: Event } } */
    #event;

    constructor(formSelector, { papers, addBtnSelector, removeBtnSelector, groupPrefix, rowSelector, paperPrefix, weightPrefix, volumePrefix }) {
        this._selectors = {
            form: formSelector || null,
            group: groupPrefix || `[id^="paper-"]`,
            groupClass: 'paper-group',
            row : rowSelector || ".form-row",
            paper : paperPrefix || `[name^="paper[type]"]`,
            weight : weightPrefix || `[id^="weight"]`,
            volume : volumePrefix || `[id^="volume"]`,
            addBtn : addBtnSelector || `#add_paper button`,
            removeBtn : removeBtnSelector || `.remove button`,
        };
        this.papers = papers || null;

        this.setForm(this._selectors.form);
        this.setEvents();
    }

    get event() {
        return this.#event.name;
    }

    setEvents(eventName = 'change_paper') {
        this.#event = {
            name: eventName,
            elem: new Event(eventName)
        };
    }

    setForm(formSelector) {
        this.#form = document.querySelector(formSelector);
    }

    incrementCount() {
        this.#count++;
    }

    setGroup(group, index) {
        this.cloned = document.importNode(group, true);

        let newGroup = new PaperGroup({index, group, selectors: this._selectors, papers: this.papers});

        this.groups.push(newGroup);
        this.incrementCount();

        newGroup.init();

        return newGroup;
    }


    clonePaperGroup() {
        let groups = this.#form.querySelectorAll(this._selectors.group),
            newGroup = this.setGroup(this.cloned, this.#count);

        groups[groups.length - 1].after(newGroup.getNode());
    }

    removePaperGroup(elem) {
        let groupIndex = elem.closest(this._selectors.group).dataset.count;
        this.groups[groupIndex].getNode().remove();
        document.dispatchEvent(this.#event.elem);
    }

    hideRemoveBtn(group) {
        group.querySelector(this._selectors.removeBtn).style.display = 'none';
    }

    init() {

        if (this.#form === null) {
            throw new Error(`Form ${formSelector} not exists in document.`);
        }

        this.#form.querySelectorAll(this._selectors.group).forEach((group, index) => {
            let newGroup = this.setGroup(group, index);

            if (index === 0) {
                this.hideRemoveBtn(newGroup.getNode());
            }
        });

        this.#form.querySelector(this._selectors.addBtn).onclick = () => {
            this.clonePaperGroup();
            document.dispatchEvent(this.#event.elem);
        };

        this.#form.onclick = ({target}) => {
            if (target.matches(this._selectors.removeBtn)) {
                this.removePaperGroup(target);
            }
        };

        return this;
    }
}

export default Paper;