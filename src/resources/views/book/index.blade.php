@extends('book-generator::book.panel')

@section('title', __('Covers Schema Generator'))

@include('book-generator::book.form')

@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <form method="POST" action="{{ route('book-generator.pdf') }}" id="book-generator">
                @csrf
                <div class="row">
                    <div class="col-md-9 col-12 order-md-2 box">
                        <div class="box-body">
                            <div class="row justify-content-between">
                                <div class="col-6">
                                    <div class="form-group row">
                                        <span class="col-form-label">
                                            {{ __('Cover') }}
                                        </span>
                                        <span class="mx-3">
                                            <input type="checkbox" class="switch-toggle" id="cover_type" name="cover_type" value="1">
                                            <label for="cover_type"></label>
                                        </span>
                                        <span class="col-form-label">
                                            {{ __('Jacket') }}
                                        </span>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group row justify-content-end">
                                        <label class="col-form-label mr-2" for="language">
                                            {{ __('PDF language') }}:
                                        </label>
                                        <div>
                                            <div class="input-group">
                                                <select id="language" name="language" class="form-control">
                                                    @foreach($form->getLanguages() as $language )
                                                        <option value="{{ $language['value'] }}">{{ $language['text'] }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="input-group-append">
                                                    <button class="btn btn-primary">
                                                        {{ __('Download PDF') }}
                                                    </button>
                                                    <button class="btn btn-success"
                                                            formaction="{{ route('book-generator.pdf-outline') }}">
                                                        {{ __('Download PDF Scheme') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                @include('book-generator::book.preview')
                                <div>
                                    <div class="col-xs-4">
                                        <div class="row">
                                            <div class="col-xs-8 text-right"><b>{{ __( 'Block size' ) }} :</b></div>
                                            <div class="col-xs-4">
                                                <span class="block-thickness"></span> mm
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8 text-right"><b>{{ __( 'Total block size' ) }} :</b></div>
                                            <div class="col-xs-4">
                                                <span class="total-block-thickness"></span> mm
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="row">
                                            <div class="col-xs-8 text-right"><b>{{ __( 'Net format' ) }} :</b></div>
                                            <div class="col-xs-4">
                                                <span class="net_width"></span> x <span class="net_height"></span> mm
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8 text-right"><b>{{ __( 'Book weight' ) }} :</b></div>
                                            <div class="col-xs-4">
                                                <span class="book-weight"></span> g
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-12 order-md-1">
                        @yield('width')
                        @yield('height')
                        @yield('bind_type')
                        <hr>
                        <div id="paper-">
                            @yield('pages')
                            @yield('paper_type')
                            @yield('paper_weight_select')
                            @yield('paper_weight')
                            @yield('paper_volume_select')
                            @yield('paper_volume')
                            <div class="remove text-right">
                                <button class="btn btn-sm btn-danger" type="button">
                                    <i class="fa fa-trash-o"></i> {{ __('Delete') }}
                                </button>
                            </div>
                            <hr>
                        </div>
                        <div class="row form-group justify-content-center" id="add_paper">
                            <div class="col-6">
                                <button class="btn btn-block btn-success" type="button">
                                    <i class="fa fa-plus-square"></i> {{ __('Add paper') }}
                                </button>
                            </div>
                        </div>
                        <hr>
                        @yield('spine')
                        @yield('cardboard')
                        <hr>
                        @yield('flaps')
                        @yield('flaps_symmetric')
                        @yield('flaps_width')
                        @yield('flaps_left')
                        @yield('flaps_right')
                        @yield('flaps_thickness')
                        <hr>
                        @yield('printer')
                        @yield('job_name')
                        @yield('show_big')
                        @yield('no_logo')
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts-inline')
    <script>
        const formSelector = '#book-generator',
            rowClass = '.row.form-group',
            engine = new window.RuleEngine({ rowSelector: rowClass }),
            paper = new window.Paper(formSelector, {
                papers: (_ => _)( @json($papers) ),
                rowSelector: rowClass,
            }).init();

        let f = {
            getFormData : () => {
                return new FormData(document.querySelector(formSelector));
            },
            runRuleEngine : (ruleEngine, event) => {
                if (event.target.matches('select, input[type="checkbox"], input[type="number"], input[type="text"]')) {
                    ruleEngine.setFacts(f.getFormData()).run(true);
                    f.makeData();
                }
            },
            makeData : () => {
                let object = {};

                f.getFormData().forEach((value, key) => {
                    object[key] = value
                });
                document.querySelector('.preview').innerHTML = `<pre>${JSON.stringify(object, undefined, 2)}</pre>`;
            },
        },

        rules = [
            // WHEN #cover_type IS checked THEN #flaps [ checked readonly ] #flaps_symmetric [ checked readonly ]
            {
                if: {
                    rules: [
                        {
                            selector: '#cover_type',
                            operator: 'is',
                            value: 'checked'
                        }
                    ]
                },
                then: [
                    {
                        selector: '#flaps',
                        attribute: {
                            checked: true,
                            readonly: true
                        }
                    },
                    {
                        selector: '#flaps_symmetric',
                        attribute: {
                            checked: true,
                            readonly: true
                        }
                    },
                ]
            },
            // WHEN #cover_type NOT checked THEN #flaps [ !readonly ] #flaps_symmetric [ !readonly ]
            {
                if: {
                    rules: [
                        {
                            selector: '#cover_type',
                            operator: 'not',
                            value: 'checked'
                        }
                    ]
                },
                then: [
                    {
                        selector: '#flaps',
                        attribute: {
                            readonly: false
                        }
                    },
                    {
                        selector: '#flaps_symmetric',
                        attribute: {
                            readonly: false
                        }
                    },
                ]
            },

            // WHEN #bind_type EQUAL 4 OR 5 THEN #spine [ show ] #cardboard [ show ] #flaps [ hide ]
            {
                if: {
                    condition: 'or',
                    rules: [
                        {
                            selector: '#bind_type',
                            operator: 'equal',
                            value: '4'
                        },
                        {
                            selector: '#bind_type',
                            operator: 'equal',
                            value: '5'
                        },
                    ]
                },
                then: [
                    {
                        selector: '#spine',
                        event: 'show'
                    },
                    {
                        selector: '#cardboard',
                        event: 'show'
                    },
                    {
                        selector: '#flaps',
                        event: 'hide'
                    },
                ]
            },
            // WHEN #bind_type NOT_EQUAL 4 OR 5 THEN #spine [ hide ] #cardboard [ hide ]
            {
                if: {
                    condition: 'and',
                    rules: [
                        {
                            selector: '#bind_type',
                            operator: 'notEqual',
                            value: '4'
                        },
                        {
                            selector: '#bind_type',
                            operator: 'notEqual',
                            value: '5'
                        },
                    ]
                },
                then: [
                    {
                        selector: '#spine',
                        event: 'hide'
                    },
                    {
                        selector: '#cardboard',
                        event: 'hide'
                    },
                ]
            },

            // WHEN #cover_type IS checked THEN #show_big [ hide ]
            {
                if: {
                    rules: [
                        {
                            selector: '#cover_type',
                            operator: 'is',
                            value: 'checked'
                        }
                    ]
                },
                then: [
                    {
                        selector: '#show_big',
                        event: 'hide'
                    },
                ]
            },
            // WHEN #cover_type NOT checked AND #bind_type EQUAL 1 THEN #show_big [ show ]
            {
                if: {
                    rules: [
                        {
                            selector: '#cover_type',
                            operator: 'not',
                            value: 'checked'
                        },
                        {
                            selector: '#bind_type',
                            operator: 'equal',
                            value: '1'
                        },
                    ]
                },
                then: [
                    {
                        selector: '#show_big',
                        event: 'show'
                    },
                ]
            },
            // WHEN #cover_type NOT checked AND #bind_type EQUAL 2 THEN #show_big [ show ]
            {
                if: {
                    rules: [
                        {
                            selector: '#cover_type',
                            operator: 'not',
                            value: 'checked'
                        },
                        {
                            selector: '#bind_type',
                            operator: 'equal',
                            value: '2'
                        },
                    ]
                },
                then: [
                    {
                        selector: '#show_big',
                        event: 'show'
                    },
                ]
            },


            // WHEN #cover_type IS checked AND #bind_type EQUAL 4 THEN #flaps [ readonly checked ] #flaps_symmetric [ readonly checked ]
            {
                if: {
                    rules: [
                        {
                            selector: '#cover_type',
                            operator: 'is',
                            value: 'checked'
                        },
                        {
                            selector: '#bind_type',
                            operator: 'equal',
                            value: '4'
                        },
                    ]
                },
                then: [
                    {
                        selector: '#flaps',
                        attribute: {
                            readonly: true,
                            checked: true,
                        }
                    },
                    {
                        selector: '#flaps_symmetric',
                        attribute: {
                            readonly: true,
                            checked: true,
                        }
                    },
                ]
            },
            // WHEN #cover_type NOT checked AND #bind_type EQUAL 4 THEN #flaps [ readonly !checked ] #flaps_symmetric [ readonly !checked ]
            {
                if: {
                    rules: [
                        {
                            selector: '#cover_type',
                            operator: 'not',
                            value: 'checked'
                        },
                        {
                            selector: '#bind_type',
                            operator: 'equal',
                            value: '4'
                        }
                    ]
                },
                then: [
                    {
                        selector: '#flaps',
                        attribute: {
                            readonly: true,
                            checked: false,
                        }
                    },
                    {
                        selector: '#flaps_symmetric',
                        attribute: {
                            readonly: true,
                            checked: false,
                        }
                    },
                ]
            },

            // WHEN #cover_type IS checked AND #bind_type EQUAL 5 THEN #flaps [ readonly checked ] #flaps_symmetric [ readonly checked ]
            {
                if: {
                    rules: [
                        {
                            selector: '#cover_type',
                            operator: 'is',
                            value: 'checked'
                        },
                        {
                            selector: '#bind_type',
                            operator: 'equal',
                            value: '5'
                        },
                    ]
                },
                then: [
                    {
                        selector: '#flaps',
                        attribute: {
                            readonly: true,
                            checked: true,
                        }
                    },
                    {
                        selector: '#flaps_symmetric',
                        attribute: {
                            readonly: true,
                            checked: true,
                        }
                    },

                ]
            },
            // WHEN #cover_type NOT checked AND #bind_type EQUAL 5 THEN #flaps [ readonly !checked ] #flaps_symmetric [ readonly !checked ]
            {
                if: {
                    rules: [
                        {
                            selector: '#cover_type',
                            operator: 'not',
                            value: 'checked'
                        },
                        {
                            selector: '#bind_type',
                            operator: 'equal',
                            value: '5'
                        }
                    ]
                },
                then: [
                    {
                        selector: '#flaps',
                        attribute: {
                            readonly: true,
                            checked: false,
                        }
                    },
                    {
                        selector: '#flaps_symmetric',
                        attribute: {
                            readonly: true,
                            checked: false,
                        }
                    },
                ]
            },

            // WHEN #flaps IS checked THEN #flaps_symmetric [ show ] #flaps_thickness [ show ]
            {
                if: {
                    rules: [
                        {
                            selector: '#flaps',
                            operator: 'is',
                            value: 'checked'
                        }
                    ]
                },
                then: [
                    {
                        selector: '#flaps_symmetric',
                        event: 'show'
                    },
                    {
                        selector: '#flaps_thickness',
                        event: 'show'
                    },
                ]
            },
            // WHEN #cover_type IS checked AND #flaps IS checked THEN #flaps_thickness [ hide ]
            {
                if: {
                    rules: [
                        {
                            selector: '#cover_type',
                            operator: 'is',
                            value: 'checked'
                        },
                        {
                            selector: '#flaps',
                            operator: 'is',
                            value: 'checked'
                        }
                    ]
                },
                then: [
                    {
                        selector: '#flaps_thickness',
                        event: 'hide'
                    },
                ]
            },
            // WHEN #cover_type NOT checked AND #flaps IS checked THEN #flaps_thickness [ show ]
            {
                if: {
                    rules: [
                        {
                            selector: '#cover_type',
                            operator: 'not',
                            value: 'checked'
                        },
                        {
                            selector: '#flaps',
                            operator: 'is',
                            value: 'checked'
                        }
                    ]
                },
                then: [
                    {
                        selector: '#flaps_thickness',
                        event: 'show'
                    },
                ]
            },

            // WHEN #flaps IS checked AND #flaps_symmetric NOT checked THEN #flaps_left [ show ] #flaps_right [ show ]
            {
                if: {
                    rules: [
                        {
                            selector: '#flaps',
                            operator: 'is',
                            value: 'checked'
                        },
                        {
                            selector: '#flaps_symmetric',
                            operator: 'not',
                            value: 'checked'
                        }
                    ]
                },
                then: [
                    {
                        selector: '#flaps_left',
                        event: 'show'
                    },
                    {
                        selector: '#flaps_right',
                        event: 'show'
                    },
                ]
            },
            // WHEN #flaps IS checked AND #flaps_symmetric IS checked THEN #flaps_width [ show ]
            {
                if: {
                    rules: [
                        {
                            selector: '#flaps',
                            operator: 'is',
                            value: 'checked'
                        },
                        {
                            selector: '#flaps_symmetric',
                            operator: 'is',
                            value: 'checked'
                        }
                    ]
                },
                then: [
                    {
                        selector: '#flaps_width',
                        event: 'show'
                    },
                ]
            },

            // WHEN #cover_type NOT checked AND #flaps IS readonly AND #flaps_symmetric IS readonly THEN #flaps [ hide ]
            {
                if: {
                    rules: [
                        {
                            selector: '#cover_type',
                            operator: 'not',
                            value: 'checked'
                        },
                        {
                            selector: '#flaps',
                            operator: 'is',
                            value: 'readonly'
                        },
                        {
                            selector: '#flaps_symmetric',
                            operator: 'is',
                            value: 'readonly'
                        }
                    ]
                },
                then: [
                    {
                        selector: '#flaps',
                        event: 'hide'
                    },
                ]
            },

        ];

        engine.addRules(rules);
        engine.addFacts(f.getFormData());
        engine.run();

        f.makeData();

        document.addEventListener("change", f.runRuleEngine.bind(null, engine));
        document.addEventListener(paper.event, f.makeData);

    </script>
@endpush