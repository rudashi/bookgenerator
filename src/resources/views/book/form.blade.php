@section('printer')
<div class="row form-group" id="printer_row">
    <label class="col-form-label col-sm-5" for="printer" >{{ __('Printer') }}</label>
    <div class="col-sm-7">
        <select class="form-control" id="printer" name="printer" required>
            @foreach($form->getPrinterTypes() as $printer)
                <option value="{{ $printer['value'] }}">{{ __($printer['text']) }}</option>
            @endforeach
        </select>
    </div>
</div>
@endsection
@section('job_name')
<div class="row form-group" id="job_name_row">
    <label class="col-form-label col-sm-5" for="job_name">{{ __('Job name') }}</label>
    <div class="col-sm-7">
        <input class="form-control" id="job_name" name="job_name" type="text">
    </div>
</div>
@endsection
@section('width')
<div class="row form-group" id="width_row">
    <label class="col-form-label col-sm-5" for="width">{{ __('Width') }}</label>
    <div class="col-sm-7">
        <div class="input-group">
            <input class="form-control" id="width" name="width" type="number" step="1" value="165" required>
            <div class="input-group-append">
                <span class="input-group-text">{{ __('mm') }}</span>
            </div>
        </div>
    </div>
</div>
@endsection
@section('height')
<div class="row form-group" id="height_row">
    <label class="col-form-label col-sm-5" for="height">{{ __('Height') }}</label>
    <div class="col-sm-7">
        <div class="input-group">
            <input class="form-control" id="height" name="height" type="number" step="1" value="235" required>
            <div class="input-group-append">
                <span class="input-group-text">{{ __('mm') }}</span>
            </div>
        </div>
    </div>
</div>
@endsection
@section('bind_type')
<div class="row form-group" id="bind_type_row">
    <label class="col-form-label col-sm-5" for="bind_type" >{{ __('Bind type') }}</label>
    <div class="col-sm-7">
        <select class="form-control" id="bind_type" name="bind_type" required>
            @foreach($form->getBindTypes() as $bind)
                <option value="{{ $bind['value'] }}" data-flaps="{{ $bind['flaps_allowed'] }}" data-cardboard="{{ $bind['cardboard_allowed']  }}" data-show-big="{{ $bind['big_allowed'] }}">{{ __($bind['label']) }}</option>
            @endforeach
        </select>
    </div>
</div>
@endsection
@section('pages')
    <div class="row form-group">
        <label class="col-form-label col-sm-5" for="pages-">{{ __('Page count') }}</label>
        <div class="col-sm-7">
            <input class="form-control" id="pages-" name="paper[pages]" type="number" step="1" value="100">
        </div>
    </div>
@endsection
@section('paper_type')
<div class="row form-group">
    <label class="col-form-label col-sm-5" for="paper_type-">{{ __('Paper type') }}</label>
    <div class="col-sm-7">
        <select class="form-control" id="paper_type-" name="paper[type]">
            @foreach($papers as $key => $paper)
                <option value="{{ $paper->id }}" data-slug="{{ $paper->slug }}">{{ __($paper->name) }}</option>
            @endforeach
        </select>
    </div>
</div>
@endsection
@section('paper_weight_select')
<div class="row form-group">
    <label class="col-form-label col-sm-5" for="weight-">{{ __('Paper weight') }}</label>
    <div class="col-sm-7">
        <div class="input-group">
            <select class="form-control" id="weight-" name="paper[weight]"></select>
            <div class="input-group-append">
                <span class="input-group-text">{{ __('gsm') }}</span>
            </div>
        </div>
    </div>
</div>
@endsection
@section('paper_weight')
<div class="row form-group">
    <label class="col-form-label col-sm-5" for="weight_i-">{{ __('Paper weight') }}</label>
    <div class="col-sm-7">
        <div class="input-group">
            <input class="form-control" id="weight_i-" name="paper[weight]" type="number" step="1" value="80" disabled>
            <div class="input-group-append">
                <span class="input-group-text">{{ __('gsm') }}</span>
            </div>
        </div>
    </div>
</div>
@endsection
@section('paper_volume_select')
<div class="row form-group">
    <label class="col-form-label col-sm-5" for="volume-">{{ __('Paper volume') }}</label>
    <div class="col-sm-7">
        <input name="volume_name" type="hidden" value="">
        <select class="form-control" id="volume-" name="paper[volume]"></select>
    </div>
</div>
@endsection
@section('paper_volume')
<div class="row form-group">
    <label class="col-form-label col-sm-5" for="volume_i-">{{ __('Paper volume') }}</label>
    <div class="col-sm-7">
        <input class="form-control" id="volume_i-" name="paper[volume]" type="number" step="0.01" value="1" disabled>
    </div>
</div>
@endsection
@section('spine')
<div class="row form-group" id="spine_row">
    <label class="col-form-label col-sm-5" for="spine">{{ __('Spine type') }}</label>
    <div class="col-sm-7">
        <select class="form-control" id="spine" name="spine">
            @foreach($form->getSpineTypes() as $spine )
                <option value="{{ $spine['value'] }}">{{ __($spine['text']) }}</option>
            @endforeach
        </select>
    </div>
</div>
@endsection
@section('cardboard')
<div class="row form-group" id="cardboard_row">
    <label class="col-form-label col-sm-5" for="cardboard">{{ __('Cardboard thickness') }}</label>
    <div class="col-sm-7">
        <div class="input-group">
            <select class="form-control" id="cardboard" name="cardboard">
                @foreach($form->getCardboardTypes() as $cardboard)
                    <option value="{{ $cardboard['value'] }}" {{ $cardboard['value'] === 2.5 ? 'selected' : '' }}>{{ $cardboard['text'] }}</option>
                @endforeach
            </select>
            <div class="input-group-append">
                <span class="input-group-text">{{ __('mm') }}</span>
            </div>
        </div>
    </div>
</div>
@endsection
@section('flaps')
<div class="row form-group" id="flaps_row">
    <label class="col-form-label col-sm-5" for="flaps">{{ __('Cover flaps') }}</label>
    <div class="col-sm-7">
        <input type="checkbox" class="switch-toggle" id="flaps" name="flaps" value="1">
        <label for="flaps"></label>
    </div>
</div>
@endsection
@section('flaps_symmetric')
<div class="row form-group" id="flaps_symmetric_row">
    <label class="col-form-label col-sm-5" for="flaps_symmetric">{{ __('Symmetric') }}</label>
    <div class="col-sm-7">
        <input type="checkbox" class="switch-toggle" id="flaps_symmetric" name="flaps_symmetric" value="1">
        <label for="flaps_symmetric"></label>
    </div>
</div>
@endsection
@section('flaps_left')
<div class="row form-group" id="flaps_left_row">
    <label class="col-form-label col-sm-5" for="flaps_left">{{ __('Flap left') }}</label>
    <div class="col-sm-7">
        <div class="input-group">
            <input class="form-control" id="flaps_left" name="flaps_left" type="number" step="1" value="50">
            <div class="input-group-append">
                <span class="input-group-text">{{ __('mm') }}</span>
            </div>
        </div>
    </div>
</div>
@endsection
@section('flaps_width')
<div class="row form-group" id="flaps_width_row">
    <label class="col-form-label col-sm-5" for="flaps_width">{{ __('Width') }}</label>
    <div class="col-sm-7">
        <div class="input-group">
            <input class="form-control" id="flaps_width" name="flaps_width" type="number" step="1" value="80">
            <div class="input-group-append">
                <span class="input-group-text">{{ __('mm') }}</span>
            </div>
        </div>
    </div>
</div>
@endsection
@section('flaps_right')
<div class="row form-group" id="flaps_right_row">
    <label class="col-form-label col-sm-5" for="flaps_right">{{ __('Flap right') }}</label>
    <div class="col-sm-7">
        <div class="input-group">
            <input class="form-control" id="flaps_right" name="flaps_right" type="number" step="1" value="50">
            <div class="input-group-append">
                <span class="input-group-text">{{ __('mm') }}</span>
            </div>
        </div>
    </div>
</div>
@endsection
@section('flaps_thickness')
<div class="row form-group" id="flaps_thickness_row">
    <label class="col-form-label col-sm-5" for="flaps_thickness">{{ __('Flap thickness') }}</label>
    <div class="col-sm-7">
        <select class="form-control" id="flaps_thickness" name="flaps_thickness">
            @foreach($form->getFlapsThickness() as $flap)
                <option value="{{ $flap['value'] }}" {{ $flap['value'] === 1 ? 'selected' : '' }}>{{ $flap['text'] }}</option>
            @endforeach
        </select>
    </div>
</div>
@endsection
@section('show_big')
<div class="row form-group" id="show_big_row">
    <label class="col-form-label col-sm-5" for="show_big">{{ __('Show big fields') }}</label>
    <div class="col-sm-7">
        <input type="checkbox" class="switch-toggle" id="show_big" name="show_big" value="1" checked>
        <label for="show_big"></label>
    </div>
</div>
@endsection
@section('no_logo')
<div class="row form-group" id="no_logo_row">
    <label class="col-form-label col-sm-5" for="no_logo">{{ __('Without logo') }}</label>
    <div class="col-sm-7">
        <input type="checkbox" class="switch-toggle" id="no_logo" name="no_logo" value="1">
        <label for="no_logo"></label>
    </div>
</div>
@endsection