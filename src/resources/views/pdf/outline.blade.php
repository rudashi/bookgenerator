<!doctype html>
<html>
<head>
    <style>
        * {
            box-sizing: border-box;
        }
        body,
        html {
            margin: 0;
            padding: 0;
            text-rendering: optimizeLegibility;
        }
        .pull-left{
            float: left;
        }

        #bleeds {
            width: {{ $cover->total_width }}mm;
            height: {{ $cover->total_height }}mm;
            border: 0.5mm #000 solid;
        }

        #container {
            margin: {{ $cover->bleeds }}mm 0 0 {{ $cover->bleeds }}mm;
            height: {{ $cover->page_height }}mm;
            width: {{ $cover->net_width }}mm;
        }

        #flap_left,
        #flap_right {
            height: {{ $cover->page_height }}mm;
            border: 0.5mm dashed #000;
            border-right-color: transparent;
            border-left-color: transparent;
        }

        #flap_left {
            display: {{ $cover->flaps_left > 0 ? 'block' : 'none' }};
            width: {{ $cover->flaps_left }}mm;
            border-left-color: #000;
        }

        #page_left,
        #page_right {
            width: {{ $cover->page_width_rear }}mm;
            height: {{ $cover->page_height }}mm;
            border: 0.5mm #000 dashed;
        }

        #left_hinge,
        #right_hinge {
            display: {{ $cover->hinge > 0 || $cover->big > 0 ? 'block' : 'none' }};
            height: {{ $cover->page_height }}mm;
            width: {{ $cover->hinge > 0 ? $cover->hinge : $cover->big }}mm;
            border: 0.5mm dashed #000;
            border-right-color: transparent;
            border-left-color: transparent;
        }

        #left_hinge{
            border-right-color: #000;
        }

        #spine {
            display: {{ $cover->spine > 0 ? 'block' : 'none' }};
            width: {{ $cover->spine }}mm;
            height: {{ $cover->page_height }}mm;
            border: 0.5mm dashed #000;
            border-right-color: transparent;
            border-left-color: transparent;
        }

        #right_hinge{
            border-left-color: #000;
        }

        @if( $cover->spine === 0)
        #page_right {
            border-left-width: 0;
        }
        @endif

        #flap_right{
            display: {{ $cover->flaps_right > 0 ? 'block' : 'none' }};
            width: {{ $cover->flaps_right }}mm;
            border-right-color: #000;
        }
    </style>
    <title></title>
</head>
<body>
<div id="bleeds">
    <div id="container">
        <div id="flap_left" class="pull-left"></div>
        <div id="page_left" class="pull-left"></div>
        <div id="left_hinge" class="pull-left"></div>
        <div id="spine" class="pull-left"></div>
        <div id="right_hinge" class="pull-left"></div>
        <div id="page_right" class="pull-left"></div>
        <div id="flap_right" class="pull-left"></div>
    </div>
</div>
</body>
</html>