<!doctype html>
<html lang="{{ $language }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ __($title) }}</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Lato&subset=latin-ext');
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Lato', sans-serif;
            font-weight: 400;
            font-size: 18px;
            height: 100vh;
            margin: 0;
        }

        header,
        h1, h2, h3, h4, h5, h6 {
            text-align: center;
        }
        h5 {
            margin: 0;
            font-size: 24px;
        }
        .totem {
            height: 80px;
        }
        .totem.dark path {
            fill: #003b71 !important;
        }
        .sam-book-calculator--preview {
            height: 400px;
            display: -webkit-box;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            font-size: 25px;
        }
        .table-borderless {
            border-width: 0;
            width: 100%;
            margin: 0 auto;
        }
        .table-borderless td:first-of-type {
            font-weight: 600;
            text-align: right;
            width: 40%;
        }
        .table-borderless td:last-of-type {
            text-align: left;
            padding-left: 30px;
        }
        .container {
            padding-top: 40px;
        }
    </style>
    <style>
        .cover {
            display: -webkit-box;
            display: flex;
            height: 100%;
            width: 100%;
            font-size: 18px;
            font-weight: 500;
            text-transform: uppercase;
            background-color: #fff;
        }
        .cover div {
            box-sizing: border-box;
        }
        .cover.landscape {
            height: 70%;
        }
        .cover:not(.has-flaps--left):not(.has-flaps--right) {
            width: 600px;
        }
        .cover.landscape:not(.has-flaps--left):not(.has-flaps--right) {
            max-width: 700px;
        }
        .cover:not(.has-flaps--right).has-flaps--left,
        .cover:not(.has-flaps--left).has-flaps--right {
            width: 750px;
        }
        .cover:not(.has-big) .hinge,
        .cover:not(.has-big) .dimension-hinge {
            display: none;
        }
        .cover:not(.has-flaps--left):not(.has-flaps--right) .flaps,
        .cover:not(.has-flaps--left):not(.has-flaps--right) .dimension-flaps--left,
        .cover:not(.has-flaps--left):not(.has-flaps--right) .dimension-flaps--right,
        .cover:not(.has-flaps--right).has-flaps--left .flaps--right,
        .cover:not(.has-flaps--right).has-flaps--left .dimension-flaps--right,
        .cover:not(.has-flaps--left).has-flaps--right .flaps--left,
        .cover:not(.has-flaps--left).has-flaps--right .dimension-flaps--left {
            display: none;
        }

        .cover:not(.wire-o) .bleeds--inner,
        .cover:not(.wire-o) .dimension-bleed--inner {
            display: none;
        }

        .stapled .hinge,
        .stapled .spine,
        .stapled .dimension-hinge,
        .stapled .dimension-spine,
        .wire-o .hinge,
        .wire-o .spine,
        .wire-o .dimension-hinge,
        .wire-o .dimension-spine {
            display: none;
        }

        .cover-dimensions {
            display: -webkit-box;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            flex-direction: column;
        }
        .cover-dimensions--group {
            -webkit-box-flex: 1;
            flex-grow: 1;
        }
        .cover-dimensions--left,
        .cover-dimensions--right {
            width: 32px;
            padding: 32px 0 42px 0;
        }
        .cover-dimensions--top,
        .cover-dimensions--bottom {
            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            flex-direction: row;
            height: 32px;
            width: 100%;
        }
        .cover-dimensions--bottom {
            height: 42px;
        }

        .cover--elements {
            display: -webkit-box;
            display: flex;
            -webkit-box-flex: 1;
            flex-grow: 1;
        }

        .flaps, .hinge {
            background: -webkit-gradient(linear, left top, left bottom, from(#e0e0e0), color-stop(50%, #e0e0e0), color-stop(50%, #FFFFFF), to(#FFFFFF));
            background: linear-gradient(to bottom, #e0e0e0, #e0e0e0 50%, #FFFFFF 50%, #FFFFFF);
            background-size: 100% 5px;
        }
        .flaps, .page, .hinge, .spine, .bleeds--top, .bleeds--bottom {
            display: -webkit-box;
            display: flex;

            -webkit-box-pack: center;
            -webkit-justify-content: center;
            justify-content: center;

            -webkit-box-align: center;
            align-items: center;
        }
        .flaps, .page, .hinge, .spine {
            border-width: 1px;
            border-color: #212121;
            border-style: dashed;
            text-align: center;
        }
        .hinge > div, .spine > div {
            -webkit-writing-mode: vertical-lr;
            writing-mode: vertical-lr;
        }
        .page > div {
            min-width: 75px;
        }
        .bleeds {
            background-color: #f5f5f5;
            -webkit-box-flex: 0;
            flex-shrink: 1;
        }
        .bleeds--top,
        .bleeds--bottom {
            height: 24px;
        }
        .bleeds--rear,
        .bleeds--inner,
        .bleeds--front {
            width: 24px;
        }
        .bleeds--top {
            padding-right: 20%;
            -webkit-box-pack: end;
            justify-content: flex-end;
        }
        .bleeds--bottom {
            padding-left: 20%;
            -webkit-box-pack: start;
            justify-content: flex-start;
        }

        .flaps {
            width: 150px;
        }
        .flaps--left {
            border-right-width: 0;
        }
        .flaps--right {
            border-left-width: 0;
        }
        .page {
            -webkit-box-flex: 1;
            flex-grow: 1;
        }
        .hinge {
            width: 24px;
            -webkit-box-flex: 0;
            flex-shrink: 1;
            border-left-width: 0;
            border-right-width: 0;
        }
        .spine {
            width: 48px;
            -webkit-box-flex: 0;
            flex-shrink: 1;
        }


        .dimension-group,
        .dimension-arrow {
            display: -webkit-box;
            display: flex;
            -webkit-box-flex: 1;
            flex-grow: 1;
        }
        .dimension-text {
            font-size: 15px;
            font-weight: 500;
        }

        .dimension-arrow--left {
            border-left: 1px solid #000;
        }
        .dimension-arrow--right {
            border-right: 1px solid #000;
        }
        .dimension-arrow--top {
            border-top: 1px solid #000;
        }
        .dimension-arrow--bottom {
            border-bottom: 1px solid #000;
        }

        .cover-dimensions--left .dimension-group,
        .cover-dimensions--right .dimension-group {
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            margin: 1px 5px;
        }
        .cover-dimensions--left .dimension-group {
            -webkit-box-align: end;
            align-items: flex-end;
        }
        .cover-dimensions--left .dimension-arrow {
            width: 12px;
            border-left: 1px solid #000;
        }

        .cover-dimensions--right .dimension-group {
            -webkit-box-align: start;
            align-items: flex-start;
        }
        .cover-dimensions--right .dimension-arrow {
            width: 12px;
            border-right: 1px solid #000;
        }
        .cover-dimensions--right .dimension-group:first-child,
        .cover-dimensions--right .dimension-group:last-child {
            height: 24px;
            -webkit-box-flex: 0;
            flex-shrink: 0;

            -webkit-box-orient: horizontal;
            -webkit-box-direction: normal;
            -ms-flex-direction: row;
            flex-direction: row;
        }
        .cover-dimensions--right .dimension-group .dimension-arrow--top.dimension-arrow--bottom {
            height: 24px;
            margin-right: 1px;
        }

        .cover-dimensions--top .dimension-group {
            margin: 5px 1px;
            -webkit-box-align: end;
            align-items: flex-end;
        }
        .cover-dimensions--top .dimension-arrow {
            height: 12px;
            border-top: 1px solid #000;
        }
        .cover-dimensions--top .dimension-text {
            margin: 0 .5rem;
        }

        .cover-dimensions--bottom .dimension-group {
            margin: 5px 1px;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            flex-direction: column;
        }
        .cover-dimensions--bottom .dimension-arrow {
            height: 12px;
            border-bottom: 1px solid #000;
        }
        .cover-dimensions--bottom .dimension-text {
            text-align: center;
        }

        .cover-dimensions--bottom .dimension-bleed,
        .cover-dimensions--bottom .dimension-hinge,
        .cover-dimensions--bottom .dimension-bleed--inner,
        .cover-dimensions--bottom .dimension-spine,
        .cover-dimensions--bottom .dimension-flaps--left,
        .cover-dimensions--bottom .dimension-flaps--right {
            -webkit-box-flex: 0;
            flex-shrink: 1;
        }
        .cover-dimensions--bottom .dimension-bleed,
        .cover-dimensions--bottom .dimension-hinge,
        .cover-dimensions--bottom .dimension-bleed--inner {
            width: 22px;
        }
        .cover-dimensions--bottom .dimension-spine {
            width: 46px;
        }
        .cover-dimensions--bottom .dimension-flaps--left,
        .cover-dimensions--bottom .dimension-flaps--right {
            width: 146px;
        }
        .cover-dimensions--bottom .dimension-page {
            -webkit-box-flex: 1;
            flex-grow: 1;
        }
    </style>
    <style>
        .cover.wire-o .bleeds--inner {
            position: relative;
            border-right-width: 1px;
            border-right-color: #212121;
            border-right-style: solid;
            margin-top: -24px;
            margin-bottom: -24px;
            z-index: 1;
        }
        .cover.wire-o .bleeds--inner:before {
            background-position:  0 0, 0 0, 100% 0, 0 100%;
            background-size: 12px 100%, 100% 12px, 12px 100% , 100% 12px;
            background-repeat: no-repeat;
            background-image: -webkit-repeating-linear-gradient(top, #212121, #212121 12px, transparent 12px, transparent 24px);
            border-radius: 50px;
            content: '';
            left: -24px;
            height: 78%;
            width: 12px;
            position: absolute;
            top: 36px;
        }
        .cover.landscape.wire-o .bleeds--inner:before {
            height: 70%;
        }
        .cover.wire-o .bleeds--inner ~ .bleeds--inner {
            border-right-width: 0;
        }
        .cover.wire-o .bleeds--inner ~ .bleeds--inner:before {
            left: 36px;
        }
    </style>
</head>
<body>
    <div class="container">
        <header>
            @if($append_logo)
                @svg('images/logo-totem.svg', 'totem dark')
                <hr>
            @endif
            <h1>{{ __($title) }}</h1>
            <h5>{{ $book->name }}</h5>
        </header>
        <main>
            <div class="sam-book-calculator--preview" style="">
                <div class="cover
                    {{ $book->cover->flaps_left > 0 ? ' has-flaps--left ' : '' }}
                    {{ $book->cover->flaps_right > 0 ? ' has-flaps--right ' : '' }}
                    {{ $book->cover->page_width_rear > $book->cover->page_height ? ' landscape ' : '' }}
                    {{ $book->cover->bleeds_inner > 0 ? ' wire-o ' : '' }}
                    {{ $book->cover->spine === 0.0 ? ' stapled ' : '' }}
                    {{ $book->cover->big > 0 || $book->cover->hinge > 0 ? ' has-big ' : '' }}
                        ">
                    <div class="cover-dimensions cover-dimensions--left">
                        <div class="dimension-group">
                            <div class="dimension-arrow dimension-arrow--top"></div>
                            <div class="dimension-text">{{ $book->cover->total_height }}</div>
                            <div class="dimension-arrow dimension-arrow--bottom"></div>
                        </div>
                    </div>
                    <div class="cover-dimensions cover-dimensions--group">
                        <div class="cover-dimensions cover-dimensions--top">
                            @if($book->cover->bleeds_inner > 0)
                                <div class="dimension-group">
                                    <div class="dimension-arrow dimension-arrow--left"></div>
                                    <div class="dimension-text">{{ $book->cover->bleeds + $book->cover->bleeds_inner + $book->cover->page_width_front + $book->cover->flaps_left }}</div>
                                    <div class="dimension-arrow dimension-arrow--right"></div>
                                </div>
                                <div class="dimension-group">
                                    <div class="dimension-arrow dimension-arrow--left"></div>
                                    <div class="dimension-text">{{ $book->cover->bleeds + $book->cover->bleeds_inner + $book->cover->page_width_front + $book->cover->flaps_right }}</div>
                                    <div class="dimension-arrow dimension-arrow--right"></div>
                                </div>
                            @else
                                <div class="dimension-group">
                                    <div class="dimension-arrow dimension-arrow--left"></div>
                                    <div class="dimension-text">{{ $book->cover->total_width }}</div>
                                    <div class="dimension-arrow dimension-arrow--right"></div>
                                </div>
                            @endif
                        </div>
                        <div class="bleeds bleeds--top">
                            <div>{{ __('Bleed') }}</div>
                        </div>
                        <div class="cover--elements">
                            <div class="bleeds bleeds--front"></div>
                            <div class="flaps flaps--left">
                                <div>{{ __('Rear flap') }}</div>
                            </div>
                            <div class="page">
                                <div>{{ __('Rear') }}</div>
                            </div>
                            <div class="bleeds bleeds--inner"></div>
                            <div class="hinge">
                                <div>{{ __($book->cover->big > 0 ? 'Crease' : 'Hinge') }}</div>
                            </div>
                            <div class="spine">
                                <div>{{ __('Spine') }}</div>
                            </div>
                            <div class="hinge">
                                <div>{{ __($book->cover->big > 0 ? 'Crease' : 'Hinge') }}</div>
                            </div>
                            <div class="bleeds bleeds--inner"></div>
                            <div class="page">
                                <div>{{ __('Front') }}</div>
                            </div>
                            <div class="flaps flaps--right">
                                <div>{{ __('Front flap') }}</div>
                            </div>
                            <div class="bleeds bleeds--rear"></div>
                        </div>
                        <div class="bleeds bleeds--bottom">
                            <div>{{ __('Bleed') }}</div>
                        </div>
                        <div class="cover-dimensions cover-dimensions--bottom">
                            <div class="dimension-group dimension-bleed">
                                <div class="dimension-arrow dimension-arrow--left dimension-arrow--right"></div>
                                <div class="dimension-text">{{ $book->cover->bleeds }}</div>
                            </div>
                            <div class="dimension-group dimension-flaps--left">
                                <div class="dimension-arrow dimension-arrow--left dimension-arrow--right"></div>
                                <div class="dimension-text">{{ $book->cover->flaps_left }}</div>
                            </div>
                            <div class="dimension-group dimension-page">
                                <div class="dimension-arrow dimension-arrow--left dimension-arrow--right"></div>
                                <div class="dimension-text">{{ $book->cover->page_width_rear }}</div>
                            </div>
                            <div class="dimension-group dimension-bleed--inner">
                                <div class="dimension-arrow dimension-arrow--left dimension-arrow--right"></div>
                                <div class="dimension-text">{{ $book->cover->bleeds_inner }}</div>
                            </div>
                            <div class="dimension-group dimension-hinge">
                                <div class="dimension-arrow dimension-arrow--left dimension-arrow--right"></div>
                                <div class="dimension-text">{{ $book->cover->big > 0 ? $book->cover->big : $book->cover->hinge }}</div>
                            </div>
                            <div class="dimension-group dimension-spine">
                                <div class="dimension-arrow dimension-arrow--left dimension-arrow--right"></div>
                                <div class="dimension-text">{{ $book->cover->spine }}</div>
                            </div>
                            <div class="dimension-group dimension-hinge">
                                <div class="dimension-arrow dimension-arrow--left dimension-arrow--right"></div>
                                <div class="dimension-text">{{ $book->cover->big > 0 ? $book->cover->big : $book->cover->hinge }}</div>
                            </div>
                            <div class="dimension-group dimension-bleed--inner">
                                <div class="dimension-arrow dimension-arrow--left dimension-arrow--right"></div>
                                <div class="dimension-text">{{ $book->cover->bleeds_inner }}</div>
                            </div>
                            <div class="dimension-group dimension-page">
                                <div class="dimension-arrow dimension-arrow--left dimension-arrow--right"></div>
                                <div class="dimension-text">{{ $book->cover->page_width_front }}</div>
                            </div>
                            <div class="dimension-group dimension-flaps--right">
                                <div class="dimension-arrow dimension-arrow--left dimension-arrow--right"></div>
                                <div class="dimension-text">{{ $book->cover->flaps_right }}</div>
                            </div>
                            <div class="dimension-group dimension-bleed">
                                <div class="dimension-arrow dimension-arrow--left dimension-arrow--right"></div>
                                <div class="dimension-text">{{ $book->cover->bleeds }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="cover-dimensions cover-dimensions--right">
                        <div class="dimension-group">
                            <div class="dimension-arrow dimension-arrow--top dimension-arrow--bottom"></div>
                            <div class="dimension-text">{{ $book->cover->bleeds }}</div>
                        </div>
                        <div class="dimension-group">
                            <div class="dimension-arrow dimension-arrow--top"></div>
                            <div class="dimension-text">{{ $book->cover->page_height }}</div>
                            <div class="dimension-arrow dimension-arrow--bottom"></div>
                        </div>
                        <div class="dimension-group">
                            <div class="dimension-arrow dimension-arrow--top dimension-arrow--bottom"></div>
                            <div class="dimension-text">{{ $book->cover->bleeds }}</div>
                        </div>
                    </div>
                </div>
            </div>
            <h6>
                {{ __('* Measurements are made in millimeters.') }}
            </h6>
            <div class="description">
                @if ($book->validation->get('perforation'))
                    <h6>{{ __($book->validation->get('perforation')[0]) }}</h6>
                @endif
                <h3>
                    {{ __('Basic data of the book') }}
                </h3>
                <table class="table-borderless">
                    <tbody>
                        <tr>
                            <td>{{ __('Block size') }}</td>
                            <td>{{ "{$book->binding->width} x {$book->binding->height} mm" }}</td>
                        </tr>
                        <tr>
                            <td>{{ __('Page count') }}</td>
                            <td>{{ $book->binding->pages_total }}</td>
                        </tr>
                        <tr>
                            <td>{{ __('Bind type') }}</td>
                            <td>{{ __($book->binding->label) }}</td>
                        </tr>
                        @if ($book->binding->cardboard_allowed === true)
                        <tr>
                            <td>{{ __('Spine type') }}</td>
                            <td>{{ __(\Rudashi\BookGenerator\App\Enums\SpineType::getDescription($book->binding->spine)) }}</td>
                        </tr>
                        <tr>
                            <td>{{ __('Cardboard thickness') }}</td>
                            <td>{{ "{$book->binding->cardboard} mm" }}</td>
                        </tr>
                        @endif
                        <tr>
                            <td colspan="2">
                                <h3>
                                    {{ __('Paper type') }}
                                </h3>
                            </td>
                        </tr>
                        @foreach($book->binding->papers as $paper)
                            <tr>
                                <td>{{ $paper->pages }} {{ __('pages') }}</td>
                                <td>{{ __($paper->name) }} {{ "{$paper->weight} g/m vol. {$paper->volume_name}" }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="2">
                                <h3>
                                    {{ __('Result data') }}
                                </h3>
                            </td>
                        </tr>
                        <tr>
                            <td>{{ __('Total size') }}</td>
                            <td>{{ "{$book->cover->total_width} x {$book->cover->total_height} mm" }}</td>
                        </tr>
                        <tr>
                            <td>{{ __('Spine width') }}</td>
                            <td>{{ "{$book->cover->spine} mm" }}</td>
                        </tr>
                        <tr>
                            <td>{{ __('Bleeds') }}</td>
                            <td>{{ "{$book->cover->bleeds} mm" }}</td>
                        </tr>
                        @if($book->cover->flaps_left > 0)
                            <tr>
                                <td>{{ __('Rear flap') }}</td>
                                <td>{{ "{$book->cover->flaps_left} mm" }}</td>
                            </tr>
                        @endif
                        @if($book->cover->flaps_right > 0)
                            <tr>
                                <td>{{ __('Front flap') }}</td>
                                <td>{{ "{$book->cover->flaps_right} mm" }}</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </main>
    </div>
</body>
</html>
