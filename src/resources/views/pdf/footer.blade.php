<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Lato&subset=latin-ext');

        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Lato', sans-serif;
            font-weight: 400;
            font-size: 18px;
            height: 100vh;
            margin: 0;
        }
        p {
            text-align: center;
        }
    </style>
</head>
<body>
    <p>
        {{ __('Customer is obliged to deliver materials made in accordance with standards include in') }}<br>
        <a href="{{ __('https://www.totem.com.pl/en/guide/') }}" target="_blank"><b>{{ __('Guidelines for preparing materials for digital printing.') }}</b></a>
    </p>
    <p>
        <a href="{{ __('https://www.totem.com.pl/en/video-tutorials') }}" target="_blank">{{ __('How-to videos (preparing files for printing)') }}</a>
    </p>
</body>
</html>