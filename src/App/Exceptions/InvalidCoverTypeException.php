<?php

namespace Rudashi\BookGenerator\App\Exceptions;

use Throwable;
use InvalidArgumentException;

class InvalidCoverTypeException extends InvalidArgumentException
{

    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message ?: __('Invalid Cover type.'), $code, $previous);
    }

}
