<?php

namespace Rudashi\BookGenerator\App\Exceptions;

use Throwable;
use InvalidArgumentException;

class InvalidBindTypeException extends InvalidArgumentException
{

    public function __construct($message = '', $code = 400, Throwable $previous = null)
    {
        parent::__construct($message ?: __('Invalid Binding type.'), $code, $previous);
    }

}
