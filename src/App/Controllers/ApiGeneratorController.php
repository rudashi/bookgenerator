<?php

namespace Rudashi\BookGenerator\App\Controllers;

use Barryvdh\Snappy\PdfWrapper;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Rudashi\BookGenerator\App\Classes\Book;
use Rudashi\BookGenerator\App\Classes\Contracts\CoverInterface;
use Rudashi\BookGenerator\App\Classes\Form;
use Rudashi\BookGenerator\App\Classes\Parameters\Paper;
use Rudashi\BookGenerator\App\Requests\BookApiRequest;
use Rudashi\BookGenerator\App\Requests\BookRequest;
use Rudashi\BookGenerator\App\Resources\BookResource;
use Rudashi\BookGenerator\App\Resources\FormResource;
use Rudashi\BookGenerator\Events\CoverSchemeGenerated;
use Rudashi\PapersLibrary\App\Repositories\Contracts\PaperRepositoryInterface;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Totem\SamCore\App\Controllers\ApiController;

class ApiGeneratorController extends ApiController
{

    private Application $app;

    public function __construct(Book $book, Application $application)
    {
        $this->repository = $book;
        $this->app = $application;
    }

    public function createBook(BookApiRequest $request): BookResource
    {
        $this->app->setLocale($request->input('language'));

        return new BookResource($this->repository->create($request));
    }

    public function publicInformation(Request $request): \Totem\SamCore\App\Resources\ApiResource
    {
        if ($request->has('default')) {
            return new FormResource(new Form($this->repository));
        }

        throw new MethodNotAllowedHttpException([], 'The GET method is not supported for this route.');
    }

    public function pdf(PaperRepositoryInterface $paperRepository, BookApiRequest $request): \Illuminate\Http\Response
    {
        $this->app->setLocale($request->input('language'));

        $book = $this->repository->create($request);
        $papers = $paperRepository->findByIds($book->binding->papers->pluck('paper_id')->toArray());
        $book->binding->papers->transform(static function (Paper $paper) use ($papers) {
            if ($paper->volume_name === null) {
                $paper->setVolumeName($paper->volume);
            }
            $paper->setName($papers->firstWhere('id', $paper->paper_id)->name);
            return $paper;
        });

        $this->logAction($book, $request);

        $pdf = $this->newPDF()->loadView('book-generator::pdf.scheme', [
            'language' => $request->input('language'),
            'title' => $book->cover->label . ' scheme',
            'book' => $book,
            'append_logo' => !$request->input('no_logo'),
        ]);
        if (!$request->input('no_logo')) {
            $pdf->setOption('footer-html', view('book-generator::pdf.footer'));
        }
        return $pdf->download($this->setPDFName($book->cover, $book->binding->label, $book->name) . '.pdf');
    }

    public function pdfOutline(BookApiRequest $request): \Illuminate\Http\Response
    {
        $book = $this->repository->create($request);

        $this->logAction($book, $request);

        return $this->newPDF()->setOptions([
            'page-width' => $book->cover->total_width . 'mm',
            'page-height' => $book->cover->total_height + 0.01 . 'mm',
            'dpi' => 96,
            'margin-bottom' => '0',
            'margin-top' => '0',
            'margin-left' => '0',
            'margin-right' => '0',
            'zoom' => 1.33
        ])
        ->loadView('book-generator::pdf.outline', [
            'cover' => $book->cover,
        ])
        ->download($this->setPDFName($book->cover, $book->binding->label, $book->name) . '.pdf');
    }

    private function logAction(Book $book, BookRequest $request): void
    {
        event(new CoverSchemeGenerated($book, $request));
    }

    private function newPDF(): PdfWrapper
    {
        return $this->app->make(PdfWrapper::class);
    }

    private function setPDFName(CoverInterface $cover, string $bind, string $job_name = null): string
    {
        if ($job_name) {
            return Str::slug(__($cover->label . ' scheme') . ' - ' . $job_name);
        }
        return Str::slug(__($bind) . ' - ' . __($cover->label . ' scheme'));
    }

}
