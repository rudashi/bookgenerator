<?php

namespace Rudashi\BookGenerator\App\Enums;

use BenSampo\Enum\Enum;

class CoverType extends Enum
{

    public const COVER  = '0';
    public const JACKET = '1';

}
