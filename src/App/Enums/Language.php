<?php

namespace Rudashi\BookGenerator\App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

class Language extends Enum implements LocalizedEnum
{
    public const POLISH     = 'pl';
    public const ENGLISH    = 'en';
    public const GERMAN     = 'de';
    public const FRENCH     = 'fr';
    public const SPANISH    = 'sp';
    public const PORTUGUESE = 'pt';
    public const SWEDISH    = 'sv';

    public static function getLocalizationKey(): string
    {
        return 'book-generator::enums.' . self::class;
    }

    public static function codeArray(): array
    {
        return [
            'pl' => self::POLISH,
            'en' => self::ENGLISH,
            'en-au' => self::ENGLISH,
            'en-bz' => self::ENGLISH,
            'en-ca' => self::ENGLISH,
            'en-cb' => self::ENGLISH,
            'en-gb' => self::ENGLISH,
            'en-in' => self::ENGLISH,
            'en-ie' => self::ENGLISH,
            'en-jm' => self::ENGLISH,
            'en-nz' => self::ENGLISH,
            'en-ph' => self::ENGLISH,
            'en-za' => self::ENGLISH,
            'en-tt' => self::ENGLISH,
            'en-us' => self::ENGLISH,
            'de' => self::GERMAN,
            'de-at' => self::GERMAN,
            'de-de' => self::GERMAN,
            'de-li' => self::GERMAN,
            'de-lu' => self::GERMAN,
            'de-ch' => self::GERMAN,
            'fr' => self::FRENCH,
            'fr-be' => self::FRENCH,
            'fr-ca' => self::FRENCH,
            'fr-fr' => self::FRENCH,
            'fr-lu' => self::FRENCH,
            'fr-ch' => self::FRENCH,
            'sp' => self::SPANISH,
            'es' => self::SPANISH,
            'es-ar' => self::SPANISH,
            'es-bo' => self::SPANISH,
            'es-cl' => self::SPANISH,
            'es-co' => self::SPANISH,
            'es-cr' => self::SPANISH,
            'es-do' => self::SPANISH,
            'es-ec' => self::SPANISH,
            'es-sv' => self::SPANISH,
            'es-gt' => self::SPANISH,
            'es-hn' => self::SPANISH,
            'es-mx' => self::SPANISH,
            'es-ni' => self::SPANISH,
            'es-pa' => self::SPANISH,
            'es-py' => self::SPANISH,
            'es-pe' => self::SPANISH,
            'es-pr' => self::SPANISH,
            'es-es' => self::SPANISH,
            'es-uy' => self::SPANISH,
            'es-ve' => self::SPANISH,
            'pt' => self::PORTUGUESE,
            'pt-br' => self::PORTUGUESE,
            'pt-pt' => self::PORTUGUESE,
            'sv' => self::SWEDISH,
            'sv-fi' => self::SWEDISH,
            'sv-se' => self::SWEDISH,
        ];
    }

}
