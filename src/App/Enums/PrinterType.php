<?php

namespace Rudashi\BookGenerator\App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;
use InvalidArgumentException;

class PrinterType extends Enum implements LocalizedEnum
{

    public const TONER      = 'toner';
    public const INK_JET    = 'ink-jet';

    public static function getMachines(string $value): array
    {
        switch ($value) {
            case self::TONER :
                return [
                    'RICOH',
                    'VARIO PRINT',
                    'KM-1'
                ];
            case self::INK_JET :
                return [
                    'SCREEN',
                    'COLOR STREAM',
                ];
            default:
                throw new InvalidArgumentException('Unrecognized printer type.');
        }
    }

}
