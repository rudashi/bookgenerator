<?php

namespace Rudashi\BookGenerator\App\Enums;

use BenSampo\Enum\Enum;

class BookDefaults extends Enum
{

    public const FLAPS_MIN      = 40;
    public const FLAPS_DIFF     = 15;

}
