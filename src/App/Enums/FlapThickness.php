<?php

namespace Rudashi\BookGenerator\App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

class FlapThickness extends Enum implements LocalizedEnum
{

    public const FLAP_0     = 0;
    public const FLAP_1     = 1;
    public const FLAP_2     = 2;
    public const FLAP_3     = 3;

}
