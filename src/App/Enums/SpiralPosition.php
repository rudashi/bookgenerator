<?php

namespace Rudashi\BookGenerator\App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

class SpiralPosition extends Enum implements LocalizedEnum
{

    public const LONG_SIDE   = 0;
    public const SHORT_SIDE  = 1;

    public static function getLocalizationKey(): string
    {
        return 'book-generator::enums.' . self::class;
    }

}
