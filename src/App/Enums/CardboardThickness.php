<?php

namespace Rudashi\BookGenerator\App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

class CardboardThickness extends Enum implements LocalizedEnum
{

    public const CARDBOARD_0_8  = 0.8;
    public const CARDBOARD_1    = 1;
    public const CARDBOARD_1_5  = 1.5;
    public const CARDBOARD_2    = 2;
    public const CARDBOARD_2_5  = 2.5;
    public const CARDBOARD_3    = 3;

}
