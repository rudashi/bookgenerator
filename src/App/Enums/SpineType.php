<?php

namespace Rudashi\BookGenerator\App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

class SpineType extends Enum implements LocalizedEnum
{

    public const FLAT   = 0;
    public const ROUND  = 1;

    public static function getLocalizationKey(): string
    {
        return 'book-generator::enums.' . static::class;
    }

}
