<?php

namespace Rudashi\BookGenerator\App\Requests;

use Illuminate\Validation\Rule;
use Rudashi\BookGenerator\App\Enums\SpiralPosition;
use Rudashi\Profis\Enums\BindType;
use Rudashi\BookGenerator\App\Enums\BookDefaults;
use Rudashi\BookGenerator\App\Enums\CardboardThickness;
use Rudashi\BookGenerator\App\Enums\CoverType;
use Rudashi\BookGenerator\App\Enums\FlapThickness;
use Rudashi\BookGenerator\App\Enums\Language;
use Rudashi\BookGenerator\App\Enums\PrinterType;
use Rudashi\BookGenerator\App\Enums\SpineType;
use Totem\SamCore\App\Requests\BaseRequest;

class BookRequest extends BaseRequest
{

    public function rules(): array
    {
        $flapsMax = in_array($this->input('bind_type'), ['4', '5'], true) ? 5 : BookDefaults::FLAPS_DIFF;

        return [
            'cover_type' => [
                'required',
                Rule::in(CoverType::toArray())
            ],

            'width' => [
                'required',
                'integer',
                'min:0',
            ],
            'height' => [
                'required',
                'integer',
                'min:0',
            ],
            'bind_type' => [
                'required',
                Rule::in(BindType::toArray()),
            ],

            'paper' => [
                'required',
                'array'
            ],
            'paper.*.pages' => [
                'required',
                'integer',
            ],
            'paper.*.type' => [
                'required',
                'integer',
            ],
            'paper.*.weight' => [
                'required',
                'integer',
            ],
            'paper.*.volume' => [
                'required',
                'numeric',
            ],

            'spine' => [
                Rule::in(SpineType::toArray())
            ],
            'spine_encore' => [
                Rule::in([ 1,0 ])
            ],
            'cardboard' => [
                'numeric',
                'min:'.CardboardThickness::CARDBOARD_0_8,
                'max:'.CardboardThickness::CARDBOARD_3,
            ],

            'flaps' => [
                Rule::in([ 1,0 ])
            ],
            'flaps_symmetric' => [
                Rule::in([ 1,0 ])
            ],
            'flaps_width' => $this->input('flaps') && $this->input('flaps_symmetric') ? [
                Rule::requiredIf($this->input('flaps') === 1),
                Rule::requiredIf($this->input('flaps_symmetric') === 1),
                'numeric',
                'min:'.BookDefaults::FLAPS_MIN,
                'max:'.$flapsMax,
            ] : [],
            'flaps_left' => $this->input('flaps') && !$this->input('flaps_symmetric') ? [
                Rule::requiredIf($this->input('flaps')),
                'numeric',
                $this->input('flaps_left') === 0 ? null : 'min:'.BookDefaults::FLAPS_MIN,
                $this->input('spine_custom') ? null : 'max:'.$flapsMax,
            ] : [],
            'flaps_right' => $this->input('flaps') && !$this->input('flaps_symmetric') ? [
                Rule::requiredIf($this->input('flaps')),
                'numeric',
                $this->input('flaps_right') === 0 ? null : 'min:'.BookDefaults::FLAPS_MIN,
                $this->input('spine_custom') ? null : 'max:'.$flapsMax,
            ] : [],
            'flaps_thickness' => $this->input('flaps') ? [
                Rule::requiredIf($this->input('flaps')),
                Rule::in(FlapThickness::toArray()),
            ] : [],

            'printer' => [
                'required',
                Rule::in(PrinterType::toArray())
            ],
            'job_name' => [
                'string',
                'nullable'
            ],
            'show_big' => [
                'numeric',
                Rule::in([ 1,0 ])
            ],
            'no_logo' => [
                'numeric',
                Rule::in([ 1,0 ])
            ],
            'language' => [
                'required',
                Rule::in(Language::toArray())
            ],

            'spiral.color' => [
                Rule::requiredIf($this->filled('spiral.position')),
            ],
            'spiral.position' => [
                Rule::requiredIf($this->filled('spiral.color')),
                Rule::in(SpiralPosition::toArray())
            ],
        ];
    }

    protected function prepareForValidation(): void
    {
        if ($this->has('cover_type') === false) {
            $this->merge(['cover_type' => CoverType::COVER ]);
        }
        if ($this->has('language') === false) {
            $this->merge(['language' => Language::POLISH ]);
        }
        if ($this->has('spine_encore') === false) {
            $this->merge(['spine_encore' => 1]);
        }
        if ($this->has('spine_custom') === false) {
            $this->merge(['spine_custom' => null]);
        }
        if ($this->has('flaps') === false) {
            $this->merge(['flaps' => 0]);
        }
        if ($this->has('no_logo') === false) {
            $this->merge(['no_logo' => 0]);
        }
        if ($this->input('cover_type') === (int) CoverType::JACKET) {
            $this->merge(['flaps' => 1]);
            $this->merge(['flaps_symmetric' => 1]);
        }
    }

    public function messages(): array
    {
        return [
            'cover_type.required' => __('Invalid Cover type.'),
            'cover_type.in' => __('Invalid Cover type.'),
            'bind_type.required' => __('Invalid Binding type.'),
            'bind_type.in' => __('Invalid Binding type.'),

            'paper.*.pages.required' => __('The :attribute field is required.', [ 'attribute' => __('Paper count')]),
            'paper.*.type.required' => __('The :attribute field is required.', [ 'attribute' => __('Paper type')]),
            'paper.*.weight.required' => __('The :attribute field is required.', [ 'attribute' => __('Paper weight')]),
            'paper.*.volume.required' => __('The :attribute field is required.', [ 'attribute' => __('Paper volume')]),

            'language.in' => __('Unsupported language selected.'),

            'required_if' => __('The :attribute field is required when :other field is provided.'),
        ];
    }

    public function attributes(): array
    {
        return [
            'width' => __('Width'),
            'height' => __('Height'),
            'printer' => __('Printer'),

            'paper' => __('Paper'),

            'flaps' => __('Cover flaps'),
            'flaps_width' => __('Flaps width'),
            'flaps_left' => __('Rear flap'),
            'flaps_right' => __('Front flap'),

            'spiral.color' => __('Spiral color'),
            'spiral.position' => __('Spiral position'),
        ];
    }

}
