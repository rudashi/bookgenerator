<?php

namespace Rudashi\BookGenerator\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property \Rudashi\BookGenerator\App\Classes\Cover $resource
 */
class CoverResource extends ApiResource
{

    public function toArray($request): array
    {
        return [
            'page' => [
                'rear'      => number_format($this->resource->page_width_rear,1),
                'front'     => number_format($this->resource->page_width_front,1),
                'height'    => number_format($this->resource->page_height),
            ],
            'hinge'         => number_format($this->resource->hinge),
            'big'           => number_format($this->resource->big),
            'spine'         => number_format($this->resource->spine, 1),
            'bleeds'        => number_format($this->resource->bleeds),
            'bleeds_inner'  => number_format($this->resource->bleeds_inner),
            'flaps' => [
                'rear'      => number_format($this->resource->flaps_left),
                'front'     => number_format($this->resource->flaps_right),
            ],
            'total' => [
                'width'     => number_format($this->resource->total_width,1),
                'height'    => number_format($this->resource->total_height),
            ],
            'net' => [
                'width'     => number_format($this->resource->net_width,1),
                'height'    => number_format($this->resource->net_height),
            ],
            'weight'        => number_format($this->resource->weight,2),
        ];
    }

}
