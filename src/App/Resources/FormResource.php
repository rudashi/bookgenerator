<?php

namespace Rudashi\BookGenerator\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property \Rudashi\BookGenerator\App\Classes\Form $resource
 */
class FormResource extends ApiResource
{

    public function toArray($request): array
    {
        return [
            'printers'          => $this->resource->getPrinterTypes(),
            'binds'             => $this->resource->getBindTypes(),
            'spines'            => $this->resource->getSpineTypes(),
            'cardboard'         => $this->resource->getCardboardTypes(),
            'flap_thickness'    => $this->resource->getFlapsThickness(),
            'languages'         => $this->resource->getLanguages(),
            'spiral'            => $this->resource->getSpiralPosition(),
        ];
    }

}
