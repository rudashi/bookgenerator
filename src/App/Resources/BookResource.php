<?php

namespace Rudashi\BookGenerator\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property \Rudashi\BookGenerator\App\Classes\Book $resource
 */
class BookResource extends ApiResource
{

    public function toArray($request): array
    {
        return [
            'messages'      => $this->resource->validation->getMessages(),
            'name'          => $this->resource->name,
            'binding'       => $this->resource->binding->label,
            'cover'         => $this->resource->cover->label,
            'pages'         => $this->resource->binding->papers->sum('pages'),
            'parameters'    => CoverResource::make($this->resource->cover),
            'block'         => number_format($this->resource->binding->block_thickness, 2),
            'thickness'     => number_format($this->resource->binding->book_thickness, 2),
            'weight'        => number_format($this->resource->weight,2),
            'spiral'        => $this->resource->binding->spiral,
        ];
    }

}
