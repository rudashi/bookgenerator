<?php

namespace Rudashi\BookGenerator\App;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Rudashi\BookGenerator\App\Enums\Language;

class LocalizationMiddleware
{

    public function handle(Request $request, Closure $next)
    {
        if ($request->hasHeader('Accept-Language')) {
            $header = strtolower($request->header('Accept-Language'));

            if (array_key_exists($header, Language::codeArray())) {
                App::setLocale(Language::codeArray()[$header]);
            }
        }
        return $next($request);
    }

}
