<?php

namespace Rudashi\BookGenerator\App\Classes;

use Rudashi\BookGenerator\App\Classes\Parameters\Weight;
use Rudashi\BookGenerator\App\Classes\Contracts\CoverInterface;
use Rudashi\BookGenerator\App\Classes\Contracts\BindInterface;

abstract class Cover implements CoverInterface
{

    protected ?BindInterface $binding = null;
    public ?string $label = null;

    public float $page_width_rear;
    public float $page_width_front;
    public int $page_height;
    public int $hinge;
    public int $big;
    public float $spine;
    public float $cardboard;
    public int $bleeds;
    public int $bleeds_inner;
    public int $flaps_left;
    public int $flaps_right;
    public float $total_width;
    public int $total_height;
    public float $net_width;
    public int $net_height;
    public float $weight;

    public function __construct(Bind $binding = null)
    {
        $this->setBinding($binding);

        if ($this->label === null) {
            throw new \LogicException(\get_class($this) . ' must have a $label');
        }
    }

    public function setBinding(?BindInterface $binding): self
    {
        $this->binding = $binding;

        return $this;
    }

    public function create(BindInterface $binding = null): self
    {
        if ($binding) {
            $this->setBinding($binding);
        }

        $this->build();

        return $this;
    }

    public function setDimensions(): self
    {
        $this->setNetWidth();
        $this->setNetHeight();

        $this->setTotalWidth();
        $this->setTotalHeight();

        return $this;
    }

    public function setPageWidthRear(BindInterface $binding): self
    {
        $this->page_width_rear = (float) $binding->doPageWidthRear(
            $binding->width,
            $binding->flaps
        );
        return $this;
    }

    public function setPageWidthFront(BindInterface $binding): self
    {
        $this->page_width_front = (float) $binding->doPageWidthFront(
            $binding->width,
            $binding->flaps
        );
        return $this;
    }

    public function setPageHeight(BindInterface $binding): self
    {
        $this->page_height = $binding->doPageHeight(
            $binding->height
        );
        return $this;
    }

    public function setHinge(BindInterface $binding): self
    {
        $this->hinge = $binding->doHinge();

        return $this;
    }

    public function setBig(BindInterface $binding): self
    {
        $this->big = $binding->doBig();

        return $this;
    }

    public function setSpine(BindInterface $binding): self
    {
        $this->spine = $binding->isSpineCustom()? $binding->getCustomSpine() : $binding->doSpine();

        return $this;
    }

    public function setCardboard(BindInterface $binding): self
    {
        $this->cardboard = $binding->doCardboard(
            $binding->cardboard
        );

        return $this;
    }

    public function setBleeds(BindInterface $binding): self
    {
        $this->bleeds = $binding->doBleeds();

        return $this;
    }

    public function setBleedsInner(BindInterface $binding): self
    {
        $this->bleeds_inner = $binding->doBleedsInner();

        return $this;
    }

    public function setFlapBack(BindInterface $binding): self
    {
        $this->flaps_left = $binding->doFlapRear(
            $binding->flaps
        );

        return $this;
    }

    public function setFlapFront(BindInterface $binding): self
    {
        $this->flaps_right = $binding->doFlapFront(
            $binding->flaps
        );

        return $this;
    }

    abstract public static function weightMultiplier(): int;

    public function setTotalWidth(): self
    {
        $this->total_width = $this->net_width + ($this->bleeds * 2) + ($this->bleeds_inner * 2);

        return $this;
    }

    public function setTotalHeight(): self
    {
        $this->total_height = $this->net_height + ($this->bleeds * 2);

        return $this;
    }

    public function setNetWidth(): self
    {
        $this->net_width = $this->flaps_left + $this->flaps_right + $this->spine
            + $this->page_width_rear + $this->page_width_front
            + (2 * ($this->hinge + $this->big));

        return $this;
    }

    public function setNetHeight(): self
    {
        $this->net_height = $this->page_height;

        return $this;
    }

    public function setWeight(BindInterface $binding): self
    {
        $this->weight = $binding->doWeight(
            new Weight($this, $binding)
        );

        return $this;
    }

    private function build(): void
    {
        $this->setPageWidthRear($this->binding);
        $this->setPageWidthFront($this->binding);
        $this->setPageHeight($this->binding);

        $this->setHinge($this->binding);
        $this->setBig($this->binding);
        $this->setSpine($this->binding);
        $this->setCardboard($this->binding);
        $this->setBleeds($this->binding);
        $this->setBleedsInner($this->binding);

        $this->setFlapBack($this->binding);
        $this->setFlapFront($this->binding);

        $this->setDimensions();

        $this->setWeight($this->binding);
    }

}
