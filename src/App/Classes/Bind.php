<?php

namespace Rudashi\BookGenerator\App\Classes;

use InvalidArgumentException;
use Illuminate\Support\Collection;
use Rudashi\BookGenerator\App\Classes\Parameters\Spiral;
use Rudashi\BookGenerator\App\Enums\SpineType;
use Rudashi\BookGenerator\App\Enums\PrinterType;
use Rudashi\BookGenerator\App\Classes\Parameters\Flaps;
use Rudashi\BookGenerator\App\Classes\Parameters\Paper;
use Rudashi\BookGenerator\App\Classes\Parameters\Weight;
use Rudashi\BookGenerator\App\Classes\Contracts\BindInterface;

abstract class Bind implements BindInterface
{

    public const ACCEPTED = 1;

    public const WIDTH_MIN = 100;
    public const WIDTH_MAX = 305;
    public const HEIGHT_MIN = 100;
    public const HEIGHT_MAX = 350;
    public const BLOCK_MIN = 2;
    public const BLOCK_MAX = 62;

    public const BLEED = 5;
    public const HINGE = 0;
    public const BIG = 0;

    protected ?BookParameters $parameters = null;

    public string $label;
    protected bool $flaps_allowed;
    protected bool $cardboard_allowed;
    protected bool $spine_allowed;
    protected bool $big_allowed;
    protected bool $spiral_color_allowed;
    protected bool $spiral_position_allowed;
    protected bool $dust_jacket_allowed;

    public int $width;
    public int $height;

    public Collection $papers;

    public int $spine;
    public int $spine_encore;
    public float $cardboard;

    public Flaps $flaps;
    public ?Spiral $spiral = null;

    public string $printer;
    public int $show_big;

    public float $block_thickness;
    public float $book_thickness;
    public int $pages_total;
    public int $pages_min = 0;
    public int $pages_max = 0;

    public float $weight;

    public function __construct(BookParameters $parameters = null)
    {
        $this->setBookParameters($parameters);

        $this->verifyBindParameters();
    }

    abstract public function validateParameters(Validate $validate): void;

    public function create(BookParameters $parameters = null): self
    {
        if ($parameters) {
            $this->setBookParameters($parameters);
        }

        $this->build();

        return $this;
    }

    public function getBasicInformation(): array
    {
        return  [
            'label' => __($this->label),
            'flaps_allowed' => $this->flaps_allowed,
            'cardboard_allowed' => $this->cardboard_allowed,
            'spine_allowed' => $this->spine_allowed,
            'big_allowed' => $this->big_allowed,
            'spiral_color_allowed' => $this->spiral_color_allowed,
            'spiral_position_allowed' => $this->spiral_position_allowed,
            'dust_jacket_allowed' => $this->dust_jacket_allowed,
        ];
    }

    public function setWidth(int $width = 0): self
    {
        $this->width = $width;

        return $this;
    }

    public function setHeight(int $height = 0): self
    {
        $this->height = $height;

        return $this;
    }

    public function setPapers(Collection $collection): self
    {
        $this->papers = $collection->map(function(Paper $paper) {
            return $paper->setThickness($this->doSurplusBlock($paper))->setMass($this->width, $this->height);
        });

        return $this;
    }

    public function setSpine(int $spine = null): self
    {
        $this->spine = $spine;

        return $this;
    }

    public function setSpineEncore(int $spine_encore = null): self
    {
        $this->spine_encore = $spine_encore === self::ACCEPTED;

        return $this;
    }

    public function setCardboard(float $cardboard = null): self
    {
        $this->cardboard = $this->doCardboard($cardboard ?: 0);

        return $this;
    }

    public function setFlaps(Flaps $flaps): self
    {
        $this->flaps = $flaps->validate();

        return $this;
    }

    public function setSpiral(Spiral $spiral): self
    {
        $this->spiral = $this->doSpiral($spiral);

        return $this;
    }

    public function setPrinter(string $printer = null): self
    {
        $this->printer = $printer;

        return $this;
    }

    public function setShowBig(int $show_big = null): self
    {
        $this->show_big = $show_big === self::ACCEPTED;

        return $this;
    }

    public function setBlockThickness(Collection $paperCollection = null): self
    {
        $paperCollection = $this->papers ?? $paperCollection;

        if ($paperCollection === null) {
            throw new InvalidArgumentException('Invalid paper collection');
        }

        $this->block_thickness = $paperCollection->sum(static function(Paper $paper) {
            return $paper->thickness;
        });

        return $this;
    }

    public function setBookThickness(): self
    {
        $this->book_thickness = $this->block_thickness + (2 * $this->cardboard ?? 0.5);

        return $this;
    }

    public function setPagesTotal(Collection $paperCollection = null): self
    {
        $paperCollection = $this->papers ?? $paperCollection;

        if ($paperCollection === null) {
            throw new InvalidArgumentException('Invalid paper collection');
        }

        $this->pages_total = $paperCollection->sum(static function(Paper $paper) {
            return $paper->pages;
        });

        return $this;
    }

    public function setPagesMinimum(Collection $paperCollection = null): void
    {
        $paperCollection = $this->papers ?? $paperCollection;

        if ($paperCollection === null) {
            throw new InvalidArgumentException('Invalid paper collection');
        }

        if ($paperCollection->count() === 1) {
            $paper = $paperCollection->first();
            $pages = (int) ceil((static::BLOCK_MIN * 2000) / ($paper->weight * $paper->volume));

            $this->pages_min = ($pages % 2) ? --$pages : $pages;
        }
    }

    public function setPagesMaximum(Collection $paperCollection = null): void
    {
        $paperCollection = $this->papers ?? $paperCollection;

        if ($paperCollection === null) {
            throw new InvalidArgumentException('Invalid paper collection');
        }

        if ($paperCollection->count() === 1) {
            $paper = $paperCollection->first();
            $pages = (int) floor( (static::BLOCK_MAX - $this->doSurplusBlock($paper)) * 2000 / $paper->weight / $paper->volume);

            $this->pages_max = ($pages % 2) ? --$pages : $pages;
        }
    }

    public function setWeight(Collection $paperCollection = null): self
    {
        $paperCollection = $this->papers ?? $paperCollection;

        if ($paperCollection === null) {
            throw new InvalidArgumentException('Invalid paper collection');
        }

        $this->weight = $paperCollection->sum(static function(Paper $paper) {
            return $paper->mass;
        });

        return $this;
    }

    public function isPrinterToner(string $printer = null): bool
    {
        $printer = $printer ?? $this->parameters->printer;

        return $printer === PrinterType::TONER;
    }

    public function isSpineRound(int $spine = null): bool
    {
        $spine = $spine ?? $this->parameters->spine;

        return $spine === SpineType::ROUND;
    }

    public function isSpineCustom(float $spine = null): bool
    {
        $spine = $spine ?? $this->parameters->spine_custom;

        return $spine !== null;
    }

    public function getCustomSpine(): ?float
    {
        return $this->parameters->spine_custom;
    }

    public function doSurplusBlock(Paper $paper): float
    {
        return 0;
    }

    public function doPageWidthRear(int $width, Flaps $flap = null): int
    {
        if ($flap !== null && $flap->flaps === true) {
            return $width + $flap->thickness;
        }
        return $width;
    }

    public function doPageWidthFront(int $width, Flaps $flap = null): int
    {
        return $this->doPageWidthRear($width, $flap);
    }

    public function doPageHeight(int $height = 0): int
    {
        return $height;
    }

    public function doHinge(): int
    {
        return static::HINGE;
    }

    public function doBig(): int
    {
        return static::BIG;
    }

    public function doCardboard(float $cardboard = 0): float
    {
        return 0;
    }

    public function doBleeds(): int
    {
        return static::BLEED;
    }

    public function doBleedsInner(): int
    {
        return 0;
    }

    public function doSpine(): float
    {
        return 0;
    }

    public function doSpiral(Spiral $spiral): ?Spiral
    {
        return null;
    }

    public function doFlapRear(Flaps $flaps): int
    {
        if ($flaps->flaps) {
            if ($flaps->symmetric) {
                return $flaps->width;
            }
            return $flaps->left;
        }
        return 0;
    }

    public function doFlapFront(Flaps $flaps): int
    {
        if ($flaps->flaps) {
            if ($flaps->symmetric) {
                return $flaps->width;
            }
            return $flaps->right;
        }
        return 0;
    }

    public function doWeight(Weight $weight): float
    {
        return $weight->calculate();
    }

    private function build(): void
    {
        $this->setWidth($this->parameters->width);
        $this->setHeight($this->parameters->height);

        $this->setPapers($this->parameters->papers);

        $this->setSpine($this->parameters->spine);
        $this->setSpineEncore($this->parameters->spine_encore);
        $this->setCardboard($this->parameters->cardboard);

        $this->setFlaps($this->parameters->flaps);

        $this->setPrinter($this->parameters->printer);
        $this->setShowBig($this->parameters->show_big);

        $this->setBlockThickness();
        $this->setBookThickness();
        $this->setPagesTotal();
        $this->setPagesMinimum();
        $this->setPagesMaximum();
        $this->setSpiral($this->parameters->spiral);
        $this->setWeight();
    }

    private function setBookParameters(BookParameters $parameters = null): void
    {
        $this->parameters = $parameters;
    }

    private function verifyBindParameters(): void
    {
        if ($this->label === null) {
            throw new \LogicException(\get_class($this) . ' must have a $label');
        }
        if ($this->flaps_allowed === null) {
            throw new \LogicException(\get_class($this) . ' must have a $flaps_allowed');
        }
        if ($this->cardboard_allowed === null) {
            throw new \LogicException(\get_class($this) . ' must have a $cardboard_allowed');
        }
        if ($this->big_allowed === null) {
            throw new \LogicException(\get_class($this) . ' must have a $big_allowed');
        }
    }

}
