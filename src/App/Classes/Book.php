<?php

namespace Rudashi\BookGenerator\App\Classes;

use Illuminate\Http\Request;
use Rudashi\BookGenerator\App\Classes\Contracts\BindInterface;
use Rudashi\BookGenerator\App\Classes\Contracts\CoverInterface;
use Rudashi\BookGenerator\App\Classes\Parameters\Flaps;
use Rudashi\BookGenerator\App\Enums\CoverType;
use Rudashi\BookGenerator\App\Exceptions\InvalidBindTypeException;
use Rudashi\BookGenerator\App\Exceptions\InvalidCoverTypeException;
use Rudashi\Profis\Enums\BindType;

class Book
{

    public BookParameters $parameters;
    public ?string $name;
    public BindInterface $binding;
    public CoverInterface $cover;
    public float $weight;
    public Validate $validation;

    public function __construct(string $cover = null, string $binding = null, string $name = null)
    {
        if ($cover !== null) {
            $this->setCover($cover);
        }
        if ($binding !== null) {
            $this->setBinding($binding);
        }
        if ($name !== null) {
            $this->setName($name);
        }
        $this->validation = new Validate();
    }

    public function create(Request $request): self
    {
        $this->setParameters($request);

        $this->build();

        $this->validate();

        return $this;
    }

    public function getBindingType(string $type = null): BindInterface
    {
        switch ($type) {
            case BindType::SOFTCOVER_GLUED:
                return new Binds\SoftCoverGlued();
            case BindType::SOFTCOVER_SEWN:
                return new Binds\SoftCoverSewn();
            case BindType::STAPLED:
                return new Binds\Stapled();
            case BindType::HARDCOVER_GLUED:
                return new Binds\HardCoverGlued();
            case BindType::HARDCOVER_SEWN:
                return new Binds\HardCoverSewn();
            case BindType::WIRE_O:
                return new Binds\WireO();
            case BindType::HARDCOVER_WIRE_O:
                return new Binds\HardCoverWireO();
            case BindType::INTEGRATED:
                return new Binds\Integrated();
            default:
                throw new InvalidBindTypeException();
        }
    }

    public function getCoverType(string $type = null): CoverInterface
    {
        switch ($type) {
            case CoverType::COVER:
                return new Covers\Casewrap();
            case CoverType::JACKET:
                return new Covers\DustJacket();
            default:
                throw new InvalidCoverTypeException();
        }
    }

    public function setBinding(string $type = null): self
    {
        $this->binding = $this->getBindingType($type);

        return $this;
    }

    public function setCover(string $type = null): self
    {
        $this->cover = $this->getCoverType($type);

        return $this;
    }

    public function setName(string $job_name = null): self
    {
        $this->name = $job_name;

        return $this;
    }

    public function setParameters(Request $request): self
    {
        $this->parameters = new BookParameters($request);

        return $this;
    }

    public function setWeight(...$weight): self
    {
        $this->weight = array_sum($weight);

        return $this;
    }

    private function build(): void
    {
        $this->setName($this->parameters->job_name);

        $this->setBinding($this->parameters->bind_type);
        $this->binding->create($this->parameters);

        $this->setCover($this->parameters->cover_type);
        $this->cover->create($this->binding);

        $this->setWeight($this->binding->weight, $this->cover->weight);

        if ($this->parameters->cover_type === CoverType::JACKET) {
            $this->setWeight($this->weight, $this->getCoverType(CoverType::COVER)->create((clone $this->binding)->setFlaps(new Flaps()))->weight);
        }
    }

    private function validate(): void
    {
        $this->validation->run($this->binding);
    }

}
