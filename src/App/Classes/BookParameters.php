<?php

namespace Rudashi\BookGenerator\App\Classes;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Rudashi\BookGenerator\App\Classes\Parameters\Flaps;
use Rudashi\BookGenerator\App\Classes\Parameters\Paper;
use Rudashi\BookGenerator\App\Classes\Parameters\Spiral;
use Rudashi\BookGenerator\App\Enums\PrinterType;

class BookParameters
{

    public string $cover_type;
    public int $width;
    public int $height;
    public string $bind_type;
    public Collection $papers;
    public int $spine;
    public int $spine_encore;
    public ?float $spine_custom = null;
    public ?float $cardboard = null;
    public Flaps $flaps;
    public string $printer;
    public int $show_big;
    public int $no_logo;
    public ?string $job_name = null;
    public Spiral $spiral;

    public function __construct(Request $request)
    {
        $this->cover_type   = $request->input('cover_type');
        $this->width        = $request->input('width');
        $this->height       = $request->input('height');
        $this->bind_type    = $request->input('bind_type');
        $this->papers       = (new Collection($request->input('paper')))->mapInto(Paper::class);
        $this->spine        = (int) $request->input('spine');
        $this->spine_encore = (int) $request->input('spine_encore');
        $this->spine_custom = $request->input('spine_custom');
        $this->cardboard    = $request->input('cardboard');
        $this->flaps        = new Flaps($request);
        $this->printer      = (new PrinterType($request->input('printer', '')))->value;
        $this->job_name     = $request->input('job_name');
        $this->show_big     = (int) $request->input('show_big');
        $this->no_logo      = (int) $request->input('no_logo');
        $this->spiral       = new Spiral($request->input('spiral.color'), $request->input('spiral.position'), $request->input('spiral.qty'));
    }

}
