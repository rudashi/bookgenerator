<?php

namespace Rudashi\BookGenerator\App\Classes\Binds;

use Illuminate\Support\Collection;
use Rudashi\BookGenerator\App\Classes\Validate;

class SoftCoverSewn extends SoftCover
{

    public string $label = 'Softcover - thread sewn';
    public bool $flaps_allowed = true;
    public bool $cardboard_allowed = false;
    public bool $spine_allowed = false;
    public bool $big_allowed = true;
    public bool $spiral_color_allowed = false;
    public bool $spiral_position_allowed = false;
    public bool $dust_jacket_allowed = true;

    public const BLOCK_MIN = 3;

    public function doSpine(): float
    {
        return round(($this->block_thickness + 0.5 + 1.5) * 2) / 2;
    }

    public function setPagesMaximum(Collection $paperCollection = null): void
    {
        parent::setPagesMaximum($paperCollection);

        if ($this->pages_max % 4) {
            $this->pages_max += 2;
        }
    }

    public function setPagesMinimum(Collection $paperCollection = null): void
    {
        parent::setPagesMinimum($paperCollection);

        if ($this->pages_min % 4) {
            $this->pages_min += 2;
        }
    }

    public function validateParameters(Validate $validate): void
    {
        $validate->isDivisibleByFour();
        parent::validateParameters($validate);
    }

}
