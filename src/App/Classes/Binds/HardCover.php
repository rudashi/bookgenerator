<?php

namespace Rudashi\BookGenerator\App\Classes\Binds;

use Rudashi\BookGenerator\App\Classes\Bind;
use Rudashi\BookGenerator\App\Classes\Covers\DustJacket;
use Rudashi\BookGenerator\App\Classes\Parameters\Flaps;
use Rudashi\BookGenerator\App\Classes\Parameters\Paper;
use Rudashi\BookGenerator\App\Classes\Parameters\Weight;
use Rudashi\BookGenerator\App\Classes\Validate;

abstract class HardCover extends Bind
{
    public const TONER_BLOCK_GROW = 0.0025;
    public const SPINE_BLOCK_MIN = 8;
    public const SPINE_BLOCK_MAX = 45;

    public const BLEED = 18;
    public const HINGE = 9;

    public function doCardboard(float $cardboard = 0): float
    {
        return $cardboard;
    }

    public function doFlapFront(Flaps $flaps): int
    {
        return 0;
    }

    public function doFlapRear(Flaps $flaps): int
    {
        return 0;
    }

    public function doHinge(): int
    {
        if ($this->isSpineRound()) {
            return static::HINGE;
        }
        return static::HINGE + 2;
    }

    public function doPageHeight(int $height = 0): int
    {
        return $height + 6;
    }

    public function doPageWidthRear(int $width, Flaps $flap = null): int
    {
        return parent::doPageWidthRear($width) - 5;
    }

    public function doSpine(): float
    {
        $spine = $this->block_thickness + (2 * $this->cardboard);

        if ($this->isSpineRound()) {
            $spine *= (1 + (1 / $spine));
        }

        $spineHardCover = round(($spine + 0.5) * 2) / 2;

        return ($spineHardCover < 7 ? 7 : $spineHardCover) + ($this->spine_encore ? 1 : 0);
    }

    public function doSurplusBlock(Paper $paper): float
    {
        if ($this->isPrinterToner()) {
            return ($paper->pages ** 2) * self::TONER_BLOCK_GROW / 1000;
        }
        return parent::doSurplusBlock($paper);
    }

    public function doWeight(Weight $weight): float
    {
        return $weight->setCaseWrap(
            $weight->getCover()->total_width,
            $weight->getCover()->total_height,
            DustJacket::weightMultiplier()
        )->calculate();
    }

    public function validateParameters(Validate $validate): void
    {
        $validate->width(static::WIDTH_MIN, static::WIDTH_MAX);
        $validate->height(static::HEIGHT_MIN, static::HEIGHT_MAX);
        $validate->pages();
        $validate->cardboard();
        $validate->spineRoundPossibility(static::SPINE_BLOCK_MIN, static::SPINE_BLOCK_MAX);
        $validate->blockThickness(static::BLOCK_MIN, static::BLOCK_MAX, 0.1);
        $validate->flapsMaximum($this->width - 5);
    }

}
