<?php

namespace Rudashi\BookGenerator\App\Classes\Binds;

use Rudashi\BookGenerator\App\Classes\Bind;
use Rudashi\BookGenerator\App\Classes\Validate;
use Rudashi\BookGenerator\App\Enums\BookDefaults;

class Stapled extends Bind
{

    public const WIDTH_MIN = 120;
    public const WIDTH_MAX = 285;
    public const HEIGHT_MIN = 85;
    public const BLOCK_MAX = 10;

    public string $label = 'Softcover - saddle stitched';
    public bool $flaps_allowed = true;
    public bool $cardboard_allowed = false;
    public bool $spine_allowed = false;
    public bool $big_allowed = false;
    public bool $spiral_color_allowed = false;
    public bool $spiral_position_allowed = false;
    public bool $dust_jacket_allowed = true;

    public function validateParameters(Validate $validate): void
    {
        $validate->width(static::WIDTH_MIN, static::WIDTH_MAX);
        $validate->height(static::HEIGHT_MIN, static::HEIGHT_MAX);
        $validate->pages();
        $validate->isDivisibleByFour();
        $validate->flapsMaximum($this->width - BookDefaults::FLAPS_DIFF);
    }

}
