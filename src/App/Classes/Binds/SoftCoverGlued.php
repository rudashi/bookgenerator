<?php

namespace Rudashi\BookGenerator\App\Classes\Binds;

use Rudashi\BookGenerator\App\Classes\Validate;

class SoftCoverGlued extends SoftCover
{

    public string $label = 'Softcover - perfect bound';
    public bool $flaps_allowed = true;
    public bool $cardboard_allowed = false;
    public bool $spine_allowed = false;
    public bool $big_allowed = true;
    public bool $spiral_color_allowed = false;
    public bool $spiral_position_allowed = false;
    public bool $dust_jacket_allowed = true;

    public function doSpine(): float
    {
        return round(($this->block_thickness + 0.75) * 2) / 2;
    }

    public function validateParameters(Validate $validate): void
    {
        $validate->isPagesEven();
        parent::validateParameters($validate);
    }

}
