<?php

namespace Rudashi\BookGenerator\App\Classes\Binds;

use Illuminate\Support\Collection;
use InvalidArgumentException;
use Rudashi\BookGenerator\App\Classes\Bind;
use Rudashi\BookGenerator\App\Classes\Parameters\Flaps;
use Rudashi\BookGenerator\App\Classes\Parameters\Spiral;
use Rudashi\BookGenerator\App\Classes\Validate;

class HardCoverWireO extends Bind
{

    public const WIDTH_MIN = 100;
    public const WIDTH_MAX = 315;
    public const HEIGHT_MIN = 85;
    public const HEIGHT_MAX = 480;
    public const BLOCK_MIN = 2;
    public const BLOCK_MAX = 28;
    public const SPIRAL_MARGIN = 10;
    public const BLEED = 18;

    public string $label = 'Hardcover spiral binding';
    public bool $flaps_allowed = false;
    public bool $cardboard_allowed = true;
    public bool $spine_allowed = false;
    public bool $big_allowed = false;
    public bool $spiral_color_allowed = true;
    public bool $spiral_position_allowed = true;
    public bool $dust_jacket_allowed = false;

    public function doCardboard(float $cardboard = 0): float
    {
        return $cardboard;
    }

    public function doBleedsInner(): int
    {
        return static::BLEED;
    }

    public function doPageHeight(int $height = 0): int
    {
        return $height + 3;
    }

    public function doPageWidthRear(int $width, Flaps $flap = null): int
    {
        $surplus = 4;
        if ($this->block_thickness < 15) {
            $surplus = 3;
        }
        if ($this->block_thickness > 25) {
            $surplus = 5;
        }
        return parent::doPageWidthRear($width) + $surplus;
    }

    public function doSpiral(Spiral $spiral): ?Spiral
    {
        try {
            return $spiral->validate(
                $this->book_thickness,
                $this->width,
                $this->height,
            );
        } catch (InvalidArgumentException $e) {
            return null;
        }
    }

    public function setPagesMaximum(Collection $paperCollection = null): void
    {
        $paperCollection = $this->papers ?? $paperCollection;

        if ($paperCollection === null) {
            throw new InvalidArgumentException('Invalid paper collection');
        }

        if ($paperCollection->count() === 1) {
            $paper = $paperCollection->first();
            $pages = (int) floor((static::BLOCK_MAX - $this->doSurplusBlock($paper) - (2 * $this->cardboard)) * 2000 / $paper->weight / $paper->volume);

            $this->pages_max = ($pages % 2) ? --$pages : $pages;
        }
    }

    public function validateParameters(Validate $validate): void
    {
        $validate->width(static::WIDTH_MIN, static::WIDTH_MAX);
        $validate->height(static::HEIGHT_MIN, static::HEIGHT_MAX);
        $validate->isPagesEven();
        $validate->pages();
        $validate->blockThickness(static::BLOCK_MIN, (static::BLOCK_MAX - (2 * $this->cardboard)));
        $validate->add('perforation',
            __(
                'Spiral perforation - the recommended margin from the spine edge is :margin mm. It is recommended not to place significant graphics in this area that may fall into the perforation area.',
                ['margin' => self::SPIRAL_MARGIN]
            )
        );
    }

}
