<?php

namespace Rudashi\BookGenerator\App\Classes\Binds;

use Rudashi\BookGenerator\App\Classes\Bind;
use Rudashi\BookGenerator\App\Classes\Parameters\Flaps;
use Rudashi\BookGenerator\App\Classes\Validate;
use Rudashi\BookGenerator\App\Enums\BookDefaults;

class Integrated extends Bind
{

    public const WIDTH_MIN = 100;
    public const WIDTH_MAX = 305;
    public const HEIGHT_MIN = 100;
    public const HEIGHT_MAX = 380;
    public const BLOCK_MIN = 6;
    public const BLOCK_MAX = 62;
    public const BLEED = 21;
    public const HINGE = 6;

    public string $label = 'Flexo bound';
    public bool $flaps_allowed = false;
    public bool $cardboard_allowed = false;
    public bool $spine_allowed = false;
    public bool $big_allowed = false;
    public bool $spiral_color_allowed = false;
    public bool $spiral_position_allowed = false;
    public bool $dust_jacket_allowed = true;

    public function doFlapFront(Flaps $flaps): int
    {
        return 0;
    }

    public function doFlapRear(Flaps $flaps): int
    {
        return 0;
    }

    public function doPageHeight(int $height = 0): int
    {
        return $height + $this->doHinge();
    }

    public function doPageWidthRear(int $width, Flaps $flap = null): int
    {
        return parent::doPageWidthRear($width, $flap) - 3;
    }

    public function doSpine(): float
    {
        $spine = round((($this->block_thickness + 2) * (1 + (1 / ($this->block_thickness + 2)))) + 0.5) - 0.5;

        return ($spine < 7 ? 7 : $spine);
    }

    public function validateParameters(Validate $validate): void
    {
        $validate->width(static::WIDTH_MIN, static::WIDTH_MAX);
        $validate->height(static::HEIGHT_MIN, static::HEIGHT_MAX);
        $validate->isDivisibleByFour();
        $validate->pages();
        $validate->blockThickness(static::BLOCK_MIN, static::BLOCK_MAX);
        $validate->flapsMaximum($this->width - BookDefaults::FLAPS_DIFF);
    }

}
