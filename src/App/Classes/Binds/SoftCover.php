<?php

namespace Rudashi\BookGenerator\App\Classes\Binds;

use Rudashi\BookGenerator\App\Classes\Bind;
use Rudashi\BookGenerator\App\Classes\Parameters\Flaps;
use Rudashi\BookGenerator\App\Classes\Validate;
use Rudashi\BookGenerator\App\Enums\BookDefaults;

abstract class SoftCover extends Bind
{

    public const WIDTH_MIN = 70;
    public const WIDTH_MAX = 320;
    public const HEIGHT_MIN = 100;
    public const HEIGHT_MAX = 480;
    public const BLOCK_MIN = 2;
    public const BLOCK_MAX = 62;
    public const BIG = 7;

    public const FLAPS_MIN = 40;
    public const FLAPS_MAX = 0; //width * 1.75

    public function doPageWidthRear(int $width, Flaps $flap = null): int
    {
        return parent::doPageWidthRear($width, $flap) - $this->doBig();
    }

    public function doBig(): int
    {
        return $this->show_big ? static::BIG : 0;
    }

    public function validateParameters(Validate $validate): void
    {
        $validate->width(static::WIDTH_MIN, static::WIDTH_MAX);
        $validate->height(static::HEIGHT_MIN, static::HEIGHT_MAX);
        $validate->pages();
        $validate->blockThickness(static::BLOCK_MIN, static::BLOCK_MAX);
        $validate->flapsMaximum($this->width - BookDefaults::FLAPS_DIFF);
    }

}
