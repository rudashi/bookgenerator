<?php

namespace Rudashi\BookGenerator\App\Classes\Binds;

use Rudashi\BookGenerator\App\Classes\Bind;
use Rudashi\BookGenerator\App\Classes\Parameters\Flaps;
use Rudashi\BookGenerator\App\Classes\Parameters\Spiral;
use Rudashi\BookGenerator\App\Classes\Validate;

class WireO extends Bind
{

    public const WIDTH_MIN = 100;
    public const WIDTH_MAX = 315;
    public const HEIGHT_MIN = 85;
    public const HEIGHT_MAX = 480;
    public const BLOCK_MIN = 2;
    public const BLOCK_MAX = 28;
    public const SPIRAL_MARGIN = 10;

    public string $label = 'Softcover spiral binding';
    public bool $flaps_allowed = true;
    public bool $cardboard_allowed = false;
    public bool $spine_allowed = false;
    public bool $big_allowed = false;
    public bool $spiral_color_allowed = true;
    public bool $spiral_position_allowed = true;
    public bool $dust_jacket_allowed = false;

    public function doBleedsInner(): int
    {
        return static::BLEED;
    }

    public function doPageWidthRear(int $width, Flaps $flap = null): int
    {
        return parent::doPageWidthRear($width, $flap) + 2;
    }

    public function doSpiral(Spiral $spiral): ?Spiral
    {
        try {
            return $spiral->validate(
                $this->book_thickness,
                $this->width,
                $this->height,
            );
        } catch (\InvalidArgumentException $e) {
            return null;
        }
    }

    public function validateParameters(Validate $validate): void
    {
        $validate->width(static::WIDTH_MIN, static::WIDTH_MAX);
        $validate->height(static::HEIGHT_MIN, static::HEIGHT_MAX);
        $validate->isPagesEven();
        $validate->pages();
        $validate->blockThickness(static::BLOCK_MIN, static::BLOCK_MAX);
        $validate->add('perforation',
            __(
                'Spiral perforation - the recommended margin from the spine edge is :margin mm. It is recommended not to place significant graphics in this area that may fall into the perforation area.',
                ['margin' => self::SPIRAL_MARGIN]
            )
        );
    }

}
