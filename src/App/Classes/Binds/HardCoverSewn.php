<?php

namespace Rudashi\BookGenerator\App\Classes\Binds;

use Illuminate\Support\Collection;
use Rudashi\BookGenerator\App\Classes\Validate;

class HardCoverSewn extends HardCover
{

    public string $label = 'Hardcover - thread sewn';
    public bool $flaps_allowed = false;
    public bool $cardboard_allowed = true;
    public bool $spine_allowed = true;
    public bool $big_allowed = false;
    public bool $spiral_color_allowed = false;
    public bool $spiral_position_allowed = false;
    public bool $dust_jacket_allowed = true;

    public const WIDTH_MIN = 100;
    public const WIDTH_MAX = 305;
    public const HEIGHT_MIN = 100;
    public const HEIGHT_MAX = 380;
    public const BLOCK_MIN = 3;
    public const BLOCK_MAX = 70;

    public function setPagesMaximum(Collection $paperCollection = null): void
    {
        parent::setPagesMaximum($paperCollection);

        if ($this->pages_max % 4) {
            $this->pages_max += 2;
        }
    }

    public function setPagesMinimum(Collection $paperCollection = null): void
    {
        parent::setPagesMinimum($paperCollection);

        if ($this->pages_min % 4) {
            $this->pages_min += 2;
        }
    }

    public function validateParameters(Validate $validate): void
    {
        $validate->isDivisibleByFour();
        parent::validateParameters($validate);
    }

}
