<?php

namespace Rudashi\BookGenerator\App\Classes\Covers;

use Rudashi\BookGenerator\App\Classes\Bind;
use Rudashi\BookGenerator\App\Classes\Cover;
use Rudashi\BookGenerator\App\Classes\Contracts\BindInterface;
use Rudashi\BookGenerator\App\Classes\Parameters\Weight;

class DustJacket extends Cover
{

    public ?string $label = 'Dust Jacket';

    public static function weightMultiplier(): int
    {
        return 135;
    }

    public function setHinge(BindInterface $binding): Cover
    {
        $this->hinge = 0;

        return $this;
    }

    public function setBig(BindInterface $binding): Cover
    {
        $this->big = 0;

        return $this;
    }

    public function setPageWidthRear(BindInterface $binding): Cover
    {
        $this->page_width_rear = $binding->doPageWidthRear($binding->width)
            + $binding->doHinge()
            + $binding->doBig()
            + $binding->doCardboard($binding->cardboard)
            + 0.5 ;

        return $this;
    }

    public function setPageWidthFront(BindInterface $binding): Cover
    {
        $this->page_width_front = $this->page_width_rear;

        return $this;
    }

    public function setSpine(BindInterface $binding): Cover
    {
        parent::setSpine($binding);

        $this->spine += $this->spine > 0 ? 0.5 : 0;

        return $this;
    }

    public function setCardboard(BindInterface $binding): Cover
    {
        $this->cardboard = 0;

        return $this;
    }

    public function setBleeds(BindInterface $binding): Cover
    {
        $this->bleeds = Bind::BLEED;

        return $this;
    }

    public function setFlapBack(BindInterface $binding): Cover
    {
        $this->flaps_left = $binding->flaps->width ? (int) $binding->flaps->width : (int) $binding->flaps->left;

        return $this;
    }

    public function setFlapFront(BindInterface $binding): Cover
    {
        $this->flaps_right = $binding->flaps->width ? (int) $binding->flaps->width : (int) $binding->flaps->right;

        return $this;
    }

    public function setWeight(BindInterface $binding): Cover
    {
        $this->weight = (new Weight($this, $binding))->calculate();

        return $this;
    }

}
