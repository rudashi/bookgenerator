<?php

namespace Rudashi\BookGenerator\App\Classes\Covers;

use Rudashi\BookGenerator\App\Classes\Cover;

class Casewrap extends Cover
{

    public ?string $label = 'Cover';

    public static function weightMultiplier(): int
    {
        return 300;
    }

}
