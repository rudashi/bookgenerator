<?php

namespace Rudashi\BookGenerator\App\Classes;

use Rudashi\BookGenerator\App\Enums\BookDefaults;
use Rudashi\BookGenerator\App\Enums\CardboardThickness;
use Rudashi\BookGenerator\App\Classes\Parameters\Paper;
use Rudashi\BookGenerator\App\Classes\Contracts\BindInterface;
use Illuminate\Support\MessageBag;

class Validate extends MessageBag
{

    private ?BindInterface $bind;

    public function __construct(array $messages = [], BindInterface $bind = null)
    {
        parent::__construct($messages);
        $this->bind = $bind;
    }

    public function setBind(BindInterface $bind): self
    {
        $this->bind = $bind;

        return $this;
    }

    public function run(BindInterface $bind): void
    {
        $this->setBind($bind);
        $this->bind->validateParameters($this);
        $this->customSpine($this->bind->doSpine(), $this->bind->getCustomSpine());
    }

    public function width(int $min, int $max): void
    {
        $this->widthMinimum($min);
        $this->widthMaximum($max);
    }

    public function widthMinimum(int $min): void
    {
        if ($this->bind->width < $min) {
            $this->add('width', __('The minimum width for this binding is :min mm.', [ 'min' => $min ]));
        }
    }

    public function widthMaximum(int $max): void
    {
        if ($this->bind->width > $max) {
            $this->add('width', __('The maximum width for this binding is :max mm.', [ 'max' => $max ]));
        }
    }

    public function height(int $min, int $max): void
    {
        $this->heightMinimum($min);
        $this->heightMaximum($max);
    }

    public function heightMinimum(int $min): void
    {
        if ($this->bind->height < $min) {
            $this->add('height', __('The minimum height for this binding is :min mm.', [ 'min' => $min]));
        }
    }

    public function heightMaximum(int $max): void
    {
        if ($this->bind->height > $max) {
            $this->add('height', __('The maximum height for this binding is :max mm.', [ 'max' => $max]));
        }
    }

    public function cardboard(float $maxBlockThickness = 5, int $maxCardboard = CardboardThickness::CARDBOARD_2): void
    {
        if ($this->bind->block_thickness < $maxBlockThickness && $this->bind->cardboard > $maxCardboard) {
            $this->add('cardboard', __('For publications with such a small volume, do not use cardboard thicker than :max mm.', [ 'max' => $maxCardboard ]));
        }
    }

    public function spineRoundPossibility(int $min, int $max): void
    {
        if ($this->bind->isSpineRound()) {
            if ($this->bind->block_thickness < $min || $this->bind->block_thickness > $max) {
                $this->add('block_thickness', __('The recommended block thickness for rounded spine should be in the range of :min-:max mm.', [ 'min' => $min, 'max' => $max ]));
            }
        }
    }

    public function blockThickness(int $min, int $max, float $delta = 0.0): void
    {
        $this->blockThicknessMinimum($min, $delta);
        $this->blockThicknessMaximum($max);
    }

    public function blockThicknessMinimum(int $min, float $delta): void
    {
        if ($this->bind->block_thickness < $min && abs($this->bind->block_thickness - $min) > $delta) {
            $this->add('block_thickness', __('The block thickness for this type of binding is a minimum :min mm.', [ 'min' => $min]));
        }
    }

    public function blockThicknessMaximum(int $max): void
    {
        if ($this->bind->block_thickness > $max) {
            $this->add('block_thickness', __('The block thickness for this type of binding is a maximum :max mm.', [ 'max' => $max]));
        }
    }

    public function pages(): void
    {
        $this->pagesMinimum($this->bind->pages_min);
        $this->pagesMaximum($this->bind->pages_max);
    }

    public function pagesMinimum(?int $min): void
    {
        if ($min && $this->bind->pages_total < $min) {
            $this->add('pages_bind_min', __('The number of pages is too small for this binding type. The minimum acceptable number of pages for this paper - :pages.', [ 'pages' => $min]));
        }
    }

    public function pagesMaximum(?int $max): void
    {
        if ($max && $this->bind->pages_total > $max) {
            $this->add('pages_bind_max', __('The number of pages is too greater for this binding type. The maximum acceptable number of pages for this paper - :pages.', [ 'pages' => $max]));
        }
    }

    public function isPagesEven(): void
    {
        $this->bind->papers->map(function(Paper $paper, $i) {
            if ($paper->pages % 2) {
                $this->merge(['pages_even' => __('The paper :name count :count is not even.', [ 'count' => $paper->pages, 'name' =>  $paper->name ?:$i+1 ])]);
            }
        });
    }

    public function isDivisibleByFour(): void
    {
        $this->bind->papers->map(function(Paper $paper, $i) {
            if ($paper->pages % 4) {
                $this->merge(['pages_even' => __('The paper :name count :count is not divisible by four.', [ 'count' => $paper->pages, 'name' =>  $paper->name ?:$i+1 ])]);
            }
        });
    }

    public function flapsMaximum(int $max): void
    {
        $this->flapLeftMaximum($max);
        $this->flapRightMaximum($max);
    }

    public function flapLeftMaximum(int $max): void
    {
        if ($this->bind->flaps->left > $max) {
            $this->add('flaps', __('The :attribute may not be greater than :max.', ['attribute' => __('Flap left') ,'max' => $max]));
        }
    }

    public function flapRightMaximum(int $max): void
    {
        if ($this->bind->flaps->right > $max) {
            $this->add('flaps', __('The :attribute may not be greater than :max.', ['attribute' => __('Flap right') ,'max' => $max]));
        }
    }

    public function customSpine(float $spine, float $getCustomSpine = null): void
    {
        if ($getCustomSpine && $spine !== $getCustomSpine) {
            $this->add('spine', __('Spine manually changed by the operator.'));
        }
    }

}
