<?php

namespace Rudashi\BookGenerator\App\Classes\Parameters;

class Paper
{

    public ?int $pages = null;
    public ?int $paper_id = null;
    public ?string $name = null;
    public ?int $weight = null;
    public ?float $volume = null;
    public ?string $volume_name = null;
    public ?float $mass = null;
    public ?float $thickness = null;

    public function __construct($pages, int $type = null, int $weight = null, float $volume = null, string $name = null, string $volume_name = null)
    {
        if (is_array($pages)) {
            $this->setProperties(
                $pages['pages'] ?? null,
                $pages['type'] ?? null,
                $pages['name'] ?? null,
                $pages['weight'] ?? null,
                $pages['volume_name'] ?? null,
                $pages['volume'] ?? null
            );
        } else {
            $this->setProperties($pages, $type, $name, $weight, $volume_name, $volume);
        }
    }

    private function setProperties(int $pages = null, int $type = null, string $name = null, int $weight = null, string $volume_name = null, float $volume = null): void
    {
        if (!$pages || !$type || !$weight || !$volume) {
            throw new \InvalidArgumentException(__('Incorrect paper parameters.'));
        }
        $this->pages        = $pages;
        $this->paper_id     = $type;
        $this->name         = $name;
        $this->weight       = $weight;
        $this->volume       = $volume;
        $this->volume_name  = $volume_name;
    }

    public function setMass(int $width, int $height): self
    {
        $this->mass = ($this->pages / 2) * $width * $height * $this->weight / 1000000;

        return $this;
    }

    public function setThickness(float $additional = 0): self
    {
        $this->thickness = round(($this->weight * $this->volume * $this->pages / 2000) + $additional, 5);

        return $this;
    }

    public function setName(string $name = null): self
    {
        $this->name = $name;

        return $this;
    }

    public function setVolumeName(string $volume_name = null): self
    {
        $this->volume_name = $volume_name;

        return $this;
    }

}
