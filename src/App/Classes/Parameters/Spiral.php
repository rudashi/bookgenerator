<?php

namespace Rudashi\BookGenerator\App\Classes\Parameters;

use Rudashi\BookGenerator\App\Enums\SpiralPosition;
use Rudashi\BookGenerator\App\Model\Spiral as SpiralModel;

class Spiral
{

    public ?string $color = null;
    public ?string $position = null;
    public ?int $qty = null;

    public ?string $spiral = null;
    public ?string $holes = null;
    public ?int $holes_qty = null;
    public ?float $spools_qty = null;
    public ?int $packages_qty = null;

    public function __construct(string $color = null, int $position = null, int $qty = null)
    {
        $this->color    = $color;
        $this->position = SpiralPosition::getKey($position);
        $this->qty      = $qty;
    }

    public function getSpiralModel(float $book_thickness = null): SpiralModel
    {
        /** @var SpiralModel|null $spiral */
        $spiral = (new SpiralModel)->newQuery()->where('block_thickness', '>=', $book_thickness)->first();

        if ($spiral === null) {
            throw new \InvalidArgumentException('Spiral parameter not found.');
        }

        return $spiral;
    }

    public function setSpiral(string $spiral): self
    {
        $this->spiral = $spiral;

        return $this;
    }

    public function setHoles(int $holes): self
    {
        $this->holes = $holes.':1';

        return $this;
    }

    public function setHolesQty(int $position_size, int $holes): self
    {
        $this->holes_qty = ceil($position_size / 25.40 * $holes * ($this->qty));

        return $this;
    }

    public function setPackagesQty(int $position_size): self
    {
        $this->packages_qty = $position_size === 297 ? $this->holes_qty / 100 : 0;

        return $this;
    }

    public function setSpoolsQty(int $holes_spool): self
    {
        $this->spools_qty = round($this->holes_qty / $holes_spool, 2);

        return $this;
    }

    public function validate(float $book_thickness, int $width, int $height, int $spiral_qty = null): self
    {
        if (!$this->color || !$this->position) {
            $this->position = null;

            return $this;
        }

        $spiral = $this->getSpiralModel($book_thickness);
        $position_size = $this->position === SpiralPosition::getKey(SpiralPosition::LONG_SIDE) ? max($width, $height) : min($width, $height);

        $this->qty = $spiral_qty ?? $this->qty;

        $this->setSpiral($spiral->spiral);
        $this->setSpiral($spiral->spiral);
        $this->setHoles($spiral->holes);

        if ($this->qty) {
            $this->setHolesQty($position_size, $spiral->holes);
            $this->setPackagesQty($position_size);
            $this->setSpoolsQty($spiral->holes_spool);
        }

        return $this;
    }

}
