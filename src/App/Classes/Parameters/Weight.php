<?php

namespace Rudashi\BookGenerator\App\Classes\Parameters;

use Rudashi\BookGenerator\App\Classes\Contracts\BindInterface;
use Rudashi\BookGenerator\App\Classes\Contracts\CoverInterface;
use Rudashi\BookGenerator\App\Enums\CardboardThickness;

class Weight
{

    private int $cardboard_multiplier;
    public float $cardboard = 0;
    public float $spine = 0;
    public float $case_wrap = 0;
    public float $end_paper = 0;
    private CoverInterface $cover;

    public function __construct(CoverInterface $cover, BindInterface $bind)
    {
        $this->cover = $cover;

        $this->setCaseWrap($cover->net_width, $cover->net_height, $cover::weightMultiplier());

        $this->cardboard_multiplier = $this->getCardboardMultiplier($cover->cardboard);

        if ($this->cardboard_multiplier > 0) {
            $this->cardboard    = $this->getCardboard($cover->page_width_rear, $cover->page_width_front, $cover->page_height);
            $this->spine        = $this->getSpine($cover->spine, $cover->page_height, $this->getSpineMultiplier($bind->isSpineRound()));
            $this->end_paper    = $this->getEndPaper($cover->page_width_rear, $cover->page_width_front, $cover->page_height);
        }
    }

    public function setCaseWrap(float $width, float $height, float $multiplier): self
    {
        $this->case_wrap = $this->setWeight($width, $height, $multiplier);

        return $this;
    }

    public function getCover(): CoverInterface
    {
        return $this->cover;
    }

    private function getCardboardMultiplier(?float $cardboard): int
    {
        switch ($cardboard) {
            case CardboardThickness::CARDBOARD_0_8:
                return 550;
            case CardboardThickness::CARDBOARD_1:
                return 630;
            case CardboardThickness::CARDBOARD_1_5:
                return 920;
            case CardboardThickness::CARDBOARD_2:
                return 1260;
            case CardboardThickness::CARDBOARD_2_5:
                return 1540;
            case CardboardThickness::CARDBOARD_3:
                return 1845;
            default :
                return 0;
        }
    }

    private function getSpineMultiplier(bool $isSpineRound): float
    {
        return $isSpineRound ? 300 : $this->cardboard_multiplier;
    }

    public function calculate(): float
    {
        return $this->case_wrap + $this->spine + $this->cardboard + $this->end_paper;
    }

    private function setWeight(float $width, float $height, float $multiplier): float
    {
        return $width * $height * $multiplier / 1000000;
    }

    private function getSpine(float $spine, float $height, float $multiplier): float
    {
        return $this->setWeight($spine, $height, $multiplier);
    }

    private function getCardboard(float $back, float $front, float $height): float
    {
        return $this->setWeight($back, $height, $this->cardboard_multiplier)
            + $this->setWeight($front, $height, $this->cardboard_multiplier);
    }

    private function getEndPaper(float $back, float $front, float $height): float
    {
        return (2 * $this->setWeight($back, $height, 140))
            + (2 * $this->setWeight($front, $height, 140));
    }

}
