<?php

namespace Rudashi\BookGenerator\App\Classes\Parameters;

use Illuminate\Http\Request;

class Flaps
{

    public bool $flaps = false;
    public bool $symmetric = false;
    public ?int $width = null;
    public ?int $left = null;
    public ?int $right = null;
    public ?int $thickness = null;

    public function __construct(Request $request = null)
    {
        if ($request !== null) {
            $this->flaps        = !($request->input('flaps') === null) && $request->input('flaps');
            $this->symmetric    = !($request->input('flaps_symmetric') === null) && $request->input('flaps_symmetric');
            $this->width        = $request->input('flaps_width');
            $this->left         = $request->input('flaps_left');
            $this->right        = $request->input('flaps_right');
            $this->thickness    = $request->input('flaps_thickness');
        }
    }

    public function validate(): self
    {
        if ($this->flaps === false) {
            $this->symmetric    = false;
            $this->thickness    = null;
        }
        if ($this->symmetric === false) {
            $this->width        = null;
        }
        if ($this->thickness === null || $this->symmetric === true) {
            $this->left         = null;
            $this->right        = null;
        }

        return $this;
    }

}
