<?php

namespace Rudashi\BookGenerator\App\Classes\Contracts;

interface CoverInterface
{

    public function setBinding(?BindInterface $binding);

    public function create(BindInterface $binding = null);

    public function setDimensions();

    public static function weightMultiplier(): int;

    public function setPageWidthFront(BindInterface $binding);

    public function setPageWidthRear(BindInterface $binding);

    public function setPageHeight(BindInterface $binding);

    public function setHinge(BindInterface $binding);

    public function setBig(BindInterface $binding);

    public function setSpine(BindInterface $binding);

    public function setCardboard(BindInterface $binding);

    public function setBleeds(BindInterface $binding);

    public function setBleedsInner(BindInterface $binding);

    public function setFlapBack(BindInterface $binding);

    public function setFlapFront(BindInterface $binding);

    public function setTotalWidth();

    public function setTotalHeight();

    public function setNetWidth();

    public function setNetHeight();

    public function setWeight(BindInterface $binding);

}
