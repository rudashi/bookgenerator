<?php

namespace Rudashi\BookGenerator\App\Classes\Contracts;

use Illuminate\Support\Collection;
use Rudashi\BookGenerator\App\Classes\BookParameters;
use Rudashi\BookGenerator\App\Classes\Parameters\Flaps;
use Rudashi\BookGenerator\App\Classes\Parameters\Paper;
use Rudashi\BookGenerator\App\Classes\Parameters\Spiral;
use Rudashi\BookGenerator\App\Classes\Parameters\Weight;
use Rudashi\BookGenerator\App\Classes\Validate;

interface BindInterface
{

    public function getBasicInformation(): array;

    public function create(BookParameters $parameters = null);

    public function validateParameters(Validate $validate): void;

    public function setWidth(int $width = 0);

    public function setHeight(int $height = 0);

    public function setPapers(Collection $collection);

    public function setSpine(int $spine = null);

    public function setSpineEncore(int $spine_encore = null);

    public function setCardboard(float $cardboard = null);

    public function setFlaps(Flaps $flaps);

    public function setPrinter(string $printer = null) ;

    public function setShowBig(int $show_big = null);

    public function setBlockThickness(Collection $paperCollection = null);

    public function setBookThickness();

    public function setPagesTotal(Collection $paperCollection = null);

    public function setPagesMinimum(Collection $paperCollection = null);

    public function setPagesMaximum(Collection $paperCollection = null);

    public function setWeight(Collection $paperCollection = null);

    public function isPrinterToner(string $printer = null): bool;

    public function isSpineCustom(float $spine = null): bool;

    public function isSpineRound(int $spine = null): bool;

    public function getCustomSpine(): ?float;

    public function doSurplusBlock(Paper $paper): float;

    public function doPageWidthRear(int $width, Flaps $flap = null): int;

    public function doPageWidthFront(int $width, Flaps $flap = null): int;

    public function doPageHeight(int $height = 0): int;

    public function doHinge(): int;

    public function doBig(): int;

    public function doCardboard(float $cardboard = 0): float;

    public function doBleeds(): int;

    public function doBleedsInner(): int;

    public function doSpine(): float;

    public function doSpiral(Spiral $spiral): ?Spiral;

    public function doFlapRear(Flaps $flaps): int;

    public function doFlapFront(Flaps $flaps): int;

    public function doWeight(Weight $weight): float;

}
