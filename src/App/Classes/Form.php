<?php

namespace Rudashi\BookGenerator\App\Classes;

use Rudashi\BookGenerator\App\Enums\SpiralPosition;
use Rudashi\BookGenerator\App\Exceptions\InvalidBindTypeException;
use Rudashi\Profis\Enums\BindType;
use Rudashi\BookGenerator\App\Enums\CardboardThickness;
use Rudashi\BookGenerator\App\Enums\FlapThickness;
use Rudashi\BookGenerator\App\Enums\Language;
use Rudashi\BookGenerator\App\Enums\PrinterType;
use Rudashi\BookGenerator\App\Enums\SpineType;

class Form
{

    private Book $book;

    public function __construct(Book $book)
    {
        $this->book = $book;
    }

    public function getPrinterTypes(): array
    {
        foreach (PrinterType::toArray() as $key => $value) {
            $selectArray[] = [
                'key' => $key,
                'value' => $value,
                'text' => PrinterType::getDescription($value),
                'description' => PrinterType::getMachines($value),
            ];
        }

        return $selectArray ?? [];
    }

    public function getBindTypes(): array
    {
        foreach (BindType::toArray() as $key => $value) {
            try {
                $selectArray[] = array_merge(
                    ['key' => $key, 'value' => $value],
                    $this->book->getBindingType($value)->getBasicInformation()
                );
            } catch (InvalidBindTypeException $e) {
            }
        }
        return $selectArray ?? [];
    }

    public function getSpineTypes(): array
    {
        foreach (SpineType::toArray() as $key => $value) {
            $selectArray[$value] = [
                'key' => $key,
                'value' => $value,
                'text' => SpineType::getDescription($value),
            ];
        }

        return $selectArray ?? [];
    }

    public function getCardboardTypes(): array
    {
        foreach (CardboardThickness::toArray() as $key => $value) {
            $selectArray[] = [
                'key' => $key,
                'value' => $value,
                'text' => $value,
            ];
        }

        return $selectArray ?? [];
    }

    public function getFlapsThickness(): array
    {
        foreach (FlapThickness::toArray() as $key => $value) {
            $selectArray[] = [
                'key' => $key,
                'value' => $value,
                'text' => $value,
            ];
        }

        return $selectArray ?? [];
    }

    public function getLanguages(): array
    {
        foreach (Language::toArray() as $key => $value) {
            $selectArray[] = [
                'key' => $key,
                'value' => $value,
                'text' => Language::getDescription($value),
            ];
        }

        return $selectArray ?? [];
    }

    public function getSpiralPosition(): array
    {
        foreach (SpiralPosition::toArray() as $key => $value) {
            $selectArray[] = [
                'key' => $key,
                'value' => $value,
                'text' => SpiralPosition::getDescription($value),
            ];
        }

        return $selectArray ?? [];
    }

}
