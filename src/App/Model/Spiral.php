<?php

namespace Rudashi\BookGenerator\App\Model;

use Illuminate\Database\Eloquent\Model;
use Totem\SamCore\App\Traits\HasUuid;

/**
 * @property string uuid
 * @property float block_thickness
 * @property string spiral
 * @property int holes
 * @property int holes_spool
 */
class Spiral extends Model
{
    use HasUuid;

    protected $casts = [
        'block_thickness'   => 'float',
        'holes'             => 'int',
        'holes_spool'       => 'int',
    ];

    public function __construct(array $attributes = [])
    {
        $this->fillable([
            'block_thickness',
            'spiral',
            'holes',
            'holes_spool',
        ]);

        $this->setTable('book_generator_spirals');

        parent::__construct($attributes);
    }

}
