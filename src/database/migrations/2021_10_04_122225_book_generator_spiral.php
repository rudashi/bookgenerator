<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

class BookGeneratorSpiral extends Migration
{

    /**
     * @return void
     * @throws Exception
     */
    public function up(): void
    {
        try {
            Schema::create('book_generator_spirals', static function (Blueprint $table) {
                $table->uuid('id');
                $table->timestamps();
                $table->decimal('block_thickness');
                $table->string('spiral');
                $table->integer('holes');
                $table->integer('holes_spool');
            });

            Artisan::call('db:seed', [
                '--class' => Rudashi\BookGenerator\Database\Seeds\BookGenerationSpiralSeeder::class,
            ]);

        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    public function down(): void
    {
        Schema::drop('book_generator_spirals');
    }

}
