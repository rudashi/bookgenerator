<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

class BookGenerator extends Migration
{

    /**
     * @return void
     * @throws Exception
     */
    public function up(): void
    {
        try {
            Schema::create('book_generator_logs', static function (Blueprint $table) {
                $table->increments('id');
                $table->timestamp('created_at');
                $table->integer('user_id')->unsigned()->nullable();
                $table->string('user');
                $table->string('job_title')->nullable();
                $table->string('type');
                $table->string('language');
                $table->json('request');
                $table->json('output');

                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('no action')->onDelete('no action');
            });

            Artisan::call('db:seed', [
                '--class' => Rudashi\BookGenerator\Database\Seeds\BookGenerationPermissionSeeder::class,
            ]);

        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    public function down(): void
    {
        Schema::drop('book_generator_logs');

        (new Rudashi\BookGenerator\Database\Seeds\BookGenerationPermissionSeeder)->down();
    }

}
