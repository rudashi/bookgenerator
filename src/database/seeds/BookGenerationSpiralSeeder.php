<?php

namespace Rudashi\BookGenerator\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Rudashi\BookGenerator\App\Model\Spiral;

class BookGenerationSpiralSeeder extends Seeder
{

    public function run(): void
    {
        $now = now();
        $data = [
            [
                'id' => Str::uuid(),
                'created_at' => $now,
                'block_thickness' => 4.8,
                'spiral' => '1/4',
                'holes' => 3,
                'holes_spool' => 91000,
            ],
            [
                'id' => Str::uuid(),
                'created_at' => $now,
                'block_thickness' => 6.4,
                'spiral' => '5/16',
                'holes' => 3,
                'holes_spool' => 64000,
            ],
            [
                'id' => Str::uuid(),
                'created_at' => $now,
                'block_thickness' => 7.9,
                'spiral' => '3/8',
                'holes' => 3,
                'holes_spool' => 45000,
            ],
            [
                'id' => Str::uuid(),
                'created_at' => $now,
                'block_thickness' => 9.5,
                'spiral' => '7/16',
                'holes' => 3,
                'holes_spool' => 32000,
            ],
            [
                'id' => Str::uuid(),
                'created_at' => $now,
                'block_thickness' => 11.1,
                'spiral' => '1/2',
                'holes' => 3,
                'holes_spool' => 25000,
            ],
            [
                'id' => Str::uuid(),
                'created_at' => $now,
                'block_thickness' => 12.7,
                'spiral' => '9/16',
                'holes' => 3,
                'holes_spool' => 21000,
            ],
            [
                'id' => Str::uuid(),
                'created_at' => $now,
                'block_thickness' => 14.3,
                'spiral' => '5/8',
                'holes' => 2,
                'holes_spool' => 12000,
            ],
            [
                'id' => Str::uuid(),
                'created_at' => $now,
                'block_thickness' => 17.4,
                'spiral' => '3/4',
                'holes' => 2,
                'holes_spool' => 8000,
            ],
            [
                'id' => Str::uuid(),
                'created_at' => $now,
                'block_thickness' => 20.6,
                'spiral' => '7/8',
                'holes' => 2,
                'holes_spool' => 6000,
            ],
            [
                'id' => Str::uuid(),
                'created_at' => $now,
                'block_thickness' => 23.8,
                'spiral' => '1',
                'holes' => 2,
                'holes_spool' => 4500,
            ],
            [
                'id' => Str::uuid(),
                'created_at' => $now,
                'block_thickness' => 29.9,
                'spiral' => '5/4',
                'holes' => 2,
                'holes_spool' => 2700,
            ],
        ];

        Spiral::query()->insert($data);
    }

}


