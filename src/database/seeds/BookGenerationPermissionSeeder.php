<?php

namespace Rudashi\BookGenerator\Database\Seeds;

use Illuminate\Database\Seeder;
use Totem\SamAcl\Database\PermissionTraitSeeder;

class BookGenerationPermissionSeeder extends Seeder
{
    use PermissionTraitSeeder;

    /**
     * Array of permissions.
     * %action  : view|create|edit|show|delete|modify
     * %name    : translatable from JSON
     *
     * @return array
     *  [
     *      'slug' => 'roles.modify',
     *      'name' => 'Can Modify Roles',
     *      'description' => 'Can modify roles',
     *  ]
     */
    public function permissions(): array
    {
        return [
            [
                'slug' => 'book-generator.view',
                'name' => 'Can View Book generator Form',
                'description' => 'Display Cover Calculator Form',
            ],
        ];
    }

}
