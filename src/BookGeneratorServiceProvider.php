<?php

namespace Rudashi\BookGenerator;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Rudashi\BookGenerator\Events\CoverSchemeGenerated;
use Rudashi\BookGenerator\Listeners\BookLogger;
use Totem\SamCore\App\Traits\TraitServiceProvider;

class BookGeneratorServiceProvider extends ServiceProvider
{

    use TraitServiceProvider;

    public function getNamespace(): string
    {
        return 'book-generator';
    }

    public function boot(): void
    {
        $this->loadAndPublish(
            __DIR__ . '/resources/lang',
            __DIR__ . '/database/migrations',
            __DIR__ . '/resources/views'
        );
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang',  $this->getNamespace());

        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');

        Event::listen(CoverSchemeGenerated::class, BookLogger::class);
    }

    public function register(): void
    {

    }

}
