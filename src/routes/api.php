<?php

use Illuminate\Support\Facades\Route;
use Rudashi\BookGenerator\App\Controllers\ApiGeneratorController;

Route::group(['prefix' => 'api'], static function () {

    Route::middleware(config('sam-admin.guard-api'))->group(static function () {

        Route::middleware('permission:book-generator.view')->prefix('book-generator')->group(static function () {
            Route::post('/', [ApiGeneratorController::class, 'createBook']);
            Route::post('schema', [ApiGeneratorController::class, 'pdfOutline']);
            Route::post('pdf', [ApiGeneratorController::class, 'pdf']);
        });
    });

    Route::prefix('book-generator/public')->group(static function () {
        Route::get('/', [ApiGeneratorController::class, 'publicInformation'])->middleware(\Rudashi\BookGenerator\App\LocalizationMiddleware::class);
        Route::post('/', [ApiGeneratorController::class, 'createBook'])->middleware(\Rudashi\BookGenerator\App\LocalizationMiddleware::class);
        Route::post('pdf', [ApiGeneratorController::class, 'pdf']);
        Route::post('schema', [ApiGeneratorController::class, 'pdfOutline']);
    });

});
