<?php

namespace Tests;

use Rudashi\Profis\Enums\BindType;
use Rudashi\BookGenerator\App\Enums\CardboardThickness;
use Rudashi\BookGenerator\App\Enums\CoverType;
use Rudashi\BookGenerator\App\Enums\FlapThickness;
use Rudashi\BookGenerator\App\Enums\Language;
use Rudashi\BookGenerator\App\Enums\PrinterType;
use Rudashi\BookGenerator\App\Enums\SpineType;

class StapledTest extends BookGeneratorTest
{

    public function testCoverToner_21(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 165,
            'height' => 240,
            'bind_type' => BindType::STAPLED,
            'paper' => [
                [
                    'pages' => 168,
                    'type' => '15',
                    'weight' => 115,
                    'volume' => 0.98,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 1,
            'flaps_symmetric' => 0,
            'flaps_width' => 60,
            'flaps_left' => 60,
            'flaps_right' => 70,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => 0,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(166.0, $book->cover->page_width_rear);
        self::assertSame(166.0, $book->cover->page_width_front);
        self::assertSame(240, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(0.0, $book->cover->spine);

        self::assertSame(60, $book->cover->flaps_left);
        self::assertSame(70, $book->cover->flaps_right);

        self::assertSame(472.0, $book->cover->total_width);
        self::assertSame(250, $book->cover->total_height);

        self::assertSame(9.4668, $book->binding->papers->first()->thickness);
        self::assertSame(9.4668, $book->binding->block_thickness);

        self::assertSame(382.536, $book->binding->papers->first()->mass);
        self::assertSame(33.264, $book->cover->weight);
        self::assertSame(415.8, $book->weight);

        self::assertCount(0, $book->validation->getMessages());

    }

    public function testJacketToner_22(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::JACKET,
            'width' => 130,
            'height' => 184,
            'bind_type' => BindType::STAPLED,
            'paper' => [
                [
                    'pages' => 688,
                    'type' => '15',
                    'weight' => 90,
                    'volume' => 0.97,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 1,
            'flaps_symmetric' => 1,
            'flaps_width' => 50,
            'flaps_left' => 60,
            'flaps_right' => 70,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => 0,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(130.5, $book->cover->page_width_rear);
        self::assertSame(130.5, $book->cover->page_width_front);
        self::assertSame(184, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(0.0, $book->cover->spine);

        self::assertSame(50, $book->cover->flaps_left);
        self::assertSame(50, $book->cover->flaps_right);

        self::assertSame(371.0, $book->cover->total_width);
        self::assertSame(194, $book->cover->total_height);

        self::assertSame(30.0312, $book->binding->papers->first()->thickness);
        self::assertSame(30.0312, $book->binding->block_thickness);

        self::assertSame(740.5632, $book->binding->papers->first()->mass);
        self::assertSame(8.96724, $book->cover->weight);
        self::assertSame(763.88244, $book->weight);

        self::assertCount(0, $book->validation->getMessages());

    }

    public function testCoverToner_23(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 146,
            'height' => 205,
            'bind_type' => BindType::STAPLED,
            'paper' => [
                [
                    'pages' => 140,
                    'type' => '15',
                    'weight' => 80,
                    'volume' => 1.25,
                ],
                [
                    'pages' => 12,
                    'type' => '15',
                    'weight' => 80,
                    'volume' => 1.25,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 0,
            'flaps_symmetric' => 1,
            'flaps_width' => 50,
            'flaps_left' => 60,
            'flaps_right' => 70,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => 0,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(146.0, $book->cover->page_width_rear);
        self::assertSame(146.0, $book->cover->page_width_front);
        self::assertSame(205, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(0.0, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(302.0, $book->cover->total_width);
        self::assertSame(215, $book->cover->total_height);

        self::assertSame(7.0, $book->binding->papers->first()->thickness);
        self::assertSame(7.6, $book->binding->block_thickness);

        self::assertSame(167.608, $book->binding->papers->first()->mass);
        self::assertSame(17.958, $book->cover->weight);
        self::assertSame(185.566 + 14.3664, $book->weight);

        self::assertCount(0, $book->validation->getMessages());

    }

    public function testCoverInkJet_24(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 170,
            'height' => 240,
            'bind_type' => BindType::STAPLED,
            'paper' => [
                [
                    'pages' => 200,
                    'type' => '15',
                    'weight' => 115,
                    'volume' => 0.98,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 0,
            'flaps_symmetric' => 1,
            'flaps_width' => 50,
            'flaps_left' => 60,
            'flaps_right' => 70,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::INK_JET,
            'job_name' => null,
            'show_big' => 0,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(170.0, $book->cover->page_width_rear);
        self::assertSame(170.0, $book->cover->page_width_front);
        self::assertSame(240, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(0.0, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(350.0, $book->cover->total_width);
        self::assertSame(250, $book->cover->total_height);

        self::assertSame(11.27, $book->binding->papers->first()->thickness);
        self::assertSame(11.27, $book->binding->block_thickness);

        self::assertSame(469.2, $book->binding->papers->first()->mass);
        self::assertSame(24.48, $book->cover->weight);
        self::assertSame(493.68, $book->weight);

        self::assertCount(0, $book->validation->getMessages());

    }

}
