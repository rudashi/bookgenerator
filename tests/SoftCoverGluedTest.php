<?php

namespace Tests;

use Rudashi\Profis\Enums\BindType;
use Rudashi\BookGenerator\App\Enums\CardboardThickness;
use Rudashi\BookGenerator\App\Enums\CoverType;
use Rudashi\BookGenerator\App\Enums\FlapThickness;
use Rudashi\BookGenerator\App\Enums\Language;
use Rudashi\BookGenerator\App\Enums\PrinterType;
use Rudashi\BookGenerator\App\Enums\SpineType;

class SoftCoverGluedTest extends BookGeneratorTest
{

    public function testCoverToner_13(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 220,
            'height' => 280,
            'bind_type' => BindType::SOFTCOVER_GLUED,
            'paper' => [
                [
                    'pages' => 116,
                    'type' => '15',
                    'weight' => 100,
                    'volume' => 1.13,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 0,
            'flaps_symmetric' => 1,
            'flaps_width' => 60,
            'flaps_left' => 50,
            'flaps_right' => 50,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => 1,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(7, $book->cover->big);

        self::assertSame(213.0, $book->cover->page_width_rear);
        self::assertSame(213.0, $book->cover->page_width_front);
        self::assertSame(280, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(7.5, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(457.5, $book->cover->total_width);
        self::assertSame(290, $book->cover->total_height);

        self::assertSame(6.554, $book->binding->papers->first()->thickness);
        self::assertSame(6.554, $book->binding->block_thickness);

        self::assertSame(357.28, $book->binding->papers->first()->mass);
        self::assertSame(37.59, $book->cover->weight);
        self::assertSame(394.87, $book->weight);

        self::assertCount(0, $book->validation->getMessages());
    }

    public function testJacketInkJet_14(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::JACKET,
            'width' => 220,
            'height' => 280,
            'bind_type' => BindType::SOFTCOVER_GLUED,
            'paper' => [
                [
                    'pages' => 116,
                    'type' => '15',
                    'weight' => 100,
                    'volume' => 1.13,
                ],
                [
                    'pages' => 38,
                    'type' => '15',
                    'weight' => 90,
                    'volume' => 1.3,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 1,
            'flaps_symmetric' => 1,
            'flaps_width' => 50,
            'flaps_left' => 50,
            'flaps_right' => 50,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::INK_JET,
            'job_name' => null,
            'show_big' => 1,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(220.5, $book->cover->page_width_rear);
        self::assertSame(220.5, $book->cover->page_width_front);
        self::assertSame(280, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(10.0, $book->cover->spine);

        self::assertSame(50, $book->cover->flaps_left);
        self::assertSame(50, $book->cover->flaps_right);

        self::assertSame(561.0, $book->cover->total_width);
        self::assertSame(290, $book->cover->total_height);

        self::assertSame(6.554, $book->binding->papers->first()->thickness);
        self::assertSame(8.777, $book->binding->block_thickness);

        self::assertSame(357.28, $book->binding->papers->first()->mass);
        self::assertSame(20.8278, $book->cover->weight);
        self::assertSame(521.2018, $book->weight);

        self::assertCount(0, $book->validation->getMessages());
    }

    public function testCoverToner_15(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 210,
            'height' => 297,
            'bind_type' => BindType::SOFTCOVER_GLUED,
            'paper' => [
                [
                    'pages' => 214,
                    'type' => '15',
                    'weight' => 80,
                    'volume' => 1.5,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 0,
            'flaps_symmetric' => 1,
            'flaps_width' => 50,
            'flaps_left' => 50,
            'flaps_right' => 50,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => 0,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(210.0, $book->cover->page_width_rear);
        self::assertSame(210.0, $book->cover->page_width_front);
        self::assertSame(297, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(13.5, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(443.5, $book->cover->total_width);
        self::assertSame(307, $book->cover->total_height);

        self::assertSame(12.84, $book->binding->papers->first()->thickness);
        self::assertSame(12.84, $book->binding->block_thickness);

        self::assertSame(533.8872, $book->binding->papers->first()->mass);
        self::assertSame(38.62485, $book->cover->weight);
        self::assertSame(572.51205, $book->weight);

        self::assertCount(0, $book->validation->getMessages());
    }

    public function testCoverInkJet_16(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 135,
            'height' => 215,
            'bind_type' => BindType::SOFTCOVER_GLUED,
            'paper' => [
                [
                    'pages' => 240,
                    'type' => '15',
                    'weight' => 80,
                    'volume' => 2,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 1,
            'flaps_symmetric' => 0,
            'flaps_width' => 50,
            'flaps_left' => 55,
            'flaps_right' => 75,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => 1,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(7, $book->cover->big);

        self::assertSame(129.0, $book->cover->page_width_rear);
        self::assertSame(129.0, $book->cover->page_width_front);
        self::assertSame(215, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(20.0, $book->cover->spine);

        self::assertSame(55, $book->cover->flaps_left);
        self::assertSame(75, $book->cover->flaps_right);

        self::assertSame(432.0, $book->cover->total_width);
        self::assertSame(225, $book->cover->total_height);

        self::assertSame(19.2, $book->binding->papers->first()->thickness);
        self::assertSame(19.2, $book->binding->block_thickness);

        self::assertSame(278.64, $book->binding->papers->first()->mass);
        self::assertSame(27.219, $book->cover->weight);
        self::assertSame(305.859, $book->weight);

        self::assertCount(0, $book->validation->getMessages());
    }

    public function testCoverToner_17(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 148,
            'height' => 210,
            'bind_type' => BindType::SOFTCOVER_GLUED,
            'paper' => [
                [
                    'pages' => 184,
                    'type' => '15',
                    'weight' => 90,
                    'volume' => 1.5,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 0,
            'flaps_symmetric' => 1,
            'flaps_width' => 50,
            'flaps_left' => 50,
            'flaps_right' => 50,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => 1,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(7, $book->cover->big);

        self::assertSame(141.0, $book->cover->page_width_rear);
        self::assertSame(141.0, $book->cover->page_width_front);
        self::assertSame(210, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(13.0, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(319.0, $book->cover->total_width);
        self::assertSame(220, $book->cover->total_height);

        self::assertSame(12.42, $book->binding->papers->first()->thickness);
        self::assertSame(12.42, $book->binding->block_thickness);

        self::assertSame(257.3424, $book->binding->papers->first()->mass);
        self::assertSame(19.467, $book->cover->weight);
        self::assertSame(276.8094, $book->weight);

        self::assertCount(0, $book->validation->getMessages());
    }

    public function testCoverToner_32(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 148,
            'height' => 210,
            'bind_type' => BindType::SOFTCOVER_GLUED,
            'paper' => [
                [
                    'pages' => 200,
                    'type' => '15',
                    'weight' => 80,
                    'volume' => 1.25,
                ],
                [
                    'pages' => 32,
                    'type' => '15',
                    'weight' => 115,
                    'volume' => 0.97,
                ],
                [
                    'pages' => 16,
                    'type' => '15',
                    'weight' => 70,
                    'volume' => 2.0,
                ],
            ],

            'flaps' => 0,
            'flaps_symmetric' => 1,
            'flaps_width' => 50,
            'flaps_left' => 50,
            'flaps_right' => 50,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => 0,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(148.0, $book->cover->page_width_rear);
        self::assertSame(148.0, $book->cover->page_width_front);
        self::assertSame(210, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(13.5, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(319.5, $book->cover->total_width);
        self::assertSame(220, $book->cover->total_height);

        self::assertSame(10.00, $book->binding->papers->first()->thickness);
        self::assertSame(12.904800000000002, $book->binding->block_thickness);

        self::assertSame(248.64, $book->binding->papers->first()->mass);
        self::assertSame(19.4985, $book->cover->weight);
        self::assertSame((248.64 + 57.1872 + 17.4048 + 19.4985), $book->weight);

        self::assertCount(0, $book->validation->getMessages());
    }

}
