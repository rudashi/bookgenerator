<?php

namespace Tests;

use BenSampo\Enum\Exceptions\InvalidEnumMemberException;
use Rudashi\Profis\Enums\BindType;
use Rudashi\BookGenerator\App\Enums\CoverType;
use Rudashi\BookGenerator\App\Enums\PrinterType;
use Rudashi\BookGenerator\App\Exceptions\InvalidBindTypeException;
use Rudashi\BookGenerator\App\Exceptions\InvalidCoverTypeException;
use Rudashi\BookGenerator\App\Requests\BookRequest;

class DummyTest extends BookGeneratorTest
{

    public function testDummyData_30(): void
    {
        $this->expectException(InvalidCoverTypeException::class);

        $this->repository->create((new BookRequest())->merge([
            'cover_type' => 'dupa',
            'width' => 0,
            'height' => 0,
            'bind_type' => BindType::INTEGRATED,
            'printer' => PrinterType::TONER,
        ]));
    }

    public function testDummyData_31_has_wrong_printer(): void
    {
        $this->expectException(InvalidEnumMemberException::class);

        $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 0,
            'height' => 0,
            'bind_type' => BindType::INTEGRATED,
            'printer' => 'nie wiem',
        ]));
    }

    public function testDummyData_31_has_wrong_binding(): void
    {
        $this->expectException(InvalidBindTypeException::class);

        $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 0,
            'height' => 0,
            'bind_type' => 'twarda z brokatem',
            'printer' => PrinterType::TONER,
        ]));
    }

    public function testDummyData_31_has_wrong_paper_volumen(): void
    {
        $this->expectException(\TypeError::class);

        $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 0,
            'height' => 0,
            'bind_type' => BindType::INTEGRATED,
            'printer' => PrinterType::TONER,
            'paper' => [
                [
                    'pages' => 10,
                    'type' => 'papier',
                    'weight' => 130,
                    'volume' => 'a co to?',
                ],
            ],
        ]));
    }

    public function testDummyData_31_has_wrong_everything(): void
    {
        $this->expectException(\TypeError::class);

        $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 0,
            'height' => 0,
            'bind_type' => 'twarda z brokatem',
            'printer' => 'nie wiem',
            'paper' => [
                [
                    'pages' => 10,
                    'type' => 'papier',     // \TypeError::class
                    'weight' => 130,
                    'volume' => 'a co to?',
                ],
            ],
        ]));
    }

}
