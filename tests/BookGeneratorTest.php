<?php

namespace Tests;

use Rudashi\BookGenerator\App\Classes\Book;

abstract class BookGeneratorTest extends TestCase
{
    protected \Illuminate\Http\Request $request;

    protected Book $repository;

    protected function setUp(): void
    {
        $this->repository = new Book();

        parent::setUp();
    }

}
