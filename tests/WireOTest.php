<?php

namespace Tests;

use Rudashi\BookGenerator\App\Classes\Binds\WireO;
use Rudashi\Profis\Enums\BindType;
use Rudashi\BookGenerator\App\Enums\CardboardThickness;
use Rudashi\BookGenerator\App\Enums\CoverType;
use Rudashi\BookGenerator\App\Enums\FlapThickness;
use Rudashi\BookGenerator\App\Enums\Language;
use Rudashi\BookGenerator\App\Enums\PrinterType;
use Rudashi\BookGenerator\App\Enums\SpineType;

class WireOTest extends BookGeneratorTest
{

    public function testCoverToner_18(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 210,
            'height' => 297,
            'bind_type' => BindType::WIRE_O,
            'paper' => [
                [
                    'pages' => 196,
                    'type' => '15',
                    'weight' => 130,
                    'volume' => 1.25,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 1,
            'flaps_symmetric' => 0,
            'flaps_width' => 60,
            'flaps_left' => 50,
            'flaps_right' => 60,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => 0,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(213.0, $book->cover->page_width_rear);
        self::assertSame(213.0, $book->cover->page_width_front);
        self::assertSame(297, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(5, $book->cover->bleeds_inner);
        self::assertSame(0.0, $book->cover->spine);

        self::assertSame(50, $book->cover->flaps_left);
        self::assertSame(60, $book->cover->flaps_right);

        self::assertSame(556.0, $book->cover->total_width);
        self::assertSame(307, $book->cover->total_height);

        self::assertSame(15.925, $book->binding->papers->first()->thickness);
        self::assertSame(15.925, $book->binding->block_thickness);

        self::assertSame(794.5938, $book->binding->papers->first()->mass);
        self::assertSame(47.7576, $book->cover->weight);
        self::assertSame(842.3514, $book->weight);

        self::assertCount(1, $book->validation->getMessages());

        self::assertArrayHasKey('perforation', $book->validation->getMessages());

        self::assertSame(
            __('Spiral perforation - the recommended margin from the spine edge is :margin mm. It is recommended not to place significant graphics in this area that may fall into the perforation area.', [ 'margin' => WireO::SPIRAL_MARGIN ]),
            $book->validation->get('perforation')[0]
        );
    }

    public function testCoverInkJet_19(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 148,
            'height' => 210,
            'bind_type' => BindType::WIRE_O,
            'paper' => [
                [
                    'pages' => 96,
                    'type' => '15',
                    'weight' => 90,
                    'volume' => 1.25,
                ],
                [
                    'pages' => 14,
                    'type' => '15',
                    'weight' => 200,
                    'volume' => 1.2,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 0,
            'flaps_symmetric' => 1,
            'flaps_width' => 80,
            'flaps_left' => 50,
            'flaps_right' => 60,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::INK_JET,
            'job_name' => null,
            'show_big' => 0,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(150.0, $book->cover->page_width_rear);
        self::assertSame(150.0, $book->cover->page_width_front);
        self::assertSame(210, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(5, $book->cover->bleeds_inner);
        self::assertSame(0.0, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(320.0, $book->cover->total_width); //Stary kalkulator nie podliczał spadów wewnętrznych
        self::assertSame(220, $book->cover->total_height);

        self::assertSame(5.4, $book->binding->papers->first()->thickness);
        self::assertSame(7.08, $book->binding->block_thickness);

        self::assertSame(134.2656, $book->binding->papers->first()->mass);
        self::assertSame(18.9, $book->cover->weight);
        self::assertSame(153.1656 + 43.512, $book->weight);

        self::assertCount(1, $book->validation->getMessages());

        self::assertArrayHasKey('perforation', $book->validation->getMessages());

        self::assertSame(
            __('Spiral perforation - the recommended margin from the spine edge is :margin mm. It is recommended not to place significant graphics in this area that may fall into the perforation area.', [ 'margin' => WireO::SPIRAL_MARGIN ]),
            $book->validation->get('perforation')[0]
        );
    }

    public function testJacketToner_20(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::JACKET,
            'width' => 145,
            'height' => 335,
            'bind_type' => BindType::WIRE_O,
            'paper' => [
                [
                    'pages' => 485,
                    'type' => '15',
                    'weight' => 200,
                    'volume' => 1.25,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 1,
            'flaps_symmetric' => 1,
            'flaps_width' => 80,
            'flaps_left' => 50,
            'flaps_right' => 60,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => 0,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(147.5, $book->cover->page_width_rear);
        self::assertSame(147.5, $book->cover->page_width_front);
        self::assertSame(335, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(5, $book->cover->bleeds_inner);
        self::assertSame(0.0, $book->cover->spine);

        self::assertSame(80, $book->cover->flaps_left);
        self::assertSame(80, $book->cover->flaps_right);

        self::assertSame(475.0, $book->cover->total_width); //Stary kalkulator nie podliczał spadów wewnętrznych
        self::assertSame(345, $book->cover->total_height);

        self::assertSame(60.625, $book->binding->papers->first()->thickness);
        self::assertSame(60.625, $book->binding->block_thickness);

        self::assertSame(2355.8875, $book->binding->papers->first()->mass);
        self::assertSame(20.577375, $book->cover->weight);
        self::assertSame(2406.0118749999997, $book->weight);

        self::assertCount(4, $book->validation->getMessages());

        self::assertArrayHasKey('pages_even', $book->validation->getMessages());
        self::assertArrayHasKey('pages_bind_max', $book->validation->getMessages());
        self::assertArrayHasKey('block_thickness', $book->validation->getMessages());
        self::assertArrayHasKey('perforation', $book->validation->getMessages());

        self::assertSame(
            __('The paper :name count :count is not even.', [ 'count' => $book->binding->papers->first()->pages, 'name' => 1 ]),
            $book->validation->get('pages_even')[0]
        );
        self::assertSame(
            __('The number of pages is too greater for this binding type. The maximum acceptable number of pages for this paper - :pages.', [ 'pages' => 224]),
            $book->validation->get('pages_bind_max')[0]
        );
        self::assertSame(
            __('The block thickness for this type of binding is a maximum :max mm.', [ 'max' => WireO::BLOCK_MAX ]),
            $book->validation->get('block_thickness')[0]
        );
        self::assertSame(
            __('Spiral perforation - the recommended margin from the spine edge is :margin mm. It is recommended not to place significant graphics in this area that may fall into the perforation area.', [ 'margin' => WireO::SPIRAL_MARGIN ]),
            $book->validation->get('perforation')[0]
        );

    }

}
