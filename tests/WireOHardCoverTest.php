<?php

namespace Tests;

use Rudashi\BookGenerator\App\Classes\Binds\HardCoverWireO;
use Rudashi\BookGenerator\App\Classes\Parameters\Spiral;
use Rudashi\BookGenerator\App\Enums\SpiralPosition;
use Rudashi\Profis\Enums\BindType;
use Rudashi\BookGenerator\App\Enums\CardboardThickness;
use Rudashi\BookGenerator\App\Enums\CoverType;
use Rudashi\BookGenerator\App\Enums\Language;
use Rudashi\BookGenerator\App\Enums\PrinterType;

class WireOHardCoverTest extends BookGeneratorTest
{

    public function testCoverToner_37(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 145,
            'height' => 210,
            'bind_type' => BindType::HARDCOVER_WIRE_O,
            'paper' => [
                [
                    'pages' => 100,
                    'type' => '15',
                    'weight' => 70,
                    'volume' => 1.25,
                ],
            ],
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_1_5,
            'flaps' => 0,
            'printer' => PrinterType::TONER,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(148.0, $book->cover->page_width_rear);
        self::assertSame(148.0, $book->cover->page_width_front);
        self::assertSame(213, $book->cover->page_height);

        self::assertSame(18, $book->cover->bleeds);
        self::assertSame(18, $book->cover->bleeds_inner);
        self::assertSame(0.0, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(368.0, $book->cover->total_width);
        self::assertSame(249, $book->cover->total_height);

        self::assertSame(4.375, $book->binding->papers->first()->thickness);
        self::assertSame(4.375, $book->binding->block_thickness);

        self::assertSame(106.575, $book->binding->papers->first()->mass);
        self::assertSame(94.572, $book->cover->weight);
        self::assertSame(201.15, round($book->weight, 2));

        self::assertCount(1, $book->validation->getMessages());
        self::assertArrayHasKey('perforation', $book->validation->getMessages());
        self::assertSame(
            __('Spiral perforation - the recommended margin from the spine edge is :margin mm. It is recommended not to place significant graphics in this area that may fall into the perforation area.', [ 'margin' => HardCoverWireO::SPIRAL_MARGIN ]),
            $book->validation->get('perforation')[0]
        );
    }

    public function testCoverInkJet_38(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 165,
            'height' => 235,
            'bind_type' => BindType::HARDCOVER_WIRE_O,
            'paper' => [
                [
                    'pages' => 200,
                    'type' => '15',
                    'weight' => 80,
                    'volume' => 1.25,
                ],
            ],

            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_2_5,

            'flaps' => 0,

            'printer' => PrinterType::INK_JET,
            'job_name' => null,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(168.0, $book->cover->page_width_rear);
        self::assertSame(168.0, $book->cover->page_width_front);
        self::assertSame(238, $book->cover->page_height);

        self::assertSame(18, $book->cover->bleeds);
        self::assertSame(18, $book->cover->bleeds_inner);
        self::assertSame(0.0, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(408.0, $book->cover->total_width);
        self::assertSame(274, $book->cover->total_height);

        self::assertSame(10.0, $book->binding->papers->first()->thickness);
        self::assertSame(10.0, $book->binding->block_thickness);

        self::assertSame(310.2, $book->binding->papers->first()->mass);
        self::assertSame(169.53216, $book->cover->weight);
        self::assertSame(479.73, round($book->weight, 2));

        self::assertCount(1, $book->validation->getMessages());
        self::assertArrayHasKey('perforation', $book->validation->getMessages());
        self::assertSame(
            __('Spiral perforation - the recommended margin from the spine edge is :margin mm. It is recommended not to place significant graphics in this area that may fall into the perforation area.', [ 'margin' => HardCoverWireO::SPIRAL_MARGIN ]),
            $book->validation->get('perforation')[0]
        );
    }

    public function testCoverInkJet_39(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 170,
            'height' => 240,
            'bind_type' => BindType::HARDCOVER_WIRE_O,
            'paper' => [
                [
                    'pages' => 300,
                    'type' => '15',
                    'weight' => 90,
                    'volume' => 1.25,
                ],
                [
                    'pages' => 10,
                    'type' => '15',
                    'weight' => 150,
                    'volume' => 0.85,
                ],
            ],
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_2_5,
            'flaps' => 0,
            'printer' => PrinterType::INK_JET,
            'job_name' => null,
            'no_logo' => 0,
            'language' => Language::POLISH,
            'spiral' => [
                'color' => 'Wire-O - BIAŁA',
                'position' => SpiralPosition::LONG_SIDE,
            ]
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(174.0, $book->cover->page_width_rear);
        self::assertSame(174.0, $book->cover->page_width_front);
        self::assertSame(243, $book->cover->page_height);

        self::assertSame(18, $book->cover->bleeds);
        self::assertSame(18, $book->cover->bleeds_inner);
        self::assertSame(0.0, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(420.0, $book->cover->total_width);
        self::assertSame(279, $book->cover->total_height);

        self::assertSame(16.875, $book->binding->papers->first()->thickness);
        self::assertSame(17.5125, $book->binding->block_thickness);

        self::assertSame(550.8, $book->binding->papers->first()->mass);
        self::assertSame(179.27568, $book->cover->weight);
        self::assertSame(760.68, round($book->weight, 2));

        self::assertInstanceOf(Spiral::class, $book->binding->spiral);
        self::assertSame('Wire-O - BIAŁA', $book->binding->spiral->color);
        self::assertSame(SpiralPosition::getKey(SpiralPosition::LONG_SIDE), $book->binding->spiral->position);
        self::assertNull($book->binding->spiral->qty);
        self::assertSame('1', $book->binding->spiral->spiral);
        self::assertSame('2:1', $book->binding->spiral->holes);
        self::assertNull($book->binding->spiral->holes_qty);
        self::assertNull($book->binding->spiral->spools_qty);
        self::assertNull($book->binding->spiral->packages_qty);

        self::assertCount(1, $book->validation->getMessages());

        self::assertArrayHasKey('perforation', $book->validation->getMessages());

        self::assertSame(
            __('Spiral perforation - the recommended margin from the spine edge is :margin mm. It is recommended not to place significant graphics in this area that may fall into the perforation area.', [ 'margin' => HardCoverWireO::SPIRAL_MARGIN ]),
            $book->validation->get('perforation')[0]
        );
    }

    public function testCoverToner_40(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 210,
            'height' => 297,
            'bind_type' => BindType::HARDCOVER_WIRE_O,
            'paper' => [
                [
                    'pages' => 400,
                    'type' => '15',
                    'weight' => 100,
                    'volume' => 1.25,
                ],
                [
                    'pages' => 20,
                    'type' => '15',
                    'weight' => 200,
                    'volume' => 0.85,
                ],
            ],
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_2_5,
            'flaps' => 0,
            'printer' => PrinterType::TONER,
            'job_name' => null,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(215.0, $book->cover->page_width_rear);
        self::assertSame(215.0, $book->cover->page_width_front);
        self::assertSame(300, $book->cover->page_height);

        self::assertSame(18, $book->cover->bleeds);
        self::assertSame(18, $book->cover->bleeds_inner);
        self::assertSame(0.0, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(502.0, $book->cover->total_width);
        self::assertSame(336, $book->cover->total_height);

        self::assertSame(25.0, $book->binding->papers->first()->thickness);
        self::assertSame(26.7, $book->binding->block_thickness);

        self::assertSame(1247.4, $book->binding->papers->first()->mass);
        self::assertSame(273.48, $book->cover->weight);
        self::assertSame(1645.62, round($book->weight, 2));

        self::assertCount(2, $book->validation->getMessages());

        self::assertArrayHasKey('perforation', $book->validation->getMessages());
        self::assertArrayHasKey('block_thickness', $book->validation->getMessages());

        self::assertSame(
            __('Spiral perforation - the recommended margin from the spine edge is :margin mm. It is recommended not to place significant graphics in this area that may fall into the perforation area.', [ 'margin' => HardCoverWireO::SPIRAL_MARGIN ]),
            $book->validation->get('perforation')[0]
        );
        self::assertSame(
            __('The block thickness for this type of binding is a maximum :max mm.', [ 'max' => (HardCoverWireO::BLOCK_MAX - (2 * CardboardThickness::CARDBOARD_2_5))]),
            $book->validation->get('block_thickness')[0]
        );
    }

    public function testCoverToner_41(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 210,
            'height' => 297,
            'bind_type' => BindType::HARDCOVER_WIRE_O,
            'paper' => [
                [
                    'pages' => 600,
                    'type' => '15',
                    'weight' => 100,
                    'volume' => 1.25,
                ],
                [
                    'pages' => 20,
                    'type' => '15',
                    'weight' => 200,
                    'volume' => 0.85,
                ],
            ],
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_2_5,
            'flaps' => 0,
            'printer' => PrinterType::TONER,
            'job_name' => null,
            'no_logo' => 0,
            'language' => Language::POLISH,
            'spiral' => [
                'color' => 'Wire-O - BIAŁA',
                'position' => SpiralPosition::LONG_SIDE,
            ]
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(215.0, $book->cover->page_width_rear);
        self::assertSame(215.0, $book->cover->page_width_front);
        self::assertSame(300, $book->cover->page_height);

        self::assertSame(18, $book->cover->bleeds);
        self::assertSame(18, $book->cover->bleeds_inner);
        self::assertSame(0.0, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(502.0, $book->cover->total_width);
        self::assertSame(336, $book->cover->total_height);

        self::assertSame(37.5, $book->binding->papers->first()->thickness);
        self::assertSame(39.2, $book->binding->block_thickness);

        self::assertSame(1871.1, $book->binding->papers->first()->mass);
        self::assertSame(273.48, $book->cover->weight);
        self::assertSame(2269.32, round($book->weight, 2));

        self::assertNull($book->binding->spiral);

        self::assertCount(2, $book->validation->getMessages());

        self::assertArrayHasKey('perforation', $book->validation->getMessages());
        self::assertArrayHasKey('block_thickness', $book->validation->getMessages());

        self::assertSame(
            __('Spiral perforation - the recommended margin from the spine edge is :margin mm. It is recommended not to place significant graphics in this area that may fall into the perforation area.', [ 'margin' => HardCoverWireO::SPIRAL_MARGIN ]),
            $book->validation->get('perforation')[0]
        );
        self::assertSame(
            __('The block thickness for this type of binding is a maximum :max mm.', [ 'max' => (HardCoverWireO::BLOCK_MAX - (2 * CardboardThickness::CARDBOARD_2_5)) ]),
            $book->validation->get('block_thickness')[0]
        );

    }

}
