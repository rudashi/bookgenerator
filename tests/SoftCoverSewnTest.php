<?php

namespace Tests;

use Rudashi\BookGenerator\App\Classes\Binds\SoftCoverSewn;
use Rudashi\Profis\Enums\BindType;
use Rudashi\BookGenerator\App\Enums\CardboardThickness;
use Rudashi\BookGenerator\App\Enums\CoverType;
use Rudashi\BookGenerator\App\Enums\FlapThickness;
use Rudashi\BookGenerator\App\Enums\Language;
use Rudashi\BookGenerator\App\Enums\PrinterType;
use Rudashi\BookGenerator\App\Enums\SpineType;

class SoftCoverSewnTest extends BookGeneratorTest
{

    public function testJacketInkJet_9(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::JACKET,
            'width' => 140,
            'height' => 210,
            'bind_type' => BindType::SOFTCOVER_SEWN,
            'paper' => [
                [
                    'pages' => 80,
                    'type' => '15',
                    'weight' => 100,
                    'volume' => 1.5,
                ],
                [
                    'pages' => 64,
                    'type' => '15',
                    'weight' => 115,
                    'volume' => 1.5,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 1,
            'flaps_symmetric' => 1,
            'flaps_width' => 60,
            'flaps_left' => 50,
            'flaps_right' => 50,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::INK_JET,
            'job_name' => null,
            'show_big' => 1,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(140.5, $book->cover->page_width_rear);
        self::assertSame(140.5, $book->cover->page_width_front);
        self::assertSame(210, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(14.0, $book->cover->spine);

        self::assertSame(60, $book->cover->flaps_left);
        self::assertSame(60, $book->cover->flaps_right);

        self::assertSame(425.0, $book->cover->total_width);
        self::assertSame(220, $book->cover->total_height);

        self::assertSame(6.0, $book->binding->papers->first()->thickness);
        self::assertSame(11.52, $book->binding->block_thickness);

        self::assertSame(117.6, $book->binding->papers->first()->mass);
        self::assertSame(11.76525, $book->cover->weight);
        self::assertSame(256.04775, $book->weight); //Stara waga nie podlicza wkładki 147.79905 + 108.192

        self::assertCount(0, $book->validation->getMessages());
    }

    public function testCoverToner_10(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 148,
            'height' => 215,
            'bind_type' => BindType::SOFTCOVER_SEWN,
            'paper' => [
                [
                    'pages' => 112,
                    'type' => '15',
                    'weight' => 100,
                    'volume' => 1.13,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 0,
            'flaps_symmetric' => 1,
            'flaps_width' => 60,
            'flaps_left' => 50,
            'flaps_right' => 50,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => 0,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(148.0, $book->cover->page_width_rear);
        self::assertSame(148.0, $book->cover->page_width_front);
        self::assertSame(215, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(8.5, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(314.5, $book->cover->total_width);
        self::assertSame(225, $book->cover->total_height);

        self::assertSame(6.328, $book->binding->papers->first()->thickness);
        self::assertSame(6.328, $book->binding->block_thickness);

        self::assertSame(178.192, $book->binding->papers->first()->mass);
        self::assertSame(19.64025, $book->cover->weight);
        self::assertSame(197.83225, $book->weight);

        self::assertCount(0, $book->validation->getMessages());
    }

    public function testJacketToner_11(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::JACKET,
            'width' => 148,
            'height' => 205,
            'bind_type' => BindType::SOFTCOVER_SEWN,
            'paper' => [
                [
                    'pages' => 548,
                    'type' => '15',
                    'weight' => 90,
                    'volume' => 1.3,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 1,
            'flaps_symmetric' => 1,
            'flaps_width' => 50,
            'flaps_left' => 50,
            'flaps_right' => 50,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => 1,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(148.5, $book->cover->page_width_rear);
        self::assertSame(148.5, $book->cover->page_width_front);
        self::assertSame(205, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(34.5, $book->cover->spine);

        self::assertSame(50, $book->cover->flaps_left);
        self::assertSame(50, $book->cover->flaps_right);

        self::assertSame(441.5, $book->cover->total_width);
        self::assertSame(215, $book->cover->total_height);

        self::assertSame(32.058, $book->binding->papers->first()->thickness);
        self::assertSame(32.058, $book->binding->block_thickness);

        self::assertSame(748.1844, $book->binding->papers->first()->mass);
        self::assertSame(11.9417625, $book->cover->weight);
        self::assertSame(780.4211624999999, $book->weight);

        self::assertCount(0, $book->validation->getMessages());
    }

    public function testCoverInkJet_12(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 168,
            'height' => 238,
            'bind_type' => BindType::SOFTCOVER_SEWN,
            'paper' => [
                [
                    'pages' => 396,
                    'type' => '15',
                    'weight' => 80,
                    'volume' => 1.25,
                ],
                [
                    'pages' => 40,
                    'type' => '15',
                    'weight' => 150,
                    'volume' => 0.83,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 0,
            'flaps_symmetric' => 1,
            'flaps_width' => 50,
            'flaps_left' => 50,
            'flaps_right' => 50,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::INK_JET,
            'job_name' => null,
            'show_big' => 0,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(168.0, $book->cover->page_width_rear);
        self::assertSame(168.0, $book->cover->page_width_front);
        self::assertSame(238, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(24.5, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(370.5, $book->cover->total_width);
        self::assertSame(248, $book->cover->total_height);

        self::assertSame(19.8, $book->binding->papers->first()->thickness);
        self::assertSame(22.29, $book->binding->block_thickness);

        self::assertSame(633.34656, $book->binding->papers->first()->mass);
        self::assertSame(25.7397, $book->cover->weight);
        self::assertSame(659.08626 + 119.952, $book->weight);

        self::assertCount(0, $book->validation->getMessages());
    }

    public function testCoverToner_29(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 285,
            'height' => 300,
            'bind_type' => BindType::SOFTCOVER_SEWN,
            'paper' => [
                [
                    'pages' => 10,
                    'type' => '15',
                    'weight' => 80,
                    'volume' => 1.5,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 0,
            'flaps_symmetric' => 1,
            'flaps_width' => 50,
            'flaps_left' => 50,
            'flaps_right' => 50,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::INK_JET,
            'job_name' => null,
            'show_big' => 1,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(7, $book->cover->big);

        self::assertSame(278.0, $book->cover->page_width_rear);
        self::assertSame(278.0, $book->cover->page_width_front);
        self::assertSame(300, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(2.5, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(582.5, $book->cover->total_width);
        self::assertSame(310, $book->cover->total_height);

        self::assertSame(0.6, $book->binding->papers->first()->thickness);
        self::assertSame(0.6, $book->binding->block_thickness);

        self::assertSame(34.2, $book->binding->papers->first()->mass);
        self::assertSame(51.525, $book->cover->weight);
        self::assertSame(85.725, $book->weight);

        self::assertCount(3, $book->validation->getMessages());

        self::assertArrayHasKey('pages_even', $book->validation->getMessages());
        self::assertArrayHasKey('pages_bind_min', $book->validation->getMessages());
        self::assertArrayHasKey('block_thickness', $book->validation->getMessages());

        self::assertSame(
            __('The paper :name count :count is not divisible by four.', [ 'count' => $book->binding->papers->first()->pages, 'name' => 1 ]),
            $book->validation->get('pages_even')[0]
        );
        self::assertSame(
            __('The number of pages is too small for this binding type. The minimum acceptable number of pages for this paper - :pages.', [ 'pages' => 32]),
            $book->validation->get('pages_bind_min')[0]
        );
        self::assertSame(
            __('The block thickness for this type of binding is a minimum :min mm.', [ 'min' => SoftCoverSewn::BLOCK_MIN]),
            $book->validation->get('block_thickness')[0]
        );
    }

    public function testCoverInkJet_33(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 165,
            'height' => 235,
            'bind_type' => BindType::SOFTCOVER_SEWN,
            'paper' => [
                [
                    'pages' => 400,
                    'type' => '15',
                    'weight' => 70,
                    'volume' => 1.25,
                ],
                [
                    'pages' => 32,
                    'type' => '15',
                    'weight' => 130,
                    'volume' => 0.85,
                ],
                [
                    'pages' => 32,
                    'type' => '15',
                    'weight' => 70,
                    'volume' => 2,
                ],
                [
                    'pages' => 16,
                    'type' => '15',
                    'weight' => 140,
                    'volume' => 1.25,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 1,
            'flaps_symmetric' => 1,
            'flaps_width' => 80,
            'flaps_left' => 60,
            'flaps_right' => 70,
            'flaps_thickness' => FlapThickness::FLAP_2,

            'printer' => PrinterType::INK_JET,
            'job_name' => null,
            'show_big' => 1,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(7, $book->cover->big);

        self::assertSame(160.0, $book->cover->page_width_rear);
        self::assertSame(160.0, $book->cover->page_width_front);
        self::assertSame(235, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(25.0, $book->cover->spine);

        self::assertSame(80, $book->cover->flaps_left);
        self::assertSame(80, $book->cover->flaps_right);

        self::assertSame(529.0, $book->cover->total_width);
        self::assertSame(245, $book->cover->total_height);

        self::assertSame(17.5, $book->binding->papers->first()->thickness);
        self::assertSame(22.908, $book->binding->block_thickness);

        self::assertSame(542.85, $book->binding->papers->first()->mass);
        self::assertSame(36.5895, $book->cover->weight);
        self::assertSame(746.9475, $book->weight); // 80.652 + 43.428 + 43.428

        self::assertCount(0, $book->validation->getMessages());

    }

}
