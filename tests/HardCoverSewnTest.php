<?php

namespace Tests;

use Rudashi\BookGenerator\App\Classes\Binds\HardCoverSewn;
use Rudashi\Profis\Enums\BindType;
use Rudashi\BookGenerator\App\Enums\CardboardThickness;
use Rudashi\BookGenerator\App\Enums\CoverType;
use Rudashi\BookGenerator\App\Enums\FlapThickness;
use Rudashi\BookGenerator\App\Enums\Language;
use Rudashi\BookGenerator\App\Enums\PrinterType;
use Rudashi\BookGenerator\App\Enums\SpineType;

class HardCoverSewnTest extends BookGeneratorTest
{

    public function testCoverInkJet_1(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => '135',
            'height' => '190',
            'bind_type' => BindType::HARDCOVER_SEWN,
            'paper' => [
                [
                    'pages' => '36',
                    'type' => '15',
                    'weight' => '170',
                    'volume' => '0.83',
                ]
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_2_5,

            'flaps' => 0,
//            'flaps_symmetric' => 0,
            'flaps_width' => '80',
            'flaps_left' => '50',
            'flaps_right' => '50',
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::INK_JET,
            'job_name' => null,
            'show_big' => '1',
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertNull($book->name);
        self::assertSame(184.19, round($book->weight, 2));
        self::assertSame(11, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(130.0, $book->cover->page_width_rear);
        self::assertSame(130.0, $book->cover->page_width_front);
        self::assertSame(196, $book->cover->page_height);

        self::assertSame(18, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(9.0, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(327.0, $book->cover->total_width);
        self::assertSame(232, $book->cover->total_height);

        self::assertSame(2.5398, $book->binding->papers->first()->thickness);
        self::assertSame(2.5398, $book->binding->block_thickness);

        self::assertCount(3, $book->validation->getMessages());
        self::assertArrayHasKey('cardboard', $book->validation->getMessages());
        self::assertArrayHasKey('pages_bind_min', $book->validation->getMessages());
        self::assertArrayHasKey('block_thickness', $book->validation->getMessages());

        self::assertSame(
            __('For publications with such a small volume, do not use cardboard thicker than :max mm.', [ 'max' => CardboardThickness::CARDBOARD_2]),
            $book->validation->get('cardboard')[0]
        );
        self::assertSame(
            __('The number of pages is too small for this binding type. The minimum acceptable number of pages for this paper - :pages.', [ 'pages' => 56]),
            $book->validation->get('pages_bind_min')[0]
        );
        self::assertSame(
            __('The block thickness for this type of binding is a minimum :min mm.', [ 'min' => HardCoverSewn::BLOCK_MIN]),
            $book->validation->get('block_thickness')[0]
        );
    }

    public function testJacketToner_2(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::JACKET,
            'width' => 115,
            'height' => 180,
            'bind_type' => BindType::HARDCOVER_SEWN,
            'paper' => [
                [
                    'pages' => 116,
                    'type' => 15,
                    'weight' => 100,
                    'volume' => 1.5,
                ]
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_2,

            'flaps' => 1,
            'flaps_symmetric' => 1,
            'flaps_width' => 50,
            'flaps_left' => 50,
            'flaps_right' => 50,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => '1',
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(123.5, $book->cover->page_width_rear);
        self::assertSame(123.5, $book->cover->page_width_front);
        self::assertSame(186, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(14.5, $book->cover->spine);

        self::assertSame(50, $book->cover->flaps_left);
        self::assertSame(50, $book->cover->flaps_right);

        self::assertSame(371.5, $book->cover->total_width);
        self::assertSame(196, $book->cover->total_height);

        self::assertSame(8.73364, $book->binding->papers->first()->thickness);
        self::assertSame(8.73364, $book->binding->block_thickness);

        self::assertSame(120.06, $book->binding->papers->first()->mass);
        self::assertSame(9.077265, $book->cover->weight);
        self::assertSame(204.18634500000002, $book->weight);

        self::assertCount(0, $book->validation->getMessages());
    }

    public function testCoverInkJet_3(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 148,
            'height' => 210,
            'bind_type' => BindType::HARDCOVER_SEWN,
            'paper' => [
                [
                    'pages' => 192,
                    'type' => '15',
                    'weight' => 80,
                    'volume' => 1.5,
                ]
            ],

            'spine' => SpineType::ROUND,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 0,
//            'flaps_symmetric' => 0,
            'flaps_width' => '80',
            'flaps_left' => '50',
            'flaps_right' => '50',
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::INK_JET,
            'job_name' => null,
            'show_big' => '1',
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertNull($book->name);
        self::assertSame(383.51, round($book->weight, 2));
        self::assertSame(9, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(143.0, $book->cover->page_width_rear);
        self::assertSame(143.0, $book->cover->page_width_front);
        self::assertSame(216, $book->cover->page_height);

        self::assertSame(18, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(20.0, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(360.0, $book->cover->total_width);
        self::assertSame(252, $book->cover->total_height);

        self::assertSame(11.52, $book->binding->papers->first()->thickness);
        self::assertSame(11.52, $book->binding->block_thickness);

        self::assertCount(0, $book->validation->getMessages());
    }

    public function testCoverToner_4(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 170,
            'height' => 240,
            'bind_type' => BindType::HARDCOVER_SEWN,
            'paper' => [
                [
                    'pages' => 184,
                    'type' => '15',
                    'weight' => 130,
                    'volume' => 0.99,
                ],
                [
                    'pages' => 80,
                    'type' => '15',
                    'weight' => 90,
                    'volume' => 1.5,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_2,

            'flaps' => 0,
//            'flaps_symmetric' => 0,
            'flaps_width' => '80',
            'flaps_left' => '50',
            'flaps_right' => '50',
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => '1',
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertNull($book->name);
        self::assertSame(782.64, round($book->weight, 2)); // Starta waga 635.76 nie podliczała wkładki
        self::assertSame(11, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(165.0, $book->cover->page_width_rear);
        self::assertSame(165.0, $book->cover->page_width_front);
        self::assertSame(246, $book->cover->page_height);

        self::assertSame(18, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(23.0, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(411.0, $book->cover->total_width);
        self::assertSame(282, $book->cover->total_height);

        self::assertSame(11.92504, $book->binding->papers->first()->thickness);
        self::assertSame(17.34104, $book->binding->block_thickness);

        self::assertCount(0, $book->validation->getMessages());
    }

    public function testCoverInkJet_34(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 210,
            'height' => 297,
            'bind_type' => BindType::HARDCOVER_SEWN,
            'paper' => [
                [
                    'pages' => 116,
                    'type' => '15',
                    'weight' => 115,
                    'volume' => 0.8,
                ],
                [
                    'pages' => 32,
                    'type' => '15',
                    'weight' => 80,
                    'volume' => 2,
                ],
                [
                    'pages' => 48,
                    'type' => '15',
                    'weight' => 80,
                    'volume' => 1.25,
                ],
            ],

            'spine' => SpineType::ROUND,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_2_5,

            'flaps' => 0,
            'flaps_symmetric' => 0,
            'flaps_width' => '80',
            'flaps_left' => '50',
            'flaps_right' => '50',
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::INK_JET,
            'job_name' => null,
            'show_big' => '1',
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(9, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(205.0, $book->cover->page_width_rear);
        self::assertSame(205.0, $book->cover->page_width_front);
        self::assertSame(303, $book->cover->page_height);

        self::assertSame(18, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(18.0, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(482.0, $book->cover->total_width);
        self::assertSame(339, $book->cover->total_height);

        self::assertSame(5.336, $book->binding->papers->first()->thickness);
        self::assertSame(10.296000000000001, $book->binding->block_thickness);

        self::assertSame(416.0079, $book->binding->papers->first()->mass);
        self::assertSame(249.79353, $book->cover->weight);
        self::assertSame((416.0079 + 79.8336 + 119.7504 + 249.79353), $book->weight);

        self::assertCount(0, $book->validation->getMessages());
    }

}
