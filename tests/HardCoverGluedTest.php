<?php

namespace Tests;

use Rudashi\BookGenerator\App\Classes\Binds\HardCoverGlued;
use Rudashi\Profis\Enums\BindType;
use Rudashi\BookGenerator\App\Enums\CardboardThickness;
use Rudashi\BookGenerator\App\Enums\CoverType;
use Rudashi\BookGenerator\App\Enums\FlapThickness;
use Rudashi\BookGenerator\App\Enums\Language;
use Rudashi\BookGenerator\App\Enums\PrinterType;
use Rudashi\BookGenerator\App\Enums\SpineType;

class HardCoverGluedTest extends BookGeneratorTest
{

    public function testCoverToner_5(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 124,
            'height' => 194,
            'bind_type' => BindType::HARDCOVER_GLUED,
            'paper' => [
                [
                    'pages' => 272,
                    'type' => '15',
                    'weight' => 80,
                    'volume' => 2,
                ],
            ],

            'spine' => SpineType::ROUND,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_2,

            'flaps' => 0,
//            'flaps_symmetric' => 0,
            'flaps_width' => '80',
            'flaps_left' => '50',
            'flaps_right' => '50',
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => '1',
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertNull($book->name);
        self::assertSame(9, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(119.0, $book->cover->page_width_rear);
        self::assertSame(119.0, $book->cover->page_width_front);
        self::assertSame(200, $book->cover->page_height);

        self::assertSame(18, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(28.5, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(320.5, $book->cover->total_width);
        self::assertSame(236, $book->cover->total_height);

        self::assertSame(21.94496, $book->binding->papers->first()->thickness);
        self::assertSame(21.94496, $book->binding->block_thickness);

        self::assertSame(346.95, round($book->weight, 2));

        self::assertCount(0, $book->validation->getMessages());
    }

    public function testJacketToner_6(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::JACKET,
            'width' => 165,
            'height' => 238,
            'bind_type' => BindType::HARDCOVER_GLUED,
            'paper' => [
                [
                    'pages' => 1288,
                    'type' => 15,
                    'weight' => 70,
                    'volume' => 1.25,
                ],
                [
                    'pages' => 72,
                    'type' => 15,
                    'weight' => 150,
                    'volume' => 0.83,
                ],
            ],

            'spine' => SpineType::ROUND,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_2,

            'flaps' => 1,
            'flaps_symmetric' => 1,
            'flaps_width' => 75,
            'flaps_left' => 50,
            'flaps_right' => 50,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => '1',
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertNull($book->name);
        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(171.5, $book->cover->page_width_rear);
        self::assertSame(171.5, $book->cover->page_width_front);
        self::assertSame(244, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(72.0, $book->cover->spine);

        self::assertSame(75, $book->cover->flaps_left);
        self::assertSame(75, $book->cover->flaps_right);

        self::assertSame(575.0, $book->cover->total_width);
        self::assertSame(254, $book->cover->total_height);

        self::assertSame(60.49736, $book->binding->papers->first()->thickness);
        self::assertSame(64.99232, $book->binding->block_thickness);

        self::assertSame(1770.2916, $book->binding->papers->first()->mass);
        self::assertSame(18.6111, $book->cover->weight);

        self::assertSame(2143.28, round($book->weight, 2)); //Stara waga nie podlicza wkładki 1931.01

        self::assertCount(1, $book->validation->getMessages());

        self::assertArrayHasKey('block_thickness', $book->validation->getMessages());

        self::assertSame(
            __('The recommended block thickness for rounded spine should be in the range of :min-:max mm.', [ 'min' => HardCoverGlued::SPINE_BLOCK_MIN, 'max' => HardCoverGlued::SPINE_BLOCK_MAX]),
            $book->validation->get('block_thickness')[0]
        );
        self::assertSame(
            __('The block thickness for this type of binding is a maximum :max mm.', [ 'max' => HardCoverGlued::BLOCK_MAX]),
            $book->validation->get('block_thickness')[1]
        );
    }

    public function testCoverInkJet_7(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 212,
            'height' => 298,
            'bind_type' => BindType::HARDCOVER_GLUED,
            'paper' => [
                [
                    'pages' => 240,
                    'type' => 15,
                    'weight' => 80,
                    'volume' => 1.25,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_2_5,

            'flaps' => 0,
            'flaps_symmetric' => 1,
            'flaps_width' => 75,
            'flaps_left' => 50,
            'flaps_right' => 50,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::INK_JET,
            'job_name' => null,
            'show_big' => '1',
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(11, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(207.0, $book->cover->page_width_rear);
        self::assertSame(207.0, $book->cover->page_width_front);
        self::assertSame(304, $book->cover->page_height);

        self::assertSame(18, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(18.5, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(490.5, $book->cover->total_width);
        self::assertSame(340, $book->cover->total_height);

        self::assertSame(12.0, $book->binding->papers->first()->thickness);
        self::assertSame(12.0, $book->binding->block_thickness);

        self::assertSame(606.4896, $book->binding->papers->first()->mass);
        self::assertSame(260.23283000000004, $book->cover->weight);
        self::assertSame(866.72243, $book->weight);

        self::assertCount(0, $book->validation->getMessages());
    }

    public function testJacketToner_8(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::JACKET,
            'width' => 168,
            'height' => 240,
            'bind_type' => BindType::HARDCOVER_GLUED,
            'paper' => [
                [
                    'pages' => 456,
                    'type' => 15,
                    'weight' => 90,
                    'volume' => 1.3,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 1,
            'flaps_symmetric' => 1,
            'flaps_width' => 65,
            'flaps_left' => 50,
            'flaps_right' => 50,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => '1',
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(177.5, $book->cover->page_width_rear);
        self::assertSame(177.5, $book->cover->page_width_front);
        self::assertSame(246, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(35.0, $book->cover->spine);

        self::assertSame(65, $book->cover->flaps_left);
        self::assertSame(65, $book->cover->flaps_right);

        self::assertSame(530.0, $book->cover->total_width);
        self::assertSame(256, $book->cover->total_height);

        self::assertSame(27.19584, $book->binding->papers->first()->thickness);
        self::assertSame(27.19584, $book->binding->block_thickness);

        self::assertSame(827.3664, $book->binding->papers->first()->mass);
        self::assertSame(17.2692, $book->cover->weight);
        self::assertSame(1046.64291, $book->weight);

        self::assertCount(0, $book->validation->getMessages());
    }

}
