<?php

namespace Tests;

use Totem\SamAdmin\Testing\ApiTest;

class BookGeneratorApiTest extends ApiTest
{

    protected string $endpoint = 'book-generator';

    public function test_failed_get_form_information(): void
    {
        $this->get("/api/$this->endpoint/public")
            ->assertStatus(405)
            ->assertJsonFragment([
                'error' => [
                    'code' => 405,
                    'message' => 'The GET method is not supported for this route.'
                ]
            ]);
    }

    public function test_get_default_form_information(): void
    {
        $this->call('GET', "/api/$this->endpoint/public", ['default' => 1])
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    'printers',
                    'binds',
                    'spines',
                    'cardboard',
                    'flap_thickness',
                    'languages',
                ],
                'apiVersion'
            ]);
    }

}
