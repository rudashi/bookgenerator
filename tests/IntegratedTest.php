<?php

namespace Tests;

use Rudashi\BookGenerator\App\Classes\Binds\Integrated;
use Rudashi\Profis\Enums\BindType;
use Rudashi\BookGenerator\App\Enums\CardboardThickness;
use Rudashi\BookGenerator\App\Enums\CoverType;
use Rudashi\BookGenerator\App\Enums\FlapThickness;
use Rudashi\BookGenerator\App\Enums\Language;
use Rudashi\BookGenerator\App\Enums\PrinterType;
use Rudashi\BookGenerator\App\Enums\SpineType;

class IntegratedTest extends BookGeneratorTest
{

    public function testCoverInkJet_25(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 140,
            'height' => 210,
            'bind_type' => BindType::INTEGRATED,
            'paper' => [
                [
                    'pages' => 64,
                    'type' => '15',
                    'weight' => 100,
                    'volume' => 1.5,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 0,
            'flaps_symmetric' => 0,
            'flaps_width' => 60,
            'flaps_left' => 60,
            'flaps_right' => 70,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::INK_JET,
            'job_name' => null,
            'show_big' => 0,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(6, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(137.0, $book->cover->page_width_rear);
        self::assertSame(137.0, $book->cover->page_width_front);
        self::assertSame(216, $book->cover->page_height);

        self::assertSame(21, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(8.5, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(336.5, $book->cover->total_width);
        self::assertSame(258, $book->cover->total_height);

        self::assertSame(4.8, $book->binding->papers->first()->thickness);
        self::assertSame(4.8, $book->binding->block_thickness);

        self::assertSame(94.08, $book->binding->papers->first()->mass);
        self::assertSame(19.0836, $book->cover->weight);
        self::assertSame(113.1636, $book->weight);

        self::assertCount(0, $book->validation->getMessages());

    }

    public function testJacketInkJet_26(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::JACKET,
            'width' => 210,
            'height' => 292,
            'bind_type' => BindType::INTEGRATED,
            'paper' => [
                [
                    'pages' => 40,
                    'type' => '15',
                    'weight' => 150,
                    'volume' => 0.83,
                ],
                [
                    'pages' => 32,
                    'type' => '15',
                    'weight' => 135,
                    'volume' => 0.82,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_3,

            'flaps' => 1,
            'flaps_symmetric' => 1,
            'flaps_width' => 55,
            'flaps_left' => 60,
            'flaps_right' => 70,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::INK_JET,
            'job_name' => null,
            'show_big' => 0,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(0, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(213.5, $book->cover->page_width_rear);
        self::assertSame(213.5, $book->cover->page_width_front);
        self::assertSame(298, $book->cover->page_height);

        self::assertSame(5, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(9.0, $book->cover->spine);

        self::assertSame(55, $book->cover->flaps_left);
        self::assertSame(55, $book->cover->flaps_right);

        self::assertSame(556.0, $book->cover->total_width);
        self::assertSame(308, $book->cover->total_height);

        self::assertSame(2.49, $book->binding->papers->first()->thickness);
        self::assertSame(4.2612, $book->binding->block_thickness);

        self::assertSame(183.96, $book->binding->papers->first()->mass);
        self::assertSame(21.96558, $book->cover->weight);
        self::assertSame(377.22108, $book->weight);

        self::assertCount(0, $book->validation->getMessages());

    }

    public function testCoverToner_27(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 167,
            'height' => 238,
            'bind_type' => BindType::INTEGRATED,
            'paper' => [
                [
                    'pages' => 688,
                    'type' => '15',
                    'weight' => 70,
                    'volume' => 1.25,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_2_5,

            'flaps' => 0,
            'flaps_symmetric' => 1,
            'flaps_width' => 55,
            'flaps_left' => 60,
            'flaps_right' => 70,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => 0,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(6, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(164.0, $book->cover->page_width_rear);
        self::assertSame(164.0, $book->cover->page_width_front);
        self::assertSame(244, $book->cover->page_height);

        self::assertSame(21, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(34.5, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(416.5, $book->cover->total_width);
        self::assertSame(286, $book->cover->total_height);

        self::assertSame(30.1, $book->binding->papers->first()->thickness);
        self::assertSame(30.1, $book->binding->block_thickness);

        self::assertSame(957.08368, $book->binding->papers->first()->mass);
        self::assertSame(27.4134, $book->cover->weight);
        self::assertSame(984.49708, $book->weight);

        self::assertCount(0, $book->validation->getMessages());

    }

    public function testCoverToner_28(): void
    {
        $book = $this->repository->create(request()->merge([
            'cover_type' => CoverType::COVER,
            'width' => 285,
            'height' => 300,
            'bind_type' => BindType::INTEGRATED,
            'paper' => [
                [
                    'pages' => 10,
                    'type' => '15',
                    'name' => 'Objętościowy',
                    'weight' => 130,
                    'volume' => 0.99,
                ],
                [
                    'pages' => 10,
                    'type' => '15',
                    'name' => 'Amber Graphics',
                    'weight' => 115,
                    'volume' => 0.98,
                ],
            ],

            'spine' => SpineType::FLAT,
            'spine_encore' => 1,
            'cardboard' => CardboardThickness::CARDBOARD_2_5,

            'flaps' => 0,
            'flaps_symmetric' => 1,
            'flaps_width' => 55,
            'flaps_left' => 60,
            'flaps_right' => 70,
            'flaps_thickness' => FlapThickness::FLAP_1,

            'printer' => PrinterType::TONER,
            'job_name' => null,
            'show_big' => 0,
            'no_logo' => 0,
            'language' => Language::POLISH
        ]));

        self::assertSame(6, $book->cover->hinge);
        self::assertSame(0, $book->cover->big);

        self::assertSame(282.0, $book->cover->page_width_rear);
        self::assertSame(282.0, $book->cover->page_width_front);
        self::assertSame(306, $book->cover->page_height);

        self::assertSame(21, $book->cover->bleeds);
        self::assertSame(0, $book->cover->bleeds_inner);
        self::assertSame(8.0, $book->cover->spine);

        self::assertSame(0, $book->cover->flaps_left);
        self::assertSame(0, $book->cover->flaps_right);

        self::assertSame(626.0, $book->cover->total_width);
        self::assertSame(348, $book->cover->total_height);

        self::assertSame(0.6435, $book->binding->papers->first()->thickness);
        self::assertSame(1.2069999999999999, $book->binding->block_thickness);

        self::assertSame(55.575, $book->binding->papers->first()->mass);
        self::assertSame(53.6112, $book->cover->weight);
        self::assertSame(158.3487, $book->weight);

        self::assertCount(2, $book->validation->getMessages());
        self::assertArrayHasKey('pages_even', $book->validation->getMessages());
        self::assertArrayHasKey('block_thickness', $book->validation->getMessages());

        self::assertSame(
            __('The paper :name count :count is not divisible by four.', [ 'count' => $book->binding->papers->first()->pages, 'name' => $book->binding->papers->first()->name ]),
            $book->validation->get('pages_even')[0]
        );
        self::assertSame(
            __('The paper :name count :count is not divisible by four.', [ 'count' => $book->binding->papers->get(1)->pages, 'name' => $book->binding->papers->get(1)->name ]),
            $book->validation->get('pages_even')[1]
        );
        self::assertSame(
            __('The block thickness for this type of binding is a minimum :min mm.', [ 'min' => Integrated::BLOCK_MIN ]),
            $book->validation->get('block_thickness')[0]
        );
    }

}
